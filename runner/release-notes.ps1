Write-Host "Project directory: $env:CI_PROJECT_DIR"
Write-Host "Create build directory: $env:CI_PROJECT_DIR\build"
Write-Host "Getting tag $env:CI_COMMIT_TAG"
Write-Host "Running as $env:USERNAME";
$releaseNotes = $null
[system.io.directory]::CreateDirectory($env:CI_PROJECT_DIR+"\build");
$result = Invoke-WebRequest -Headers @{'PRIVATE-TOKEN'=$env:GITLAB_AUTH_TOKEN} -UseBasicParsing https://gitlab.com/api/v4/projects/boardworks%2Fboardworks-product-launcher/repository/tags/$env:CI_COMMIT_TAG;
Write-Host "Got Result $result";
$data = ($result | ConvertFrom-Json);
$data.release.description | tee -Variable releaseNotes;
$buildReleaseNotesPath = "$env:CI_PROJECT_DIR/build/release-notes.md";
$buildAssetsNotesPath = "$env:CI_PROJECT_DIR/src/assets/release-notes.md";
[IO.File]::WriteAllLines($buildReleaseNotesPath, $releaseNotes);
[IO.File]::WriteAllLines($buildAssetsNotesPath, $releaseNotes);
Write-Host $releaseNotes