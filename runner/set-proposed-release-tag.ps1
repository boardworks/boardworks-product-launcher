param (
	[string]$channel = "release"
)
Get-ChildItem Env:
Remove-Item Env:\CSC_LINK
Remove-Item Env:\CSC_KEY_PASSWORD
Write-Host Removed envs
Get-ChildItem Env:
Write-Host "Getting Release Tag"
cd $env:CI_PROJECT_DIR
$version = (& ".\node_modules\.bin\semantic-release-gitlab-dryrun.cmd" --dryrun)
If ($channel -ne "release") {
	$version = $version + "-" + $channel
}
Write-Host Version $version
(Get-Content .\package.json) | foreach-object { $_ -replace "0\.0\.0", "$version"} | Set-Content .\package.json