import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityThumbnailComponent } from './activity-thumbnail.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../../common/electron/testing/electron-testing.module';
import { Activity } from '../../common/product/activity';
import { Product } from '../../common/product/product';
import { ElectronService } from '../../common/electron/electron.module';

describe('ActivityThumbnailComponent', () => {
	let component: ActivityThumbnailComponent;
	let fixture: ComponentFixture<ActivityThumbnailComponent>;

	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			imports: [
			],
			declarations: [ ActivityThumbnailComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ActivityThumbnailComponent);
		component = fixture.componentInstance;
		const p: Product = new Product(undefined);
		component.data = new Activity(p, 0, 'test activity', 'test', 0, 0, 'test');
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
