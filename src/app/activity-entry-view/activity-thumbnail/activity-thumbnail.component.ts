import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Activity } from '../../common/product/activity';
import { ElectronService } from '../../common/electron/electron.module';
import { Observable,  BehaviorSubject, Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { AssetLoaderService } from '../../common/asset-loader/asset-loader.service';

@Component({
	selector: 'bwfe-activity-thumbnail',
	templateUrl: './activity-thumbnail.component.html',
	styleUrls: ['./activity-thumbnail.component.scss']
})
export class ActivityThumbnailComponent implements OnInit, AfterViewInit, OnDestroy {

	@Input() data: Activity;

	private dataUriSubject: BehaviorSubject<SafeUrl> = new BehaviorSubject<SafeUrl>('');
	public dataUri$: Observable<SafeUrl> = this.dataUriSubject.asObservable().pipe(distinctUntilChanged());

	public loading = true;
	private subscriptions: Subscription[];

	constructor(public electronService: ElectronService, private sanitizer: DomSanitizer, private assetLoaderService: AssetLoaderService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.loading = true;
		// Load the screenshot
		if (this.data != null) {
			const screenshot: string = this.data.getScreenshot(this.electronService);
			if (screenshot != null ) {
				this.subscriptions.push(this.assetLoaderService.load(screenshot).pipe(
					map((blob: Blob) => {
						if (blob != null) {
							const urlCreator = window.URL;
							return this.sanitizer.bypassSecurityTrustUrl(
								urlCreator.createObjectURL(blob));
						}
					})
				).subscribe({
					next: (result) => {
						this.loading = false;
						this.dataUriSubject.next(result);
					},
					error: (err) => {
						console.log(`Error: ${err}`);
					}
				}));
			} else {
				this.subscriptions.push(this.assetLoaderService.load(
					this.data.product.getPath(this.data.product.placeholderScreenshotPath, this.electronService)).pipe(
					map((blob: Blob) => {
						if (blob != null) {
							const urlCreator = window.URL;
							return this.sanitizer.bypassSecurityTrustUrl(
								urlCreator.createObjectURL(blob));
						}
					})
				).subscribe({
					next: (result) => {
						this.loading = false;
						this.dataUriSubject.next(result);
					},
					error: (err) => {
						console.log(`Error: ${err}`);
					}
				}));
			}
		}
	}

	ngAfterViewInit() {

	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}
}
