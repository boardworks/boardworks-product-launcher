import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { ActivityEntryViewComponent } from './activity-entry-view.component';
import { ActivityThumbnailComponent } from './activity-thumbnail/activity-thumbnail.component';

@NgModule({
	imports: [CommonModule,
		// FlexLayoutModule,
	],
	exports: [ActivityEntryViewComponent, ActivityThumbnailComponent],
	declarations: [ActivityEntryViewComponent, ActivityThumbnailComponent],
})
export class ActivityEntryViewModule {

}

export * from './activity-entry-view.component';
export * from './activity-thumbnail/activity-thumbnail.component';
