import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityEntryViewComponent } from './activity-entry-view.component';
import { Activity } from '../common/product/activity';
import { Product } from '../common/product/product';
import { ActivityThumbnailComponent } from './activity-thumbnail/activity-thumbnail.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../common/electron/testing/electron-testing.module';
import { ElectronService } from '../common/electron/electron.module';

describe('ActivityEntryViewComponent', () => {
	let component: ActivityEntryViewComponent;
	let fixture: ComponentFixture<ActivityEntryViewComponent>;
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			declarations: [ ActivityEntryViewComponent, ActivityThumbnailComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ActivityEntryViewComponent);
		component = fixture.componentInstance;
		const p: Product = new Product(undefined);
		component.activity = new Activity(p, 0, 'test', 'test', 0, 0, 'test');
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
