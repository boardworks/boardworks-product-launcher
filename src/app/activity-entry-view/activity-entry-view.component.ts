import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Activity } from '../common/product/activity';
import { ElectronService } from '../common/electron/electron.module';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
	selector: 'bwfe-activity-entry-view',
	templateUrl: './activity-entry-view.component.html',
	styleUrls: ['./activity-entry-view.component.scss']
})
export class ActivityEntryViewComponent implements OnInit, OnDestroy {

	worksheetSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	worksheet$: Observable<boolean> = this.worksheetSubject.asObservable().pipe(distinctUntilChanged());

	private subscriptions: Subscription[];

	@Input() activity: Activity;

	@Input() showThumbnail = true;

	@Input() allowPreview = true;

	@Output()
	public previewActivityChange: EventEmitter<number> = new EventEmitter<number>();

	constructor(private electronService: ElectronService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		if (this.activity != null) {
			this.subscriptions.push(this.activity.hasWorksheet(this.electronService).subscribe(value => {
				this.worksheetSubject.next(value);
			}));
		}
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	launchWorksheet(): Promise<void> {
		return this.activity.launchWorksheet(this.electronService);
	}

	launchActivity(): Promise<boolean> {
		return this.activity.launch(this.electronService);
	}

	previewActivity() {
		this.previewActivityChange.emit(this.activity.ref);
	}
}
