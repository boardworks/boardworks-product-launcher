import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductMapViewComponent } from './product-map-view.component';
import { StateMapComponent } from './state-map/state-map.component';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';
import { ToolbarTestingModule } from '../common/toolbar/testing/toolbar-testing.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FooterTestingModule } from '../common/footer/testing/footer-testing.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductTestingModule } from '../common/product/testing/product-testing.module';
import { ProductService } from '../common/product/product.module';
import { ProductTestingService } from '../common/product/testing/product-testing.service';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../common/electron/testing/electron-testing.module';
import { ElectronService } from '../common/electron/electron.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProductMapViewComponent', () => {
	let component: ProductMapViewComponent;
	let fixture: ComponentFixture<ProductMapViewComponent>;
	const mockProductService = new ProductTestingService().configure({products: []});
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {
		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				FooterTestingModule,
				ToolbarTestingModule,
				ProductTestingModule,
				ContactDetailsModule,
				PerfectScrollbarModule,
				HttpClientTestingModule
			],
			declarations: [ ProductMapViewComponent, StateMapComponent ],
			providers: [
				{ provide: ProductService, useValue: mockProductService },
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductMapViewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
