import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductMapViewComponent } from './product-map-view.component';
import { StateMapComponent } from './state-map/state-map.component';
import { FooterModule } from '../common/footer/footer.module';
import { ToolbarModule } from '../common/toolbar/toolbar.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';

@NgModule({
	imports: [
		CommonModule,
		FooterModule,
		ToolbarModule,
		ContactDetailsModule,
		PerfectScrollbarModule
	],
	declarations: [ProductMapViewComponent, StateMapComponent],
	exports: [ProductMapViewComponent, StateMapComponent]
})
export class ProductMapViewModule { }
