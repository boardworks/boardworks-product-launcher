import { Component, OnInit, AfterViewInit, ViewChildren, QueryList, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Product } from '../common/product/product';
import { State } from '../common/product/state';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../common/product/product.module';
import { StateMapComponent } from './state-map/state-map.component';

@Component({
	selector: 'bwfe-product-map-view',
	templateUrl: './product-map-view.component.html',
	styleUrls: ['./product-map-view.component.scss']
})
export class ProductMapViewComponent implements OnInit, AfterViewInit, OnDestroy {
	product$: BehaviorSubject<Product>;
	state$: BehaviorSubject<State>;
	states$: BehaviorSubject<State[]>;

	productId: string;
	stateAbbreviation: string;
	productSubscription: Subscription;

	@ViewChild('stateList') stateList: ElementRef;

	constructor(private router: Router, private route: ActivatedRoute, public productService: ProductService) {
		this.product$ = new BehaviorSubject<Product>(undefined);
		this.state$ = new BehaviorSubject<State>(undefined);
		this.states$ = new BehaviorSubject<State[]>([]);
	}

	ngOnInit() {
		this.productSubscription = this.route.params.subscribe(params => {
			this.productId = params['id'];
			this.stateAbbreviation = params['stateAbbreviation'];
			// console.log(`Product View is: ${this.productId} with state ${this.stateAbbreviation}`);
			this.productService.getProduct(this.productId).subscribe(value => {
				if (value != null) {
					this.product$.next(value);
					// Update the state list - sorted alphabetically
					this.states$.next(value.statesOnly.sort((a: State, b: State) => {
						if (a.name > b.name) {
							return 1;
						}
						if (a.name < b.name) {
							return -1;
						}
						return 0;
					}));
					this.state$.next(value.getState(this.stateAbbreviation));
				}
			});
		});
	}

	ngOnDestroy() {
		if (this.productSubscription != null) {
			this.productSubscription.unsubscribe();
		}
		this.productSubscription = null;
	}

	ngAfterViewInit() {
	}

	onStateSelectionChange(abbreviation) {
		console.log(`Selected: ${abbreviation}`);
		this.state$.next(this.product$.getValue().getState(abbreviation));
		// Scroll the state into view
		// Find the selected state
		const el: Element = this.stateList.nativeElement;
		const element: Element = el.querySelector<Element>(`#${abbreviation}`);
		element.scrollIntoView();
	}

	selectState(state: State) {
		this.state$.next(state);
	}

	viewState(state?: string) {
		if (state == null) {
			state = this.state$.getValue().abbreviation;
		}
		this.router.navigate(['./state', this.productId, state]);
	}
}
