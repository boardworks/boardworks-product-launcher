import { Component, OnInit, ViewChild, AfterViewInit, Input, ElementRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { State } from '../../common/product/state';

@Component({
	selector: 'bwfe-state-map',
	templateUrl: './state-map.component.html',
	styleUrls: ['./state-map.component.scss']
})
export class StateMapComponent implements OnInit, AfterViewInit, OnDestroy {

	@ViewChild('map') map: ElementRef;

	private _states: State[];
	private _selectedState: State;

	public get states(): State[] {
		return this._states;
	}

	@Input()
	public set states(values: State[]) {
		this._states = values;
		this.updateStates();
	}

	public get selectedState(): State {
		return this._selectedState;
	}

	@Input()
	public set selectedState(value: State) {
		this._selectedState = value;
		this.showSelectedState();
	}

	@Output()
	public stateSelectionChange: EventEmitter<string> = new EventEmitter<string>();

	constructor() { }

	ngOnInit() {
	}

	ngAfterViewInit() {
		// console.error(`AfterView!`);
	}

	ngOnDestroy() {

	}

	updateStates() {
		this.resetStates();
		this.setupStates();
		this.showSelectedState();
	}

	resetStates() {
		const elements: NodeList = this.map.nativeElement.querySelectorAll('#map_mc > g');
		const arr: Node[] = Array.from(elements);
		arr.forEach(value => {
			(value as SVGElement).classList.add('disabled');
		});
	}

	deselectStates() {
		const elements: NodeList = this.map.nativeElement.querySelectorAll('#map_mc > g');
		const arr: Node[] = Array.from(elements);
		arr.forEach(value => {
			if ((value as SVGElement).classList.contains('selected')) {
				(value as SVGElement).classList.remove('selected');
			}
		});
	}

	setupStates() {
		this.states.forEach((state: State) => {
			// Get the state SVG group
			const stateElementSelector = `#${state.abbreviation}_mc`;
			const stateElement: Element = this.map.nativeElement.querySelector(stateElementSelector);

			if (stateElement != null) {
				stateElement.classList.remove('disabled');
				stateElement.classList.add('active');
				stateElement.addEventListener('click', () => {
					this.selectState(state.abbreviation);
				});
			}
		});
	}

	showSelectedState() {
		if (this.selectedState) {
			this.deselectStates();
			const stateElementSelector = `#${this.selectedState.abbreviation}_mc`;
			const stateElement: Element = this.map.nativeElement.querySelector(stateElementSelector);
			if (stateElement != null) {
				stateElement.classList.add('selected');
			}
		}
	}

	selectState(abbreviation: string) {
		this.stateSelectionChange.emit(abbreviation);
	}
}
