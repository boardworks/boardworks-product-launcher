import { PresentationPreviewModule } from './presentation-preview.module';

describe('PresentationPreviewModule', () => {
	let presentationPreviewModule: PresentationPreviewModule;

	beforeEach(() => {
		presentationPreviewModule = new PresentationPreviewModule();
	});

	it('should create an instance', () => {
		expect(presentationPreviewModule).toBeTruthy();
	});
});
