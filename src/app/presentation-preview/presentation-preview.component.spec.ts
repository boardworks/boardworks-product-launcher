import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentationPreviewComponent } from './presentation-preview.component';
import { SlideThumbnailComponent } from './slide-thumbnail/slide-thumbnail.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../common/electron/testing/electron-testing.module';
import { ElectronService } from '../common/electron/electron.module';
import { Activity } from '../common/product/activity';

describe('PresentationPreviewComponent', () => {
	let component: PresentationPreviewComponent;
	let fixture: ComponentFixture<PresentationPreviewComponent>;
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {
		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			declarations: [ PresentationPreviewComponent, SlideThumbnailComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PresentationPreviewComponent);
		component = fixture.componentInstance;
		component.activity = new Activity(undefined, 0, '', '', 0, 0, '');
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
