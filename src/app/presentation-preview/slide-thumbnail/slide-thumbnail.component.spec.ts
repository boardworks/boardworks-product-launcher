import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideThumbnailComponent } from './slide-thumbnail.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../../common/electron/testing/electron-testing.module';
import { ElectronService } from '../../common/electron/electron.module';

describe('SlideThumbnailComponent', () => {
	let component: SlideThumbnailComponent;
	let fixture: ComponentFixture<SlideThumbnailComponent>;
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {
		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			declarations: [ SlideThumbnailComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SlideThumbnailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
