import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ElectronService } from '../../common/electron/electron.module';
import { AssetLoaderService } from '../../common/asset-loader/asset-loader.service';

@Component({
	selector: 'bwfe-slide-thumbnail',
	templateUrl: './slide-thumbnail.component.html',
	styleUrls: ['./slide-thumbnail.component.scss']
})
export class SlideThumbnailComponent implements OnInit, OnDestroy {

	private dataUriSubject: BehaviorSubject<SafeUrl> = new BehaviorSubject<SafeUrl>('');
	public dataUri$: Observable<SafeUrl> = this.dataUriSubject.asObservable().pipe(distinctUntilChanged());

	public loading = true;
	private subscriptions: Subscription[];

	@Input()
	thumbnail: string;

	constructor(public electronService: ElectronService, private sanitizer: DomSanitizer, private assetLoaderService: AssetLoaderService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.loading = true;
		// Load the screenshot
		if (this.thumbnail != null) {
			this.subscriptions.push(this.assetLoaderService.load(this.thumbnail).pipe(
				map((blob: Blob) => {
					if (blob != null) {
						console.log(`BLOB: ${blob.size}`);
						const urlCreator = window.URL;
						return this.sanitizer.bypassSecurityTrustUrl(
							urlCreator.createObjectURL(blob));
					}
				})
			).subscribe({
				next: (result) => {
					this.loading = false;
					console.log(`Loading: ${this.loading} - ${result}`);
					this.dataUriSubject.next(result);
				},
				error: (err) => {
					console.log(`Error: ${err}`);
				}
			}));
		}
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}
}
