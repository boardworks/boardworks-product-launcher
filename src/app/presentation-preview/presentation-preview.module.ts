import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PresentationPreviewComponent } from './presentation-preview.component';
import { SlideThumbnailComponent } from './slide-thumbnail/slide-thumbnail.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
	imports: [
		CommonModule,
		PerfectScrollbarModule,
	],
	declarations: [PresentationPreviewComponent, SlideThumbnailComponent],
	exports: [PresentationPreviewComponent]
})
export class PresentationPreviewModule { }
