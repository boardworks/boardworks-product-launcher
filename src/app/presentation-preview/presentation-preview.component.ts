import { Component, OnInit, Input, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Activity } from '../common/product/activity';
import { ElectronService } from '../common/electron/electron.module';
import { ValueTransformer } from '@angular/compiler/src/util';

@Component({
	selector: 'bwfe-presentation-preview',
	templateUrl: './presentation-preview.component.html',
	styleUrls: ['./presentation-preview.component.scss']
})
export class PresentationPreviewComponent implements OnInit {

	private _activity: Activity;
	private _firstVisible = 0;
	private _screenshots: string[] = [];

	public get firstVisible(): number {
		return this._firstVisible;
	}

	@ViewChild('holder')
	holder: ElementRef;

	@Output()
	public previewViewChange: EventEmitter<boolean> = new EventEmitter<boolean>();

	public get slideCount(): number {
		if (this.holder != null) {
			const holderDiv: HTMLDivElement = this.holder.nativeElement;
			const slideDiv: HTMLDivElement = holderDiv.querySelector('.slide-item');
			const slideWidth: number = slideDiv != null ? slideDiv.offsetWidth : 195;
			const holderWidth: number = holderDiv.offsetWidth;
			if (slideDiv != null) {
				const style: CSSStyleDeclaration = window.getComputedStyle(slideDiv);

			}
			return Math.floor(holderWidth / slideWidth);
		}
		return 0;
	}

	@Input()
	public set activity(value: Activity) {
		this._activity = value;
		console.log(`Preview: ${this._activity}`);
		if (this._activity != null) {
			console.log(`Preview ${this._activity.length} slides`);
		}
		this._firstVisible = 0;
		this.updateScreenshots();
	}

	public get activity(): Activity {
		return this._activity;
	}

	public get screenshots(): string[] {
		return this._screenshots;
	}

	public get hasPreviousSlide(): boolean {
		return this._firstVisible > 0;
	}

	public get hasNextSlide(): boolean {
		return this._firstVisible + this.slideCount < this.activity.length;
	}

	constructor(private electronService: ElectronService) { }

	ngOnInit() {
		this.updateScreenshots();
	}

	/**
	 * Gets the range of preview screenshots from the firstVisible
	 * for the number of visible ones (given by slideCount)
	 */
	public updateScreenshots() {
		if (this.activity != null) {
			this._screenshots = this.activity.getPreviewScreenshots(this.electronService).filter((value: string, index: number, array: string[]) => {
				if (index >= this.firstVisible && index < (this.firstVisible + this.slideCount)) {
					return true;
				}
				return false;
			});
		}
	}

	updateSlideList() {
		console.log(`UPDATE ON RESIZE`);
		this.updateScreenshots();
	}

	previousSlide() {
		if (this.hasPreviousSlide) {
			this._firstVisible--;
			this.updateSlideList();
		}
	}

	nextSlide() {
		if (this.hasNextSlide) {
			this._firstVisible++;
			this.updateSlideList();
		}
	}

	closePreview() {
		this.previewViewChange.emit(false);
	}

	launchPresentation() {
		this.activity.launch(this.electronService);
	}
}
