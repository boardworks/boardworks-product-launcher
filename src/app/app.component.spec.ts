import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { TranslateModule } from '@ngx-translate/core';
import { ElectronService } from './common/electron/electron.module';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from './common/electron/testing/electron-testing.module';
import { ToolbarTestingModule } from './common/toolbar/testing/toolbar-testing.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FooterTestingModule } from './common/footer/testing/footer-testing.module';

describe('AppComponent', () => {

	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			declarations: [
				AppComponent
			],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService },
			],
			imports: [
				RouterTestingModule,
				TranslateModule.forRoot(),
				HttpClientTestingModule,
				ToolbarTestingModule,
				FooterTestingModule
			]
		}).compileComponents();
	}));

	it('should create the app', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));
});

class TranslateServiceStub {
	setDefaultLang(lang: string): void {
	}
}
