import { Component } from '@angular/core';
import { ElectronService } from './common/electron/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { UpdateService } from './common/update/update.service';
import { Router } from '@angular/router';

@Component({
	selector: 'bwfe-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	constructor(public electronService: ElectronService,
		private translate: TranslateService, private updater: UpdateService, private route: Router) {

		translate.setDefaultLang('en');
		console.log('AppConfig', AppConfig);

		if (electronService.isElectron()) {
			console.log('Mode electron');
			console.log('Electron ipcRenderer', electronService.ipcRenderer);
			console.log('NodeJS childProcess', electronService.childProcess);
			// Get the updater
			console.log(`Getting updater: ${this.updater}`);
			console.log(`Route: ${this.route.url}`);
			if (this.route.url === '/') {
				// this.updater.checkForUpdate();
			}
		} else {
			console.log('Mode web');
		}
	}
}
