import { NgModule } from '@angular/core';
import { Routes, RouterModule, UrlSegment, UrlSerializer } from '@angular/router';
import { ProductSelectionComponent } from './product-selection/product-selection.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { StateViewComponent } from './state-view/state-view.component';
import { ProductMapViewComponent } from './product-map-view/product-map-view.component';
import { ReleaseNotesComponent } from './release-notes/release-notes.module';
import { AssetLoaderComponent } from './common/asset-loader/asset-loader.component';
import { SearchThreadComponent } from './common/search-thread/search-thread.component';
// const { ipcRenderer, remote } = window.require('electron');
// const log = remote.require('electron-log');
import {StandardlessStateViewComponent} from './standardless-state-view/standardless-state-view.component';


const routes: Routes = [
	{
		path: '',
		component: ProductSelectionComponent
	},
	/**/
	{
		path: 'release-notes/:version/:notes',
		component: ReleaseNotesComponent
	},
	{
		path: 'asset-loader',
		component: AssetLoaderComponent
	},
	{
		path: 'search',
		component: SearchThreadComponent
	},
	{
		path: 'product/:id',
		component: ProductViewComponent
	},
	{
		path: 'state/:id/:stateAbbreviation/:standardsRef',
		component: StateViewComponent
	},
	{
		path: 'state/:id/:stateAbbreviation',
		component: StateViewComponent
	},
	{
		path: 'map/:id',
		component: ProductMapViewComponent
	},
	{
		path: 'map/:id/:stateAbbreviation',
		component: ProductMapViewComponent
	},
	{
		path: 'stateless-product/:id',
		component: StandardlessStateViewComponent
	},
	{
		path: ':previous',
		component: ProductSelectionComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {useHash: true})],
	exports: [RouterModule]
})
export class AppRoutingModule { }
