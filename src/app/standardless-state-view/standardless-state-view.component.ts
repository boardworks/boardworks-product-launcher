import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService, Product, State, Activity } from '../common/product/product.module';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
	selector: 'bwfe-standardless-state-view',
	templateUrl: './standardless-state-view.component.html',
	styleUrls: ['./standardless-state-view.component.scss']
})
export class StandardlessStateViewComponent implements OnInit, OnDestroy {
	productSubject: BehaviorSubject<Product> = new BehaviorSubject<Product>(undefined);
	product$: Observable<Product> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	activitiesSubject: BehaviorSubject<Activity[]> = new BehaviorSubject<Activity[]>([]);
	activities$: Observable<Activity[]> = this.activitiesSubject.asObservable().pipe(distinctUntilChanged());

	previewSubject: BehaviorSubject<Activity> = new BehaviorSubject<Activity>(undefined);
	preview$: Observable<Activity> = this.previewSubject.asObservable().pipe(distinctUntilChanged());

	productId: string;
	productSubscription: Subscription;

	private subscriptions: Subscription[];

	constructor(private router: Router, private route: ActivatedRoute, public productService: ProductService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.activitiesSubject.next([]);
		this.subscriptions.push(this.route.params.subscribe(params => {
			this.productId = params['id'];
			// console.log(`Product View is: ${this.productId} with state ${this.stateAbbreviation}`);
			this.subscriptions.push(this.productService.getProduct(this.productId).subscribe(value => {
				if (value != null) {
					this.productSubject.next(value);
					this.activitiesSubject.next(value.activities);
				}
			}));
		}));
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	goBack() {
		this.router.navigate(['./product', this.productId]);
	}

	productStyle(): object {
		if (this.productSubject.getValue() != null &&
			this.productSubject.getValue().productColor != null) {
			return {
				'background-color': this.productSubject.getValue().productColor,
			};
		} else {
			return {};
		}
	}

	onPreviewActivity(ref: number) {
		console.log(`Preview: ${ref}`);
		// Get the activity
		const preview: Activity = this.activitiesSubject.getValue().find(activity => activity.ref === ref);
		console.log(`Found: ${preview}`);
		if (preview != null) {
			this.previewSubject.next(preview);
		}
	}

	previewViewChanged(view: boolean) {
		if (!view) {
			this.previewSubject.next(undefined);
		}
	}

}
