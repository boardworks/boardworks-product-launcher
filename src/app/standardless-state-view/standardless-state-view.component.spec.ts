import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardlessStateViewComponent } from './standardless-state-view.component';
import { StateViewComponent } from '../state-view/state-view.module';
import { RouterTestingModule } from '@angular/router/testing';
import { DefaultElectronTestingConfiguration, ElectronTestingService } from '../common/electron/testing/electron-testing.service';
import { ProductTestingService } from '../common/product/testing/product-testing.module';
import { FooterModule } from '../common/footer/footer.module';
import { ToolbarTestingModule } from '../common/toolbar/testing/toolbar-testing.module';
import { ActivityEntryViewModule } from '../activity-entry-view/activity-entry-view.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PresentationPreviewModule } from '../presentation-preview/presentation-preview.module';
import { ProductService } from '../common/product/product.module';
import { ElectronService } from '../common/electron/electron.service';

describe('StandardlessStateViewComponent', () => {
	let component: StandardlessStateViewComponent;
	let fixture: ComponentFixture<StandardlessStateViewComponent>;
	const mockProductService = new ProductTestingService().configure({products: []});
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			declarations: [ StandardlessStateViewComponent ],
			imports: [RouterTestingModule,
				FooterModule,
				ToolbarTestingModule,
				ActivityEntryViewModule,
				PerfectScrollbarModule,
				ContactDetailsModule,
				HttpClientTestingModule,
				PresentationPreviewModule
			],
			providers: [
				{ provide: ProductService, useValue: mockProductService},
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StandardlessStateViewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
