import { StandardlessStateViewModule } from './standardless-state-view.module';

describe('StandardlessStateViewModule', () => {
	let standardlessStateViewModule: StandardlessStateViewModule;

	beforeEach(() => {
		standardlessStateViewModule = new StandardlessStateViewModule();
	});

	it('should create an instance', () => {
		expect(standardlessStateViewModule).toBeTruthy();
	});
});
