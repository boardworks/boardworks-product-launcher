import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StandardlessStateViewComponent } from './standardless-state-view.component';
import { RouterModule } from '@angular/router';
import { FooterModule } from '../common/footer/footer.module';
import { ToolbarModule } from '../common/toolbar/toolbar.module';
import { ActivityEntryViewModule } from '../activity-entry-view/activity-entry-view.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';
import { PresentationPreviewModule } from '../presentation-preview/presentation-preview.module';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FooterModule,
		ToolbarModule,
		ActivityEntryViewModule,
		PerfectScrollbarModule,
		ContactDetailsModule,
		PresentationPreviewModule
	],
	declarations: [StandardlessStateViewComponent],
	exports: [StandardlessStateViewComponent]
})
export class StandardlessStateViewModule { }
