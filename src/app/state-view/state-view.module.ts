import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StateViewComponent } from './state-view.component';
import { FooterModule, FooterComponent } from '../common/footer/footer.module';
import { ToolbarModule } from '../common/toolbar/toolbar.module';
import { StandardsTreeModule } from '../standards-tree/standards-tree.module';
import { ActivityEntryViewModule } from '../activity-entry-view/activity-entry-view.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';
import { PresentationPreviewModule } from '../presentation-preview/presentation-preview.module';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FooterModule,
		ToolbarModule,
		StandardsTreeModule,
		ActivityEntryViewModule,
		PerfectScrollbarModule,
		ContactDetailsModule,
		PresentationPreviewModule
	],
	exports: [StateViewComponent],
	declarations: [StateViewComponent],
})
export class StateViewModule {

}

export * from './state-view.component';
