import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StateViewComponent } from './state-view.component';
import { ProductTestingService } from '../common/product/testing/product-testing.service';
import { ProductService } from '../common/product/product.module';
import { FooterModule } from '../common/footer/footer.module';
import { ActivityEntryViewModule } from '../activity-entry-view/activity-entry-view.module';
import { StandardsTreeModule } from '../standards-tree/standards-tree.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../common/electron/testing/electron-testing.module';
import { ElectronService } from '../common/electron/electron.module';
import { ToolbarTestingModule } from '../common/toolbar/testing/toolbar-testing.module';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PresentationPreviewModule } from '../presentation-preview/presentation-preview.module';

describe('StateViewComponent', () => {
	let component: StateViewComponent;
	let fixture: ComponentFixture<StateViewComponent>;
	const mockProductService = new ProductTestingService().configure({products: []});
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			declarations: [ StateViewComponent ],
			imports: [RouterTestingModule,
				FooterModule,
				ToolbarTestingModule,
				ActivityEntryViewModule,
				StandardsTreeModule,
				PerfectScrollbarModule,
				ContactDetailsModule,
				HttpClientTestingModule,
				PresentationPreviewModule
			],
			providers: [
				{ provide: ProductService, useValue: mockProductService},
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StateViewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
