import { Component, OnInit, OnDestroy, ViewChildren, AfterViewInit, QueryList, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService, Product, State, Activity } from '../common/product/product.module';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';
import { Standards } from '../common/product/standards';
import { StandardsTreeService } from '../standards-tree/standards-tree.service';
import { StandardsTreeComponent } from '../standards-tree/standards-tree.module';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
	selector: 'bwfe-state-view',
	templateUrl: './state-view.component.html',
	styleUrls: ['./state-view.component.scss']
})
export class StateViewComponent implements OnInit, OnDestroy, AfterViewInit {

	productSubject: BehaviorSubject<Product> = new BehaviorSubject<Product>(undefined);
	product$: Observable<Product> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	stateSubject: BehaviorSubject<State> = new BehaviorSubject<State>(undefined);
	state$: Observable<State> = this.stateSubject.asObservable().pipe(distinctUntilChanged());

	standardsSubject: BehaviorSubject<Standards> = new BehaviorSubject<Standards>(undefined);
	standards$: Observable<Standards> = this.standardsSubject.asObservable().pipe(distinctUntilChanged());

	selectedStandardsSubject: BehaviorSubject<Standards> = new BehaviorSubject<Standards>(undefined);
	selectedStandards$: Observable<Standards> = this.selectedStandardsSubject.asObservable().pipe(distinctUntilChanged());

	activitiesSubject: BehaviorSubject<Activity[]> = new BehaviorSubject<Activity[]>([]);
	activities$: Observable<Activity[]> = this.activitiesSubject.asObservable().pipe(distinctUntilChanged());

	previewSubject: BehaviorSubject<Activity> = new BehaviorSubject<Activity>(undefined);
	preview$: Observable<Activity> = this.previewSubject.asObservable().pipe(distinctUntilChanged());

	productId: string;
	stateAbbreviation: string;
	selectedStandards: number;
	productSubscription: Subscription;

	@ViewChild(StandardsTreeComponent) tree: StandardsTreeComponent;

	private subscriptions: Subscription[];

	constructor(private router: Router, private route: ActivatedRoute, public productService: ProductService,
				public standardsTreeService: StandardsTreeService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.activitiesSubject.next([]);
		this.subscriptions.push(this.route.params.subscribe(params => {
			this.productId = params['id'];
			this.stateAbbreviation = params['stateAbbreviation'];
			this.selectedStandards = parseInt(params['standardsRef'], 10);
			// console.log(`Product View is: ${this.productId} with state ${this.stateAbbreviation}`);
			this.subscriptions.push(this.productService.getProduct(this.productId).subscribe(value => {
				if (value != null) {
					this.productSubject.next(value);
					// console.log(`Get state`);
					this.subscriptions.push(this.stateSubject.subscribe(state => {
						if (state != null) {
							if (this.stateSubject.getValue().standards != null) {
								this.standardsSubject.next(this.stateSubject.getValue().standards);
								if (this.selectedStandards != null && !isNaN(this.selectedStandards)) {
									//  const standards = this.standards$.getValue().find(this.selectedStandards);
									// this.selectedStandards$.next(standards);
									console.log(`Setting selection to: ${this.selectedStandards}`);
									// this.standardsTreeService.setSelection(this.selectedStandards);
								}
							}
						}
					}));
					this.stateSubject.next(value.getState(this.stateAbbreviation));
					// Select the top level standard in the state if one isn't already selected by the routing
					// if (this.selectedStandards == null || isNaN(this.selectedStandards)) {
						this.standardsTreeService.setSelection(0);
					// }
					this.subscriptions.push(this.standardsTreeService.getSelection().subscribe(ref => {
						if (this.stateSubject.getValue().standards != null) {
							const s: Standards = this.stateSubject.getValue().standards.find(ref);
							if (s != null) {
								// console.error(`Found ${s.ref} - ${s.children.length}`);
								const activities: Activity[] = s.sortedChildActivities;
								if (activities != null) {
									// console.warn(`Has: ${activities.length}`);
									this.activitiesSubject.next(activities);
								}
							}
						}
					}));
				}
			}));
		}));
	}

	ngAfterViewInit() {
		console.log(`Children: ${this.tree.nodes.length}`);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	getStandards() {
		console.error(`get standards///`);
		return this.stateSubject.getValue().standards;
	}

	goBack() {
		this.router.navigate(['./product', this.productId]);
	}

	productStyle(): object {
		if (this.productSubject.getValue() != null &&
			this.productSubject.getValue().productColor != null) {
			return {
				'background-color': this.productSubject.getValue().productColor,
			};
		} else {
			return {};
		}
	}

	onPreviewActivity(ref: number) {
		console.log(`Preview: ${ref}`);
		// Get the activity
		const preview: Activity = this.activitiesSubject.getValue().find(activity => activity.ref === ref);
		console.log(`Found: ${preview}`);
		if (preview != null) {
			this.previewSubject.next(preview);
		}
	}

	previewViewChanged(view: boolean) {
		if (!view) {
			this.previewSubject.next(undefined);
		}
	}
}
