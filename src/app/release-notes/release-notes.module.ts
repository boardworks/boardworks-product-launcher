import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ReleaseNotesComponent } from './release-notes.component';

@NgModule({
	imports: [CommonModule,
		PerfectScrollbarModule,
	],
	exports: [ReleaseNotesComponent],
	declarations: [ReleaseNotesComponent],
	providers: [],
})
export class ReleaseNotesModule {

}

export * from './release-notes.component';
