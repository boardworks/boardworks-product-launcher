import { Component, OnInit } from '@angular/core';
import { ElectronService } from '../common/electron/electron.module';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'bwfe-release-notes',
	templateUrl: './release-notes.component.html',
	styleUrls: ['./release-notes.component.scss']
})
export class ReleaseNotesComponent implements OnInit {

	private notes: string;

	public version: string;

	public get notesHtml(): string {
		return this.electronService.markdownToHtml(this.notes);
	}

	constructor(public route: ActivatedRoute, public electronService: ElectronService) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.version = params['version'];
			try {
				this.notes = atob(decodeURIComponent(params['notes']));
			} catch (e) {
				this.notes = '';
			}
		});
	}

}
