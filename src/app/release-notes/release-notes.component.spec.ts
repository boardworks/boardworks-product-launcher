import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseNotesComponent } from './release-notes.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../common/electron/testing/electron-testing.module';
import { ElectronService } from '../common/electron/electron.module';

describe('ReleaseNotesComponent', () => {
	let component: ReleaseNotesComponent;
	let fixture: ComponentFixture<ReleaseNotesComponent>;
	let mockElectronService: ElectronTestingService;


	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule
			],
			declarations: [ ReleaseNotesComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ReleaseNotesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
