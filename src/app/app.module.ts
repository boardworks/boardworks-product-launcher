import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './common/electron/electron.service';

import { AppComponent } from './app.component';
// import { HomeComponent } from './components/home/home.component';
import { ProductModule } from './common/product/product.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ProductSelectionModule } from './product-selection/product-selection.module';
import { StateViewModule } from './state-view/state-view.module';
import { ToolbarModule } from './common/toolbar/toolbar.module';
import { FooterModule } from './common/footer/footer.module';
import { ProductViewModule } from './product-view/product-view.module';
import { ProductMapViewModule } from './product-map-view/product-map-view.module';
import { UpdateModule } from './common/update/update.module';
import { ReleaseNotesModule } from './release-notes/release-notes.module';
import { AssetLoaderModule } from './common/asset-loader/asset-loader.module';
import { SearchThreadModule } from './common/search-thread/search-thread-module';
import { ProductLocationModule } from './common/product-location/product-location.module';
import { SettingsModule } from './common/settings/settings.module';
import {StandardlessStateViewModule} from './standardless-state-view/standardless-state-view.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: false
};

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpClientModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		ToolbarModule,
		FooterModule,
		StateViewModule,
		UpdateModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (HttpLoaderFactory),
				deps: [HttpClient]
			}
		}),
		/*FlexLayoutModule,*/
		PerfectScrollbarModule,
		ProductModule,
		ProductSelectionModule,
		ProductViewModule,
		ProductMapViewModule,
		ReleaseNotesModule,
		AssetLoaderModule,
		SearchThreadModule,
		ProductLocationModule,
		StandardlessStateViewModule
	],
	providers: [ElectronService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		}],
	bootstrap: [AppComponent]
})
export class AppModule { }
