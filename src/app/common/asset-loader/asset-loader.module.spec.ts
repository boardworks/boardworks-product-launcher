import { AssetLoaderModule } from './asset-loader.module';

describe('AssetLoaderModule', () => {
	let assetLoaderModule: AssetLoaderModule;

	beforeEach(() => {
		assetLoaderModule = new AssetLoaderModule();
	});

	it('should create an instance', () => {
		expect(assetLoaderModule).toBeTruthy();
	});
});
