import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetLoaderService } from './asset-loader.service';
import { AssetLoaderComponent } from './asset-loader.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [AssetLoaderComponent],
	exports: [AssetLoaderComponent],
	providers: [AssetLoaderService]
})
export class AssetLoaderModule { }
