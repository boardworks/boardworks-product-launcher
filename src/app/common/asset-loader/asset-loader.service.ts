import { Injectable, NgZone } from '@angular/core';
// import rxIpc from '../../../rx-ipc/renderer';
import { ElectronService } from '../electron/electron.module';
import { Observable, BehaviorSubject, of } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AssetLoaderService {

	public cache: Record<string, BehaviorSubject<Blob>> = {};

	constructor(private ngZone: NgZone, private electronService: ElectronService) {
		if (this.electronService && this.electronService.ipcRenderer) {
			this.electronService.ipcRenderer.on('load-asset-complete', this.response.bind(this));
		}
	}

	getId(path: string): string {
		return btoa(path);
	}

	load(path: string): Observable<any> {
		if (this.electronService && this.electronService.ipcRenderer) {
			const id: string = this.getId(path);
			if (this.cache[id] == null) {
				// Start loading
				this.cache[id] = new BehaviorSubject<Blob>(undefined);
				console.log(`Loading ${path}`);
				this.electronService.ipcRenderer.send('load-asset', path);
			} else if (this.cache[id].getValue() == null) {
				console.log(`Already loading... ${path}`);
			} else {
				console.log(`Already loaded: ${path}`);
			}
			return this.cache[id].asObservable();
		}
		return of(undefined);
	}

	response(event, path: string, data: Buffer, err: any) {
		const id: string = this.getId(path);
		this.ngZone.run(() => {
			if (this.cache[id] != null) {
				if (err) {
					console.log(`Failed to get asset: ${err}`);
					this.cache[id].error(err);
				} else {
					console.log(`Got asset ${path}`);
					this.cache[id].next(new Blob([Uint8Array.from(data)]));
				}
			}
		});
	}

	setupAssetLoaderThread() {
		if (this.electronService && this.electronService.ipcRenderer) {
			this.electronService.ipcRenderer.on('load-asset-buffer', this.thread_loadAssetBuffer.bind(this));
		}
	}

	thread_loadAssetBuffer(event, path: string): Promise<Buffer> {
		return this.electronService.loadFileAsBuffer(path).then((value: Buffer) => {
			if (this.electronService && this.electronService.ipcRenderer ) {
				this.electronService.ipcRenderer.send('load-asset-complete', path, value);
			}
			return value;
		}).catch((reason) => {
			if (this.electronService && this.electronService.ipcRenderer ) {
				this.electronService.ipcRenderer.send('load-asset-failed', path, reason);
			}
			return undefined;
		});
	}
}
