import { async, TestBed, inject } from '@angular/core/testing';

import { AssetLoaderService } from './asset-loader.service';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { AssetLoaderComponent } from './asset-loader.component';
import { ElectronService } from '../electron/electron.module';

describe('AssetLoaderService', () => {
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {
		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			declarations: [ AssetLoaderComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	it('should be created', inject([AssetLoaderService], (service: AssetLoaderService) => {
		expect(service).toBeTruthy();
	}));
});
