import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetLoaderComponent } from './asset-loader.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';

describe('AssetLoaderComponent', () => {
	let component: AssetLoaderComponent;
	let fixture: ComponentFixture<AssetLoaderComponent>;
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {
		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			declarations: [ AssetLoaderComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AssetLoaderComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
