import { Component, OnInit } from '@angular/core';
import { AssetLoaderService } from './asset-loader.service';
import { BehaviorSubject } from 'rxjs';

@Component({
	selector: 'bwfe-asset-loader',
	templateUrl: './asset-loader.component.html',
	styleUrls: ['./asset-loader.component.scss']
})
export class AssetLoaderComponent implements OnInit {

	public get cache(): Record<string, BehaviorSubject<Blob>> {
		return this.assetLoader.cache;
	}

	public get files(): string[] {
		const keys: string[] = Object.keys(this.cache);
		console.log(`Get files... ${keys}`);
		return keys;
	}

	constructor(private assetLoader: AssetLoaderService) {
	}

	ngOnInit() {
		this.assetLoader.setupAssetLoaderThread();
	}

}
