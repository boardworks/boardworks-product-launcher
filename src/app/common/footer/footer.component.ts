import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService, Product } from '../product/product.module';
import { Subscription, BehaviorSubject, Observable, of } from 'rxjs';
import { ElectronService } from '../electron/electron.module';
import { LocationService } from '../location/location.service';
import { distinctUntilChanged } from 'rxjs/operators';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { ProcessMemoryInfo } from 'electron';

@Component({
	selector: 'bwfe-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

	private subscriptions: Subscription[];

	productSubject: BehaviorSubject<Product> = new BehaviorSubject<Product>(undefined);
	product$: Observable<Product> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	productId: string;
	stateAbbreviation: string;

	public get hasUserGuide(): Observable<boolean> {
		return this.product$.pipe(
			switchMap((value) => {
				return value.hasUserGuide(this.electronService);
			})
		);
	}

	public get hasContentsGuide(): Observable<boolean> {
		return this.product$.pipe(
			switchMap((value) => {
				return value.hasContentsGuide(this.electronService);
			})
		);
	}

	constructor(public router: Router, private element: ElementRef, private route: ActivatedRoute, public productService: ProductService,
				public electronService: ElectronService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		/* /this.element.nativeElement.style['z-index'] = 101;*/
		this.subscriptions.push(this.route.params.subscribe(params => {
			this.productId = params['id'];
			this.stateAbbreviation = params['stateAbbreviation'];
			console.log(`footer PRODUCT IS: ${this.productId}`);
			console.log(params);
			if (this.productId) {
				this.productService.getProduct(this.productId).subscribe(value => {
					if (value != null) {
						console.log(`Footer got product: ${value.path}`);
						this.productSubject.next(value);
					}
				});
			}
		}));
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	changeProduct() {
		this.router.navigate(['/', this.productId]);
	}

	changeState() {
		/* this.subscriptions.push(this.productService.getProduct(this.productId).subscribe(product => {
			if (product.statesOnly.length > 1) {
				this.router.navigate(['./map', this.productId, this.stateAbbreviation]);
			} else {
				this.router.navigate(['./product', this.productId]);
			}
		})); */
		this.productService.navigateToProduct(this.productId, this.stateAbbreviation);
	}

	isRoot(): boolean {
		return this.productId == null; // this.router.url === '/';
	}

	isMap(): boolean {
		return this.router.url.startsWith('/map');
	}

	productStyle(): object {
		if (!this.isRoot() &&
			this.productSubject.getValue() != null &&
			this.productSubject.getValue().productColor != null) {
			return {
				'background-color': this.productSubject.getValue().productColor,
			};
		} else {
			return {};
		}
	}

	launchUserGuide(): Promise<void> {
		const userGuidePath: string = this.productSubject.getValue().getUserGuidePath(this.electronService);
		/*this.electronService.pathJoin(this.product$.getValue().path, 'notes',
			this.product$.getValue().userGuideFile);*/
		return this.electronService.openExternal(userGuidePath);
	}

	launchContentsGuide(): Promise<void> {
		const contentsGuidePath: string = this.productSubject.getValue().getContentsGuidePath(this.electronService);
		/*this.electronService.pathJoin(this.product$.getValue().path, 'notes',
			this.product$.getValue().contentsGuideFile);*/
		return this.electronService.openExternal(contentsGuidePath);
	}
}
