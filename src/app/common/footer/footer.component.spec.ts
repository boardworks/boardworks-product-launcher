import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ElectronTestingModule, ElectronTestingService,
		DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ProductTestingService } from '../product/testing/product-testing.service';
import { ProductService } from '../product/product.module';
import { ElectronService } from '../electron/electron.module';

describe('FooterComponent', () => {
	let component: FooterComponent;
	let fixture: ComponentFixture<FooterComponent>;
	let mockElectronService: ElectronTestingService;

	const mockProductService = new ProductTestingService().configure({products: []});

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				ElectronTestingModule,
			],
			declarations: [ FooterComponent ],
			providers: [
				{ provide: ProductService, useValue: mockProductService },
				{ provide: ElectronService, useValue: mockElectronService }
			],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FooterComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
