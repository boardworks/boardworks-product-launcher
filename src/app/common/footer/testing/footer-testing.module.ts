import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer-testing.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [FooterComponent],
	exports: [FooterComponent]
})
export class FooterTestingModule { }
