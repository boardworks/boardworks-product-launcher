import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer.component';
import { ProductLocationModule } from '../product-location/product-location.module';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		ProductLocationModule
	],
	exports: [FooterComponent],
	declarations: [FooterComponent],
})
export class FooterModule {

}

export * from './footer.component';
