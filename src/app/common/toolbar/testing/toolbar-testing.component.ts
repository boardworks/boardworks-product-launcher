import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
	selector: 'bwfe-toolbar',
	templateUrl: './toolbar-testing.component.html',
	styleUrls: ['./toolbar-testing.component.scss']
})
export class ToolbarComponent implements OnInit, AfterViewInit {

	get version(): string {
		return '0.0.0-testing';
	}

	constructor() {
	}

	isRoot(): boolean {
		return true;
	}

	ngOnInit() {

	}

	ngAfterViewInit() {

	}

}
