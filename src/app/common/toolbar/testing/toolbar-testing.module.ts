import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar-testing.component';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [ToolbarComponent],
	exports: [ToolbarComponent]
})
export class ToolbarTestingModule {

}

// export * from './toolbar.component';
