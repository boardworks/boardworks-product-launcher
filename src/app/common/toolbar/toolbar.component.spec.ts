import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, from } from 'rxjs';
import { ToolbarComponent } from './toolbar.component';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductTestingService } from '../product/testing/product-testing.service';
import { ProductService } from '../product/product.module';
import { UpdateTestingModule } from '../update/testing/update-testing.module';
import { ProductTestingModule } from '../product/testing/product-testing.module';
import { UpdateTestingService } from '../update/testing/update-testing.service';
import { UpdateService } from '../update/update.service';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';
import { SearchModule } from '../search/search.module';
import { SettingsModule } from '../settings/settings.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ToolbarComponent', () => {
	let component: ToolbarComponent;
	let fixture: ComponentFixture<ToolbarComponent>;
	const mockUpdateService = new UpdateTestingService();
	const mockProductService = new ProductTestingService().configure({products: []});
	const mockElectronSerice = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				ProductTestingModule,
				UpdateTestingModule,
				SearchModule,
				SettingsModule,
				HttpClientTestingModule
			],
			declarations: [ ToolbarComponent ],
			providers: [
				{
					provide: ActivatedRoute,
					useValue: {
						params: from([{id: 1}]),
					},
				},
				{ provide: ProductService, useValue: mockProductService },
				{ provide: UpdateService, useValue: mockUpdateService },
				{ provide: ElectronService, useValue: mockElectronSerice }
			],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ToolbarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should have a mock electron service', () => {
		expect(mockElectronSerice).toBeDefined();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
