import { Component, OnInit, NgZone, Input, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { ProductService, Product, State } from '../product/product.module';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { ElectronService } from '../electron/electron.module';
import { GitLabBuild, UpdateService } from '../update/update.service';
import { ContactDetailsService } from '../contact-details/contact-details.service';
import { LocationService } from '../location/location.service';

@Component({
	selector: 'bwfe-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, AfterViewInit, OnDestroy {

	private productSubscription: Subscription;
	productId: string;

	productSubject: BehaviorSubject<Product> = new BehaviorSubject(undefined);
	product$: Observable<Product> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	@Input() state?: State;

	get version(): string {
		return this.electronService.applicationVersion;
	}

	constructor(private element: ElementRef, private ngZone: NgZone, private route: ActivatedRoute,
				private router: Router, private updater: UpdateService,	public productService: ProductService,
				public electronService: ElectronService, public contactDetailsService: ContactDetailsService,
				private location: LocationService) {
	}

	isRoot(): boolean {
		return this.productId == null; // return this.router.url === '/';
	}

	ngOnInit() {
		this.element.nativeElement.style['z-index'] = 100;
		this.productSubscription = this.route.params.subscribe(params => {
			this.productId = params['id'];

			console.log(`Toolbar PRODUCT IS: ${this.productId}`);
			if (this.productId) {
				this.productService.getProduct(this.productId).subscribe(value => {
					this.productSubject.next(value);
				});
			}
		});
	}

	ngOnDestroy() {
		if (this.productSubscription != null) {
			this.productSubscription.unsubscribe();
		}
		this.productSubscription = null;
	}

	ngAfterViewInit() {

	}

}
