import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar.component';
import { UpdateModule } from '../update/update.module';
import { SearchModule } from '../search/search.module';
import { SettingsModule } from '../settings/settings.module';

@NgModule({
	imports: [
		CommonModule,
		UpdateModule,
		SearchModule,
		SettingsModule
	],
	exports: [ToolbarComponent],
	declarations: [ToolbarComponent],
})
export class ToolbarModule {

}

export * from './toolbar.component';
