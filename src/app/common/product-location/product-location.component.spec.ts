import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLocationComponent } from './product-location.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ProductTestingService } from '../product/testing/product-testing.module';
import { ProductService } from '../product/product.service';
import { ElectronService } from '../electron/electron.module';

describe('ProductLocationComponent', () => {
	let component: ProductLocationComponent;
	let fixture: ComponentFixture<ProductLocationComponent>;
	let mockElectronService: ElectronTestingService;

	const mockProductService = new ProductTestingService().configure({products: []});

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			declarations: [ ProductLocationComponent ],
			providers: [
				{ provide: ProductService, useValue: mockProductService },
				{ provide: ElectronService, useValue: mockElectronService }
			],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductLocationComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
