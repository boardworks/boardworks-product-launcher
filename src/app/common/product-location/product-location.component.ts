import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product/product.service';

@Component({
	selector: 'bwfe-product-location',
	templateUrl: './product-location.component.html',
	styleUrls: ['./product-location.component.scss']
})
export class ProductLocationComponent implements OnInit {

	selected = false;

	constructor(private productService: ProductService) { }

	ngOnInit() {
	}

	showLocationPanel() {
		this.selected = true;
	}
}
