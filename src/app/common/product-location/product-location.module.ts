import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductLocationComponent } from './product-location.component';

@NgModule({
	imports: [CommonModule],
	declarations: [ProductLocationComponent],
	exports: [ProductLocationComponent]
})
export class ProductLocationModule {

}
