import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchThreadComponent } from './search-thread.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [SearchThreadComponent],
	exports: [SearchThreadComponent],
	providers: []
})
export class SearchThreadModule { }
