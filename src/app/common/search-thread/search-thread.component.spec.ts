import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchThreadComponent } from './search-thread.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';

describe('SearchThreadComponent', () => {
	let component: SearchThreadComponent;
	let fixture: ComponentFixture<SearchThreadComponent>;
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			declarations: [ SearchThreadComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SearchThreadComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
