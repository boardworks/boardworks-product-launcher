import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search/search.service';
import { ElectronService } from '../electron/electron.module';
import { Observable } from 'rxjs';

@Component({
	selector: 'bwfe-search-thread',
	templateUrl: './search-thread.component.html',
	styleUrls: ['./search-thread.component.scss']
})
export class SearchThreadComponent implements OnInit {

	public get searchTerm$(): Observable<string> {
		return this.searchService.searchTerm$;
	}

	constructor(private searchService: SearchService, private electronService: ElectronService) { }

	ngOnInit() {
		this.searchService.setupSearchThread();
	}

}
