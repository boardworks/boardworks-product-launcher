import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
/*import { ActivatedRoute } from '@angular/router';*/

@Injectable({
	providedIn: 'root'
})
export class ContactDetailsService {

	showContactDetailsSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	showContactDetails$: Observable<boolean> = this.showContactDetailsSubject.asObservable().pipe(distinctUntilChanged());

	constructor(/*public route: ActivatedRoute*/) {
		/*this.route.url.subscribe(() => {
			console.log('Route change');
			this.hideContactDetails();
		});*/
	}

	showContactDetails() {
		this.showContactDetailsSubject.next(true);
	}

	hideContactDetails() {
		this.showContactDetailsSubject.next(false);
	}

	toggleContactDetails() {
		console.log(`Toggle contact details`);
		this.showContactDetailsSubject.next(!this.showContactDetailsSubject.getValue());
	}
}
