import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactDetailsComponent } from './contact-details.component';
import { ContactDetailsService } from './contact-details.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [ContactDetailsComponent],
	exports: [ContactDetailsComponent],
	providers: [ContactDetailsService]
})
export class ContactDetailsModule { }
