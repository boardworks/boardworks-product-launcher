import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { ContactDetailsService } from './contact-details.service';
import { Observable, Subscription } from 'rxjs';
import { ElectronService } from '../electron/electron.module';
import { HttpClient } from '@angular/common/http';
import { UpdateService } from '../update/update.service';
import { ProductLocale } from '../product/product';

@Component({
	selector: 'bwfe-contact-details',
	templateUrl: './contact-details.component.html',
	styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
	updateSubscription: Subscription;
	show$: Observable<boolean>;
	private _locale: ProductLocale;

	public usLocale: ProductLocale = ProductLocale.US;
	public ukLocale: ProductLocale = ProductLocale.UK;

	public get locale(): ProductLocale {
		return this._locale;
	}

	// This is the support email address
	public get emailAddress(): string {
		switch (this.locale) {
			case ProductLocale.US:
				return 'inquiries@boardworksed.com';
			case ProductLocale.UK:
				return 'enquiries@boardworks.co.uk';
		}
	}

	// This is the website address
	public get websiteAddress(): string {
		switch (this.locale) {
			case ProductLocale.US:
				return 'www.boardworkseducation.com';
			case ProductLocale.UK:
				return 'www.boardworks.co.uk';
		}
	}

	public get version(): string {
		return this.electronService.applicationVersion;
	}

	constructor(private element: ElementRef, public http: HttpClient, private updateService: UpdateService,
				public contactDetailsService: ContactDetailsService, private electronService: ElectronService) { }

	ngOnInit() {
		this.element.nativeElement.style['z-index'] = 200;
		this.show$ = this.contactDetailsService.showContactDetails$;
		this.setLocale(ProductLocale.US);
	}

	ngOnDestroy() {
		this.unsubscribeUpdate();
	}

	public close() {
		this.contactDetailsService.hideContactDetails();
	}

	public setLocale(locale: ProductLocale) {
		this._locale = locale;
	}

	public reportBug() {
		this.electronService.openExternal('https://gitlab.com/boardworks/boardworks-product-launcher/issues/new?issuable_template=bug');
	}

	public showReleaseNotes() {
		const version: string = this.electronService.applicationVersion;
		return this.http.get('assets/release-notes.md', {responseType: 'text'}).subscribe({
			next: value => {
				this.electronService.ipcRenderer.send('show-release-notes', version, value.toString());
			},
			error: reason => {
				console.error(`Failed to get Release Notes: ${reason}`);
			}
		});
	}

	public sendEmail() {
		this.electronService.openExternal(`mailto:${this.emailAddress}`);
	}

	public launchWebsite() {
		this.electronService.openExternal(this.websiteAddress);
	}

	unsubscribeUpdate() {
		if (this.updateSubscription && !this.updateSubscription.closed) {
			this.updateSubscription.unsubscribe();
		}
	}

	checkForUpdate() {
		this.unsubscribeUpdate();
		this.updateSubscription = this.updateService.checkForUpdate().subscribe();
	}
}
