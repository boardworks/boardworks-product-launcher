import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { GitLabBuild, UpdateService, UpdateDetails } from './update.service';
import { ElectronService } from '../electron/electron.service';

@Component({
	selector: 'bwfe-update',
	templateUrl: './update.component.html',
	styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit, OnDestroy {

	private _hasUpdate = false;
	private subscriptions: Subscription[];

	get update$(): Observable<UpdateDetails> {
		return this.updater.availableUpdateDetails$;
	}

	get hasUpdate(): boolean {
		return this._hasUpdate;
	}

	get isDownloading(): Observable<boolean> {
		return this.updater.availableUpdateDetails$.pipe(
			map<UpdateDetails, boolean>(details => {
				const res: boolean = (details.percent > 0 && details.percent < 100);
				return res;
			})
		);
	}

	get error$(): Observable<boolean> {
		return this.updater.updateError$;
	}

	constructor(private updater: UpdateService, private electronService: ElectronService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.subscriptions.push(this.updater.availableUpdateDetails$.subscribe(value => {
			if (value != null) {
				console.log(`Has Update`);
				this._hasUpdate = true;
			}
		}));
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	downloadUpdate() {
		this.updater.startDownload();
	}

	update() {
		this.updater.installUpdate();
	}

	showReleaseNotes() {
		this.updater.showReleaseNotes();
	}

	show() {
		this.updater.showUpdate();
	}

	openLogFile() {
		this.electronService.openLogFile();
	}
}
