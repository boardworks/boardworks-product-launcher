import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateComponent } from './update.component';
import { UpdateService } from './update.service';

@NgModule({
	imports: [
		CommonModule
	],
	exports: [UpdateComponent],
	declarations: [UpdateComponent],
	providers: [UpdateService]
})
export class UpdateModule { }
