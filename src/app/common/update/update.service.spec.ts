import { TestBed, inject, getTestBed } from '@angular/core/testing';

import { UpdateService } from './update.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';

describe('UpdaterService', () => {
	let injector: TestBed;
	let httpMock: HttpTestingController;
	let mockElectronService: ElectronTestingService;

	mockElectronService = new ElectronTestingService();
		mockElectronService.configure(DefaultElectronTestingConfiguration);

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [
				UpdateService,
				{ provide: ElectronService, useValue: mockElectronService }
			]
		});
		injector = getTestBed();
		httpMock = injector.get(HttpTestingController);
	});

	it('should be created', inject([UpdateService], (service: UpdateService) => {
		expect(service).toBeTruthy();
	}));
});
