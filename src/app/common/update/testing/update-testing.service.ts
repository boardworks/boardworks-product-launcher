import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { GitLabBuild, UpdateDetails } from '../update.service';

export const DefaultTestingGitLabBuild: GitLabBuild = {
	id: 1,
	status: 'success',
	stage: 'build',
	name: 'build',
	ref: 'v1.0.0-testing',
	tag: true,
	created_at: new Date(),
	started_at: new Date(),
	finished_at: new Date(),
	duration: 0,
	artifacts_file: { file: 'test.zip'}
};

export const DefaultTestingUpdateDetails: UpdateDetails = {
	available: true,
	portable: false,
	version: '1.0.0-testing',
	build: DefaultTestingGitLabBuild,
	url: 'https://gitlab.com/boardworks/boardworks-product-launcher',
	downloaded: true,
	release: '',
	releaseMd: '',
	percent: 0,
};

@Injectable({
	providedIn: 'root'
})
export class UpdateTestingService {

	static apiBaseUrl = 'https://gitlab.com/api/v4/projects/boardworks%2Fboardworks-product-launcher';

	availableUpdateBuildSubject: BehaviorSubject<GitLabBuild> = new BehaviorSubject<GitLabBuild>(undefined);
	availableUpdateBuild$: Observable<GitLabBuild> = this.availableUpdateBuildSubject.asObservable().pipe(distinctUntilChanged());

	availableUpdateDetailsSubject: BehaviorSubject<UpdateDetails> = new BehaviorSubject<UpdateDetails>(undefined);
	availableUpdateDetails$: Observable<UpdateDetails> = this.availableUpdateDetailsSubject.asObservable().pipe(distinctUntilChanged());

	constructor() {
	}

	/**
	 * Gets the current application version
	 */
	getCurrentVersion(): string {
		return '0.0.0-testing';
	}

	getBuilds(): Observable<GitLabBuild[]> {
		return of([DefaultTestingGitLabBuild]);
	}

/* 	getTagDetails(name: string): Observable<GitLabTagDetails> {
		const headers: HttpHeaders = new HttpHeaders().set('PRIVATE-TOKEN', '_VbzG1-5eRyJX6cUN1tz');
		return this.http.get<GitLabTagDetails>(
			`${UpdateService.apiBaseUrl}/repository/tags/${name}`, {headers} );
	} */

	getReleases(): Observable<GitLabBuild[]> {
		console.log(`Getting Releases...`);
		return this.getBuilds();
	}

	getUpdate(): Observable<GitLabBuild | undefined> {
		return this.getReleases().pipe(
			map<GitLabBuild[], GitLabBuild | undefined>(builds => {
				return builds[0];
			})
		);
	}

	getUpdateUrl(buildId: number): string {
		return `https://gitlab.com/api/v4/projects/boardworks%2Fboardworks-product-launcher/jobs/${buildId}/artifacts/release?job=build`;
		// return `https://gitlab.com/boardworks/boardworks-product-launcher/-/jobs/${buildId}/artifacts/raw/release?job=build`;
	}

	doUpdaterCheck(buildId: number) {
		// Get the autoupdater to check for an update at this location
	/* 	const updateUrl = this.getUpdateUrl(buildId);
		let channel: string;
		const prerelease: string[] = this.electronService.version().prerelease;
		if (prerelease.length > 0) {
			channel = prerelease[0];
		}
		this.electronService.ipcRenderer.on('update-downloaded', (event) => {
			console.log(`Update has been downloaded`);
			const build: GitLabBuild = this.availableUpdateBuildSubject.getValue();
			this.ngZone.run(() => {
				this.availableUpdateDetailsSubject.next({
					build,
					version: build.ref,
					url: updateUrl,
					downloaded: true,
					release: ''
				});
			});
		});

		this.electronService.ipcRenderer.on('update-available', (info) => {
			console.log('Update available to download');
			const build: GitLabBuild = this.availableUpdateBuildSubject.getValue();
			this.createUpdateDetails().subscribe(details => {
				this.ngZone.run(() => {
					this.availableUpdateDetailsSubject.next(details);
				});
			});
		}); */

		// Tell autoUpdater in electron process to check for update at this location
		// this.electronService.ipcRenderer.send('check-for-update', updateUrl, channel);
	}

	checkForUpdate() {
/* 		this.getUpdate().subscribe(build => {
			this.availableUpdateBuildSubject.next(build);
			this.doUpdaterCheck(build.id);
		}); */
	}

	startDownload() {
		/*this.electronService.ipcRenderer.on('download-progress', (event) => {
			console.log(event);
		});
		this.electronService.ipcRenderer.send('download-update');*/
	}

	installUpdate() {
		// this.electronService.ipcRenderer.send('install-update');
	}
}
