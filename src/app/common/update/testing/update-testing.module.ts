import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateComponent } from './update-testing.component';
import { UpdateTestingService } from './update-testing.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [UpdateComponent],
	exports: [UpdateComponent],
	providers: [
		UpdateTestingService
	]
})
export class UpdateTestingModule { }
