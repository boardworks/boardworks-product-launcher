import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from '../electron/electron.module';
import { Observable, BehaviorSubject, Subscriber, Observer, Subscription, of } from 'rxjs';
import { distinctUntilChanged, map, catchError, toArray, filter, take, tap, mergeMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest, HttpEventType } from '@angular/common/http';
import { SemVer, gt } from 'semver';
const { remote } = window.require('electron');
const log = remote.require('electron-log');
const parseLinkHeader = remote.require('parse-link-header');
const YAML = remote.require('yamljs');
// const copyUpdateScript = require('raw-loader!../../../assets/portable-copy-relaunch.txt');

export interface GitLabBuildPage {
	builds: GitLabBuild[];
	perPage: number;
	page: number;
	total: number;
	nextLink: string;
	prevLink: string;
}

export interface GitLabBuild {
	id: number;
	status: 'created' | 'pending' | 'running' | 'failed' | 'success' | 'canceled' | 'skipped' | 'manual';
	stage: string;
	name: string;
	ref: string;
	tag: boolean;
	created_at: Date;
	started_at: Date;
	finished_at: Date;
	duration: number;
	artifacts_file: any;
}

export interface GitLabTagDetails {
	name: string;
	message: string;
	release: string;
}

export interface UpdateDetails {
	available: boolean;
	error?: string;
	errorCode?: number;
	portable: boolean;
	build: GitLabBuild;
	version: string;
	url: string;
	downloaded: boolean;
	percent: number;
	release: string;
	releaseMd: string;
}

@Injectable({
	providedIn: 'root'
})
export class UpdateService {

	static apiBaseUrl = 'https://gitlab.com/api/v4/projects/boardworks%2Fboardworks-product-launcher';
	private headers: HttpHeaders = new HttpHeaders().set('PRIVATE-TOKEN', 'ecSgBUpPHGMJUyeneQ8K');
	private portableUpdateUrl: string;
	private portableExeName: string;
	private portableExeDownloadPath: string;

	availableUpdateBuildSubject: BehaviorSubject<GitLabBuild> = new BehaviorSubject<GitLabBuild>(undefined);
	availableUpdateBuild$: Observable<GitLabBuild> = this.availableUpdateBuildSubject.asObservable().pipe(distinctUntilChanged());

	availableUpdateDetailsSubject: BehaviorSubject<UpdateDetails> = new BehaviorSubject<UpdateDetails>(undefined);
	availableUpdateDetails$: Observable<UpdateDetails> = this.availableUpdateDetailsSubject.asObservable().pipe(distinctUntilChanged());

	checkingForUpdateSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	checkingForUpdate$: Observable<boolean> = this.checkingForUpdateSubject.asObservable().pipe(distinctUntilChanged());

	updateErrorSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	updateError$: Observable<boolean> = this.updateErrorSubject.asObservable().pipe(distinctUntilChanged());

	get portable(): boolean {
		return this.electronService.portable;
	}

	constructor(private http: HttpClient, private ngZone: NgZone, public electronService: ElectronService) {
		if (this.electronService != null && this.electronService.ipcRenderer != null) {
			this.electronService.ipcRenderer.on('start-update-check', this.startUpdateCheck.bind(this));
			this.electronService.ipcRenderer.send('updater-ready');
		}
	}

	/**
	 * Gets the current application version
	 */
	getCurrentVersion(): string {
		const s: SemVer = this.electronService.version();
		return s.version;
	}

	getPrerelease(): string[] {
		const s: SemVer = this.electronService.version();
		return s.prerelease;
	}

	/**
	 * Gets the successful builds from GitLab.
	 * This returns an observable stream of all successful builds. Because the api
	 * is paginated there can be several pages, resulting in multiple requests
	 * each returning a number of builds.
	 */
	getBuilds(): Observable<GitLabBuild> {
		return Observable.create(async (observer: Observer<GitLabBuild>) => {
			// let subscriptions: Subscription[] = [];
			// Get the first page of builds.
			let link = `${UpdateService.apiBaseUrl}/jobs?scope=success&per_page=50`;
			let start = true;
			let page: GitLabBuildPage;
			while (link || start) {
				start = false;
				page = await this.getNextBuildPage(link).catch((reason) => {
					observer.error(reason);
					return {builds: []} as GitLabBuildPage;
				});
				page.builds.forEach(build => observer.next(build));
				link = page.nextLink;
			}
			observer.complete();
		});
	}

	/**
	 * Requests the next page of successfull build results.
	 * @param link The link to the next page to request. This is returned from the previous
	 * request in the LinkHeader.
	 */
	async getNextBuildPage(link: string): Promise<GitLabBuildPage> {
		return new Promise<GitLabBuildPage>((resolve, reject) => {
			log.warn(`Getting builds from server: ${link}`);
			const subscription: Subscription = this.http.get(link, {headers: this.headers, observe: 'response'})
				.subscribe((response: HttpResponse<GitLabBuild[]>) => {
					// Get the link header
					const linkHeader: string = response.headers.get('Link');
					const parsedLink = parseLinkHeader(linkHeader);
					const page: GitLabBuildPage = {
						builds: response.body,
						nextLink: parsedLink.next ? parsedLink.next.url : undefined,
						prevLink: parsedLink.prev ? parsedLink.prev.url : undefined,
						perPage: parseInt(parsedLink.last.per_page, 10),
						page: parseInt(response.headers.get('X-Page'), 10),
						total: parseInt(response.headers.get('X-Total-Pages'), 10),
					};
					log.warn(`Got ${page.builds.length} builds`);
					resolve(page);
				},
				(err) => {
					reject(err);
				});
		});
	}

	/**
	 * Returns the release notes for a specified update.
	 * @param update The update to get the release notes from.
	 */
	getReleaseNotes(update: UpdateDetails): Observable<string> {
		const url = `${UpdateService.apiBaseUrl}/jobs/${update.build.id}/artifacts/release/RELEASE-NOTES.md`;
		return this.http.get(url, {responseType: 'text', headers: this.headers});
	}

	createReleaseNotes(md: string, details: UpdateDetails): string {
		console.log(`MD: ${md}`);
		// tslint:disable-next-line:max-line-length
		return `Release ${details.version}\n---\n**Update ${details.version}** contains these fixes and features:\n${this.sanitiseReleaseNotes(md)}`;
	}

	sanitiseReleaseNotes(md: string): string {
		// Remove commit links
		return md.replace(/\(\[([0-9a-f]+)\]\(([a-z0-9:\/\.\-]+)\)\)/g, '').replace(/\<a href/g, '<a target="_blank" href');
	}

	showReleaseNotes() {
		if (this.availableUpdateDetailsSubject.getValue() != null &&
			this.availableUpdateDetailsSubject.getValue().release != null) {
			const version: string = this.availableUpdateDetailsSubject.getValue().version;
			const md: string = this.availableUpdateDetailsSubject.getValue().releaseMd;
			this.electronService.ipcRenderer.send('show-release-notes', version, md);
		}
	}

	getReleases(): Observable<GitLabBuild[]> {
		return this.getBuilds().pipe(
			filter(build => {
				return build.tag && build.artifacts_file != null;
			}),
			toArray(),
			catchError((err, caught) => {
				log.error(`Error getting Releases: ${err}`);
				return Observable.throw(err);
			})
		);
	}

	getUpdate(): Observable<GitLabBuild[] | undefined> {

		return this.getReleases().pipe(
			map<GitLabBuild[], GitLabBuild[] | undefined>(builds => {

				log.debug(`Has ${builds.length} builds`);
				const current: string = this.getCurrentVersion();
				log.info(`Current version is: ${current}`);
				const selected: GitLabBuild[] = [];
				// Compare all the tags with the current version to find a suitable update
				builds.forEach(build => {
					// Compare this tag ref with the current version
					const buildVersion: string = build.ref;
					try {
						if (this.canUpdateFromChannel(buildVersion) && gt(buildVersion, current)) {
							// Is it also greater than the current candidate?
							if (selected.length > 0) {
								if (gt(buildVersion, selected[0].ref)) {
									selected.unshift(build);
								}
							} else {
								selected.push(build);
							}
						}
					} catch (err) {
						log.warn(`Error comparing version: ${err}`);
					}
				});
				if (selected.length > 0) {
					log.info(`Selected releases to try:`);
					selected.forEach(b => {
						log.info(b.ref);
					});
				} else {
					log.info(`No suitable updates found.`);
				}
				return selected;
			})
		);
	}

	getUpdateUrl(buildId: number): string {
		if (this.portable && this.portableUpdateUrl) {
			return this.portableUpdateUrl;
		} else {
			// return `https://gitlab.com/boardworks/boardworks-product-launcher/-/jobs/${buildId}/artifacts/raw/release?job=build`;
			return `https://gitlab.com/api/v4/projects/boardworks%2Fboardworks-product-launcher/jobs/${buildId}/artifacts/release?job=build`;
		}
	}

	createUpdateDetails(updateInfo: any, available: boolean = true): UpdateDetails {
		const build: GitLabBuild = this.availableUpdateBuildSubject.getValue();
		const updateUrl = build ? this.getUpdateUrl(build.id) : '';
		const version: string = build ? this.electronService.parseVersion(build.ref).version : '';
		const portable: boolean = updateInfo && updateInfo.portable != null ? updateInfo.portable : false;
		const details: UpdateDetails = {
			available,
			portable,
			build,
			version: version,
			url: updateUrl,
			downloaded: false,
			release: updateInfo && updateInfo.releaseNotes != null ? this.electronService.markdownToHtml(updateInfo.releaseNotes) : '',
			releaseMd: updateInfo && updateInfo.releaseNotes != null ? updateInfo.releaseNotes : '',
			percent: 0,
		};
		return details;
	}

	updateDownloaded() {
		log.info(`Update has been downloaded`);
		const details: UpdateDetails = this.availableUpdateDetailsSubject.getValue();
		details.percent = 100;
		details.downloaded = true;
		this.ngZone.run(() => {
			this.availableUpdateDetailsSubject.next(
				details
			);
		});
	}

	updateNotAvailable(info: any) {
		log.info(`No Update Available`);
		this.checkingForUpdateSubject.next(false);
		const details: UpdateDetails = this.createUpdateDetails(info, false);
		this.ngZone.run(() => {
			log.info(`Update: ${details.version}`);
			this.availableUpdateDetailsSubject.next(details);
		});
		return details;
	}

	updateAvailable(info: any): UpdateDetails {
		log.info(`Update Available`);
		this.checkingForUpdateSubject.next(false);
		const build: GitLabBuild = this.availableUpdateBuildSubject.getValue();
		const details: UpdateDetails = this.createUpdateDetails(info);
		this.ngZone.run(() => {
			log.info(`Update: ${details.version}`);
			this.availableUpdateDetailsSubject.next(details);
		});
		return details;
	}

	doUpdaterCheck(buildId: number): Observable<UpdateDetails> {
		return Observable.create((observer: Observer<UpdateDetails>) => {
			let portableSubscription: Subscription;
			// Get the autoupdater to check for an update at this location
			const updateUrl = this.getUpdateUrl(buildId);
			let channel = 'latest';
			const prerelease: string[] = this.electronService.version().prerelease;
			if (prerelease.length > 0) {
				channel = prerelease[0];
			}
			this.electronService.ipcRenderer.on('update-downloaded', this.updateDownloaded.bind(this));

			this.electronService.ipcRenderer.on('update-available', (event, info) => {
				observer.next(this.updateAvailable(info));
				observer.complete();
			});

			this.electronService.ipcRenderer.on('update-not-available', (event, info) => {
				observer.next(this.updateNotAvailable(info));
				observer.complete();
			});

			this.electronService.ipcRenderer.on('update-error', (event, error) => {
				this.ngZone.run(() => {
					this.updateErrorSubject.next(true);
				});
				observer.complete();
			});
			// Is this the portable version?
			if (this.portable) {
				const queryPosition = updateUrl.indexOf('?');
				const url = queryPosition === -1 ? updateUrl : updateUrl.substring(0, queryPosition);
				const ymlUrl = `${url}/${channel}.yml`; // `${url}/latest.yml`;
				// Get the 'latest.yml' file
				portableSubscription = this.http.get(ymlUrl, {headers: this.headers, responseType: 'text' as 'text'}).pipe(
					map(yml => {
						yml = yml.replace('\\uFEFF', '');
						const latest = YAML.parse(yml);
						console.log(`Path is: ${latest.path}`);
						this.portableExeName = latest.path.replace(/\s+Setup/g, '');
						const release: string = latest.releaseNotes ? this.sanitiseReleaseNotes(latest.releaseNotes) : undefined;
						return {
							url: `${url}/${this.portableExeName}`,
							releaseNotes: release,
							portable: true
						};
					})
				).subscribe({
					next: (value => {
						console.log(`Value: ${value.url}`);
						this.portableUpdateUrl = value.url;
						observer.next(this.updateAvailable(value));
						observer.complete();
					}),
					error: (err) => {
						log.error(err);
						observer.error(err);
					},
					complete: () => {
						log.info(`Update check complete.`);
						observer.complete();
					}
				});
			} else {
				// Do a normal auto-update
				// Tell autoUpdater in electron main process to check for update at this location
				this.electronService.ipcRenderer.send('check-for-update', updateUrl, channel);
			}

			return () => {
				if (portableSubscription) {
					portableSubscription.unsubscribe();
				}
			};
		});
	}

	canUpdateFromChannel(version: string) {
		let thisChannel = 'latest';
		const thisVersion: string = this.getCurrentVersion();
		const prerelease: string[] = this.getPrerelease();
		if (prerelease.length > 0) {
			thisChannel = prerelease[0];
		}
		let updateChannel = 'latest';
		const updatePrerelease: string [] = this.electronService.parseVersion(version).prerelease;
		if (updatePrerelease.length > 0) {
			updateChannel = updatePrerelease[0];
		}
		if (updateChannel === thisChannel) {
			log.info(`Application version ${thisVersion} is correct channel to update from ${version}`);
			return true;
		} else if (thisChannel === 'beta') {
			if (updateChannel === 'latest') {
				// tslint:disable-next-line:max-line-length
				log.info(`Application version ${thisVersion} on channel ${thisChannel} is correct channel to update from ${version} on channel ${updateChannel}`);
				return true;
			} else {
				log.info(`Application version ${thisVersion} on channel ${thisChannel} cannot update from ${version} on channel ${updateChannel}`);
				return false;
			}
		} else if (thisChannel === 'alpha') {
			if (updateChannel === 'beta' || updateChannel === 'latest') {
				// tslint:disable-next-line:max-line-length
				log.info(`Application version ${thisVersion} on channel ${thisChannel} is correct channel to update from ${version} on channel ${updateChannel}`);
				return true;
			} else {
				log.info(`Application version ${thisVersion} on channel ${thisChannel} cannot update from ${version} on channel ${updateChannel}`);
				return false;
			}
		}
		return false;
	}

	startUpdateCheck() {
		this.checkForUpdate().subscribe();
	}

	checkForUpdate(): Observable<UpdateDetails> {
		log.info(`Checking for updates`);
		this.checkingForUpdateSubject.next(true);
		this.updateErrorSubject.next(false);
		/*this.getUpdate().subscribe(build => {
			if (build != null) {
				this.availableUpdateBuildSubject.next(build);
				this.doUpdaterCheck(build.id);
			}
		});*/
		return this.getUpdate().pipe(
			mergeMap(build => {
				if (build && build.length > 0) {
					this.availableUpdateBuildSubject.next(build[0]);
				} else {
					this.availableUpdateBuildSubject.next(undefined);
				}
				if (build && build.length > 0) {
					return this.doUpdaterCheck(build[0].id);
				} else {
					return of(this.updateNotAvailable({portable: this.portable}));
				}
			}),
			catchError((err, caught: Observable<any>) => {
				return of(this.updateNotAvailable({error: err, available: false}));
			})
		);
	}

	startDownload() {
		this.downloadProgress(event, 0);
		if (this.portable) {
			this.startPortableDownload();
		} else {
			this.startInstallerDownload();
		}
	}

	startPortableDownload() {
		const request: HttpRequest<Buffer> = new HttpRequest('GET', this.portableUpdateUrl, {
			headers: this.headers,
			reportProgress: true,
			responseType: 'arraybuffer'
		});
		this.http.request<ArrayBuffer>(request).subscribe(event => {
			if (event.type === HttpEventType.DownloadProgress) {
				const percent: number = (Math.round(100 * event.loaded / event.total));
				this.downloadProgress(event, percent);
			}
			if (event.type === HttpEventType.Response) {
				// Save the new version in the temp folder
				this.portableExeDownloadPath = this.electronService.pathJoin(this.electronService.remote.app.getPath('temp'), this.portableExeName);
				const buffer: Buffer = Buffer.from(event.body);
				this.electronService.writeFile(this.portableExeDownloadPath, buffer).then((done) => {
					if (done) {
						this.updateDownloaded();
					}
				});
			}
		});
	}

	downloadProgress(event, percent) {
		const details: UpdateDetails = this.availableUpdateDetailsSubject.getValue();
		details.percent = percent;
		this.ngZone.run(() => {
			this.availableUpdateDetailsSubject.next(details);
		});
	}

	startInstallerDownload() {
		this.electronService.ipcRenderer.on('download-progress', this.downloadProgress.bind(this));
		this.electronService.ipcRenderer.send('download-update');
	}

	showUpdate() {
		if (this.portableExeDownloadPath) {
			this.electronService.shell.showItemInFolder(this.portableExeDownloadPath);
		}
	}

	installUpdate() {
		this.electronService.ipcRenderer.send('install-update');
	}
}
