import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { UpdateComponent } from './update.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';

describe('UpdateComponent', () => {
	let injector: TestBed;
	let component: UpdateComponent;
	let fixture: ComponentFixture<UpdateComponent>;
	let httpMock: HttpTestingController;
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService();
		mockElectronService.configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule
			],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService }
			],
			declarations: [ UpdateComponent ]
		}).compileComponents();
		injector = getTestBed();
		httpMock = injector.get(HttpTestingController);
	}));

	beforeEach(async(() => {
		fixture = TestBed.createComponent(UpdateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
