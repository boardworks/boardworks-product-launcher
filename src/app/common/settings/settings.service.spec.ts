import { async, TestBed, inject } from '@angular/core/testing';

import { SettingsService } from './settings.service';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';

describe('SettingsService', () => {
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			providers: [
				SettingsService,
				{ provide: ElectronService, useValue: mockElectronService }
			]
		});
	}));

	it('should be created', inject([SettingsService], (service: SettingsService) => {
		expect(service).toBeTruthy();
	}));
});
