import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { ProductLocationSettingsComponent } from './product-location-settings/product-location-settings.component';
import { PipesModule } from '../pipes/pipes.module';
import { SettingsService } from './settings.service';

@NgModule({
	imports: [
		CommonModule,
		PipesModule
	],
	providers: [
		SettingsService
	],
	declarations: [SettingsComponent, ProductLocationSettingsComponent],
	exports: [SettingsComponent]
})
export class SettingsModule { }
