import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactDetailsService } from '../contact-details/contact-details.service';
import { UpdateService, UpdateDetails } from '../update/update.service';
import { Observable, Subscription } from 'rxjs';
import { SettingsService } from './settings.service';
import { ProductLocationSettingsComponent } from './product-location-settings/product-location-settings.component';

@Component({
	selector: 'bwfe-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

	show$: Observable<boolean>;
	updateSubscription: Subscription;

	constructor(private contactDetailsService: ContactDetailsService, private updateService: UpdateService,
		private settingsService: SettingsService) { }

	ngOnInit() {
		this.show$ = this.settingsService.showSettings$;
		this.settingsService.addSettingMenuEntry(ProductLocationSettingsComponent.entryName);
	}

	ngOnDestroy() {
		this.unsubscribeUpdate();
	}

	openSettings() {
		this.settingsService.showSettings();
	}

	closeSettings() {
		this.settingsService.hideSettings();
	}

	unsubscribeUpdate() {
		if (this.updateSubscription && !this.updateSubscription.closed) {
			this.updateSubscription.unsubscribe();
		}
	}

	checkForUpdate() {
		this.unsubscribeUpdate();
		this.updateSubscription = this.updateService.checkForUpdate().subscribe();
	}
}
