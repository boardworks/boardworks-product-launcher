import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLocationSettingsComponent } from './product-location-settings.component';
import { PipesModule } from '../../pipes/pipes.module';
import { ProductTestingService } from '../../product/testing/product-testing.module';
import { ProductService } from '../../product/product.module';
import { ElectronService } from '../../electron/electron.module';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../../electron/testing/electron-testing.module';

describe('ProductLocationSettingsComponent', () => {
	let component: ProductLocationSettingsComponent;
	let fixture: ComponentFixture<ProductLocationSettingsComponent>;
	let mockElectronService: ElectronTestingService;

	const mockProductService = new ProductTestingService().configure({products: []});

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			imports: [
				PipesModule
			],
			declarations: [ ProductLocationSettingsComponent ],
			providers: [
				{ provide: ProductService, useValue: mockProductService },
				{ provide: ElectronService, useValue: mockElectronService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductLocationSettingsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
