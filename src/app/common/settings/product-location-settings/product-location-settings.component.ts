import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product/product.module';
import { SettingsService } from '../settings.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { ElectronService } from '../../electron/electron.module';
const { remote } = window.require('electron');
const dialog = remote.require('electron').dialog;
const log = remote.require('electron-log');
@Component({
	selector: 'bwfe-product-location-settings',
	templateUrl: './product-location-settings.component.html',
	styleUrls: ['./product-location-settings.component.scss']
})
export class ProductLocationSettingsComponent implements OnInit {
	static entryName = 'product-location';
	show$: Observable<boolean>;
	errorSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	error$: Observable<boolean> = this.errorSubject.asObservable().pipe(distinctUntilChanged());

	constructor(public productService: ProductService, public settingsService: SettingsService, private electronService: ElectronService) {
	}

	ngOnInit() {
		this.show$ = this.settingsService.getSettingEntryShow(ProductLocationSettingsComponent.entryName);
	}
/*
	resetUserLocation() {
		// this.productService.resetProductLocations();
		this.settingsService.setUserProductLocations
		this.productService.reloadProducts();
	}*/

	globalRemoveLocation(...paths: string[]) {
		this.errorSubject.next(false);
		this.settingsService.removeGlobalProductLocations(...paths).subscribe({
			next: (result => {
				if (result) {
					log.info(`Successfully removed locations from global settings`);
				} else {
					log.info(`Failed to remove locations from global settings`);
				}
			}),
			error: (err) => {
				log.error(`Error removing location from global settings: ${err}`);
				this.errorSubject.next(true);
			}
		});
	}

	globalAddNewLocation() {
		this.errorSubject.next(false);
		const paths: string[] = dialog.showOpenDialog({
			properties: ['openDirectory', 'multiSelections']
		});
		// this.productService.addProductLocation(...paths);
		this.settingsService.addGlobalProductLocations(...paths).subscribe({
			next: (result => {
				if (result) {
					log.info(`Successfully added custom locations from global settings`);
				} else {
					log.info(`Failed to add custom locations to global settings`);
				}
			}),
			error: (err) => {
				log.error(`Error adding custom location to global settings: ${err}`);
				this.errorSubject.next(true);
			}
		});
	}

	globalAddDefaultLocation() {
		this.errorSubject.next(false);
		// this.productService.addProductLocation(this.productService.defaultProductLocation);
		this.settingsService.addGlobalProductLocations(...this.settingsService.defaultProductLocations).subscribe({
			next: (result => {
				if (result) {
					log.info(`Successfully added custom locations from global settings`);
				} else {
					log.info(`Failed to add custom locations to global settings`);
				}
			}),
			error: (err) => {
				log.error(`Error adding custom location to global settings: ${err}`);
				this.errorSubject.next(true);
			}
		});
	}

	userRemoveLocation(...paths: string[]) {
		this.errorSubject.next(false);
		// this.productService.removeProductLocation(path);
		console.log(`Remove paths: ${paths}`);
		this.settingsService.removeUserProductLocations(...paths).subscribe({
			next: (result => {
				if (result) {
					log.info(`Successfully removed locations from user settings`);
				} else {
					log.info(`Failed to remove locations from user settings`);
				}
			}),
			error: (err) => {
				log.error(`Error removing location from user settings: ${err}`);
				this.errorSubject.next(true);
			}
		});
	}

	userAddNewLocation() {
		this.errorSubject.next(false);
		const paths: string[] = dialog.showOpenDialog({
			properties: ['openDirectory', 'multiSelections']
		});
		// this.productService.addProductLocation(...paths);
		this.settingsService.addUserProductLocations(...paths).subscribe({
			next: (result => {
				if (result) {
					log.info(`Successfully added custom locations from user settings`);
				} else {
					log.info(`Failed to add custom locations to user settings`);
				}
			}),
			error: (err) => {
				log.error(`Error adding custom location to user settings: ${err}`);
				this.errorSubject.next(true);
			}
		});
	}

	userAddDefaultLocation() {
		this.errorSubject.next(false);
		this.settingsService.addUserProductLocations(...this.settingsService.defaultProductLocations).subscribe({
			next: (result => {
				if (result) {
					log.info(`Successfully added default location to user settings`);
				} else {
					log.info(`Failed to add default location to user settings`);
				}
			}),
			error: (err) => {
				log.error(`Error adding default location from user settings: ${err}`);
				this.errorSubject.next(true);
			}
		});
	}

	toggleSettings() {
		this.settingsService.toggleSettingsMenuEntry(ProductLocationSettingsComponent.entryName);
	}

	openLogFile() {
		this.electronService.openLogFile();
	}
}
