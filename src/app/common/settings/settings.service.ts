import { Injectable } from '@angular/core';
import { ElectronService } from '../electron/electron.module';
import { BehaviorSubject, Observable, Subscriber, of, zip, from } from 'rxjs';
import { distinctUntilChanged, map, tap, take } from 'rxjs/operators';
import { platform } from 'os';

const { remote } = window.require('electron');
const log = remote.require('electron-log');
export interface Settings {
	productLocations: string[];
}

export interface ApplicationSettings {
	userSettings: Settings;
	globalSettings: Settings;
}

export interface SettingsMenuEntry {
	name: string;
	showSubject: BehaviorSubject<boolean>;
}

@Injectable({
	providedIn: 'root'
})
export class SettingsService {

	settingsMenuEntries: SettingsMenuEntry[] = [];

	showSettingsSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	showSettings$: Observable<boolean> = this.showSettingsSubject.asObservable().pipe(distinctUntilChanged());

	get defaultSettings(): Settings {
		return {
			productLocations: [...this.defaultProductLocations]
		};
	}

	get emptySettings(): Settings {
		return {
			productLocations: []
		};
	}

	get defaultProductLocations(): string[] {
		const platformType = platform();
		if (platformType === 'win32') {
			let programFiles: string = this.electronService.environmentVariable('PROGRAMFILES');
			if ( this.electronService.environmentVariable('PROCESSOR_ARCHITECTURE') === 'AMD64') {
				programFiles = this.electronService.environmentVariable('PROGRAMFILES(X86)');
			}
			const bwFolder = this.electronService.pathJoin(programFiles, 'Boardworks');
			return [bwFolder];
		} else if (platformType === 'darwin') {
			const homeDir = this.electronService.environmentVariable('HOME');
			const bwFolder = this.electronService.pathJoin(homeDir, 'Applications', 'Boardworks');
			return [bwFolder];
		} else {
			return [];
		}
	}

	get hasGlobalSettings(): boolean {
		return this.globalSettingsDirectory != null;
	}

	settingsSubject: BehaviorSubject<ApplicationSettings> = new BehaviorSubject<ApplicationSettings>(undefined);
	settings$: Observable<ApplicationSettings> = this.settingsSubject.asObservable();

	productLocations$: Observable<string[]> = this.settings$.pipe(map((settings: ApplicationSettings) => {
		if (settings != null) {
			let locations: string[] = [].concat(settings.globalSettings.productLocations, settings.userSettings.productLocations);
			locations = locations.filter((l, index, self) => {
				return self.indexOf(l) === index;
			});
			return locations;
		}
		log.warn(`Settings are null`);
		return [];
	}));

	globalProductLocations$: Observable<string[]> = this.settings$.pipe(map((settings: ApplicationSettings) => {
		return settings.globalSettings.productLocations.slice(0);
	}));

	userProductLocations$: Observable<string[]> = this.settings$.pipe(map((settings: ApplicationSettings) => {
		return settings.userSettings.productLocations.slice(0);
	}));

	get globalSettingsDirectory(): string {
		return this.electronService.environmentVariable('PORTABLE_EXECUTABLE_DIR');
	}

	get globalSettingsPath(): string {
		const dir: string = this.globalSettingsDirectory;
		if (dir != null) {
			return this.electronService.pathJoin(dir, 'settings.json');
		}
		return undefined;
	}

	get userSettingsPath(): string {
		return this.electronService.pathJoin(this.electronService.userDataPath, 'settings.json');
	}

	constructor(private electronService: ElectronService) {
		// Load the settings
		this.loadSettings();
	}

	getSettingsMenuEntry(name: string): SettingsMenuEntry {
		const foundEntry: SettingsMenuEntry = this.settingsMenuEntries.find((entry: SettingsMenuEntry) => {
			return entry.name === name;
		});
		return foundEntry;
	}

	hasSettingsMenuEntry(name: string): boolean {
		return this.getSettingsMenuEntry(name) != null;
	}

	addSettingMenuEntry(name: string) {
		if (!this.hasSettingsMenuEntry(name)) {
			this.settingsMenuEntries.push({
				name,
				showSubject: new BehaviorSubject<boolean>(false)
			});
		}
	}

	toggleSettingsMenuEntry(name: string) {
		const settings: SettingsMenuEntry = this.getSettingsMenuEntry(name);
		if (settings != null) {
			settings.showSubject.next(!settings.showSubject.getValue());
		}
	}

	showSettingsMenuEntry(name: string) {
		console.log(`Getting settings for ${name}`);
		const settings: SettingsMenuEntry = this.getSettingsMenuEntry(name);
		if (settings != null) {
			settings.showSubject.next(true);
		} else {
			console.log(`Not found`);
		}
	}

	hideSettingsMenuEntry(name: string) {
		const settings: SettingsMenuEntry = this.getSettingsMenuEntry(name);
		if (settings != null) {
			settings.showSubject.next(false);
		}
	}

	getSettingEntryShow(name: string): Observable<boolean> {
		const settings: SettingsMenuEntry = this.getSettingsMenuEntry(name);
		if (settings != null) {
			return settings.showSubject.asObservable();
		}
	}

	toggleSettings() {
		console.log(`Toggle settings`);
		this.showSettingsSubject.next(!this.showSettingsSubject.getValue());
	}

	showSettings() {
		this.showSettingsSubject.next(true);
	}

	hideSettings() {
		this.showSettingsSubject.next(false);
	}

	getProductLocations(): string[] {
		const settings: ApplicationSettings = this.settingsSubject.getValue();
		if (settings != null) {
			let locations: string[] = [].concat(settings.globalSettings.productLocations, settings.userSettings.productLocations);
			locations = locations.filter((l, index, self) => {
				return self.indexOf(l) === index;
			});
			return locations;
		}
		return [];
	}


	loadSettings() {
		const appSettings: ApplicationSettings = {
			userSettings: this.emptySettings,
			globalSettings: this.emptySettings
		};
		let globalStream: Observable<Settings>;
		let userStream: Observable<Settings>;
		if (this.globalSettingsPath != null) {
			log.warn(`Loading portable settings from ${this.globalSettingsPath}`);
			globalStream = from(this.electronService.readFile(this.globalSettingsPath).then((value: string) => {
				const s: Settings = JSON.parse(value);
				log.warn(`Global locations (${s.productLocations.length}): ${s.productLocations}`);
				return s;
			}).catch(reason => {
				log.warn(`Unable to load global settings from ${this.globalSettingsPath}: ${reason}`);
				return this.emptySettings;
			}));
		} else {
			globalStream = of(this.emptySettings);
		}
		log.warn(`Loading user settings from ${this.userSettingsPath}`);
		userStream = from(this.electronService.readFile(this.userSettingsPath).then((value: string) => {
			const s: Settings = JSON.parse(value);
			log.warn(`User locations (${s.productLocations.length}): ${s.productLocations}`);
			return s;
		}).catch(reason => {
			log.warn(`Unable to load user settings from ${this.userSettingsPath}: ${reason}`);
			return this.defaultSettings;
		}));
		zip(globalStream, userStream).pipe(
			map<[Settings, Settings], ApplicationSettings>(value => {
				return {
					globalSettings: {
						productLocations: value[0].productLocations.slice(),
					},
					userSettings: {
						productLocations: value[1].productLocations.slice()
					}
				};
			}),
			// Apply settings to subject
			tap(settings => {
				this.settingsSubject.next(settings);
			}),
			take(1)
		).subscribe();
	}

	addUserProductLocations(...locations: string[]): Observable<boolean> {
		let list: string[] = this.settingsSubject.getValue().userSettings.productLocations.slice(0);
		list.push(...locations);
		list = list.filter((value, index, self) => {
			return self.indexOf(value) === index;
		});
		// If the array has changed set the new value
		const isSame: boolean = list.every((value => {
			return this.settingsSubject.getValue().userSettings.productLocations.includes(value);
		}));
		if (!isSame || this.settingsSubject.getValue().userSettings.productLocations.length !== list.length) {
			return this.setUserProductLocations(list);
		}
		return of(true);
	}

	removeUserProductLocations(...locations: string[]): Observable<boolean> {
		log.warn(`Removing product location from user settings: ${locations}`);
		let list: string[] = this.settingsSubject.getValue().userSettings.productLocations.slice(0);
		list = list.filter((value, index, self) => {
			return !locations.includes(value);
		});
		// If the array has changed set the new value
		const isSame: boolean = list.every((value => {
			return this.settingsSubject.getValue().userSettings.productLocations.includes(value);
		}));
		if (!isSame || this.settingsSubject.getValue().userSettings.productLocations.length !== list.length) {
			return this.setUserProductLocations(list);
		}
		return of(true);
	}

	setUserProductLocations(locations: string[]): Observable<boolean> {
		const userSettings: Settings = Object.assign({}, this.settingsSubject.getValue().userSettings);
		userSettings.productLocations = locations;
		return this.setUserSettings(userSettings);
	}

	addGlobalProductLocations(...locations: string[]): Observable<boolean> {
		let list: string[] = this.settingsSubject.getValue().globalSettings.productLocations.slice(0);
		list.push(...locations);
		list = list.filter((value, index, self) => {
			return self.indexOf(value) === index;
		});
		// If the array has changed set the new value
		const isSame: boolean = list.every((value => {
			return this.settingsSubject.getValue().globalSettings.productLocations.includes(value);
		}));
		if (!isSame || this.settingsSubject.getValue().globalSettings.productLocations.length !== list.length) {
			return this.setGlobalProductLocations(list);
		}
		return of(true);
	}

	removeGlobalProductLocations(...locations: string[]): Observable<boolean> {
		log.warn(`Removing product location from portable settings: ${locations}`);
		let list: string[] = this.settingsSubject.getValue().globalSettings.productLocations.slice(0);
		list = list.filter((value, index, self) => {
			return !locations.includes(value);
		});
		// If the array has changed set the new value
		const isSame: boolean = list.every((value => {
			return this.settingsSubject.getValue().globalSettings.productLocations.includes(value);
		}));
		if (!isSame || this.settingsSubject.getValue().globalSettings.productLocations.length !== list.length) {
			return this.setGlobalProductLocations(list);
		}
		return of(true);
	}

	setGlobalProductLocations(locations: string[]): Observable<boolean> {
		const globalSettings: Settings = this.settingsSubject.getValue().globalSettings;
		globalSettings.productLocations = locations;
		return this.setGlobalSettings(globalSettings);
	}

	setUserSettings(settings: Settings): Observable<boolean> {
		return Observable.create((subscriber: Subscriber<boolean>) => {
			// Write the settings to file
			const settings_str: string = JSON.stringify(settings);
			log.warn(`Writing user settings to ${this.userSettingsPath}`);
			this.electronService.writeFile(this.userSettingsPath, settings_str).then(success => {
				if (success) {
					const appSettings = this.settingsSubject.getValue();
					appSettings.userSettings = settings;
					this.settingsSubject.next(appSettings);
					subscriber.next(true);
					subscriber.complete();
				} else {
					log.warn(`Failed to write settings to: ${this.userSettingsPath}`);
					subscriber.next(false);
					subscriber.complete();
				}
			}).catch(reason => {
				log.error(`Error saving user settings to ${this.userSettingsPath}: ${reason}`);
				subscriber.error(reason);
				subscriber.complete();
			});
		});
	}

	setGlobalSettings(settings: Settings) {
		return Observable.create((subscriber: Subscriber<boolean>) => {
			// Write the settings to file
			const settings_str: string = JSON.stringify(settings);
			log.warn(`Writing portable settings to: ${this.globalSettingsPath}`);
			this.electronService.writeFile(this.globalSettingsPath, settings_str).then(success => {
				if (success) {
					const appSettings = this.settingsSubject.getValue();
					appSettings.globalSettings = settings;
					this.settingsSubject.next(appSettings);
					subscriber.next(true);
					subscriber.complete();
				} else {
					log.warn(`Failed to write settings to: ${this.globalSettingsPath}`);
					subscriber.next(false);
					subscriber.complete();
				}
			}).catch(reason => {
				log.error(`Error saving global portable settings to ${this.globalSettingsPath}: ${reason}`);
				subscriber.error(reason);
				subscriber.complete();
			});
		});
	}
}
