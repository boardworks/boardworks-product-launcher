import { Injectable, EventEmitter } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote, shell, IpcRenderer } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import { Observable, Observer, Subscriber, config, of, merge, from } from 'rxjs';
import * as semver from 'semver';
import * as Showdown from 'showdown';
import * as iconv from 'iconv-lite';
import { asyncFilter } from '../electron.service';
import { map } from 'rxjs/operators';
// import { Glob } from 'glob';

export interface ElectronTestingConfiguration {
	files: Record<string, Buffer | string>;
	env: Record<string, string>;
	electron: boolean;
	userDataPath: string;
	applicationPath: string;
	applicationName: string;
	pathSep: string;
}

export const programFilesEnv = 'PROGRAMFILES(X86)';

export const programFilesPath = `C:\\Program Files (x86)`;

export const DefaultElectronTestingConfiguration: ElectronTestingConfiguration = {
	files: {
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\Activities.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/Activities.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\BTMapping.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/BTMapping.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\BTStandards.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/BTStandards.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\OHMapping.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/OHMapping.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\OHStandards.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/OHStandards.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\PAMapping.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/PAMapping.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\PAStandards.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/PAStandards.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\ProductProps.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/ProductProps.xml'),
		'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\states.xml':
			require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/states.xml'),
	},
	env: {
		'PROGRAMFILES(X86)': programFilesPath,
	},
	electron: true,
	userDataPath: '',
	applicationPath: '',
	applicationName: 'Boardowrks.exe',
	pathSep: '\\'
};

/* class MockIpcRenderer extends Electron.EventEmitter {

	on(channel: string, listener: Function): this {
		return this;
	}

	once(channel: string, listener: Function): this {
		return this;
	}

	removeAllListeners(channel: string): this {
		return this;
	}

	removeListener(channel: string, listener: Function): this {
		return this;
	}

	send(channel: string, ...args: any[]): void {
	}

	sendSync(channel: string, ...args: any[]): any {
		return null;
	}

	sendTo(windowId: number, channel: string, ...args: any[]): void {

	}

	sendToHost(channel: string, ...args: any[]): void {

	}
} */

// @Injectable()
export class ElectronTestingService {

	ipcRenderer: typeof ipcRenderer;
	webFrame: typeof webFrame;
	remote: typeof remote;
	childProcess: typeof childProcess;
	fs: typeof fs;
	path: typeof path;
	shell: typeof shell;
	semver: typeof semver;
	showdown: typeof Showdown;
	iconv: typeof iconv;
	// glob: typeof Glob;

	configuration: ElectronTestingConfiguration;

	get F_OK(): number {
		return 0;
	}

	get R_OK(): number {
		return 4;
	}

	get W_OK(): number {
		return 2;
	}

	get X_OK(): number {
		return 1;
	}

	get applicationVersion(): string {
		return '0.0.0-testing';
	}

	get userDataPath(): string {
		return this.configuration.userDataPath;
	}

	get applicationPath(): string {
		return this.configuration.applicationPath;
	}

	get applicationName(): string {
		return this.configuration.applicationName;
	}

	get pathSep(): string {
		return this.configuration.pathSep;
	}

	get downloadedContentPath(): string {
		if (this.portable) {
			return this.pathJoin(this.environmentVariable('PORTABLE_EXECUTABLE_DIR'), 'boardworks-downloaded-content');
		} else {
			return this.pathJoin(this.applicationPath, 'downloaded-content');
		}
	}

	get portable(): boolean {
		return this.environmentVariable('PORTABLE_EXECUTABLE_DIR') != null;
	}

	constructor() {
		this.configure({
			files: {},
			env: {},
			electron: true,
			userDataPath: '',
			applicationPath: '',
			applicationName: '',
			pathSep: '\\'
		});
		// this.ipcRenderer = new MockIpcRenderer();
	}

	parseVersion(versionString: string): semver.SemVer {
		return new this.semver.SemVer(versionString);
	}

	version(): semver.SemVer {
		return this.parseVersion(this.applicationVersion);
	}

	configure(configuration: ElectronTestingConfiguration): ElectronTestingService {
		this.configuration = configuration;
		return this;
	}

	isElectron = () => {
		return this.configuration.electron;
	}

	environmentVariable(name: string): string {
		return  this.configuration.env[name];
	}

	markdownToHtml(markdown: string): string {
		return '';
	}

	filesInFolders(folders: string[]): Observable<string> {
		const files: Observable<string>[] = folders.map(value => {
			return this.filesInFolder(value);
		});
		return merge(...files);
	}

	fileListInFolders(folders: string[], includeSelf: boolean = false, directoriesOnly: boolean = false): Promise<string[]> {
		return new Promise<string[]>((resolve, reject) => {
			const files: Promise<string[]>[] = folders.map(value => {
				console.log(`Get Files in ${value}`);
				return this.filesListInFolder(value, includeSelf, directoriesOnly);
			});
			let allFiles: string[] = [];
			Promise.all(files).then((groups: string[][]) => {
				groups.forEach(group => {
					allFiles = allFiles.concat(group);
				});
				resolve(allFiles);
			});
		});
	}

	filesInFolder(folder): Observable<string> {
		return Observable.create((observer: Observer<string>) => {
			const files: string[] = this.getFilesInDir(folder);
			files.forEach(value => {
				observer.next(this.pathJoin(folder, value));
			});
		});
	}

	stat(filePath: string): Promise<fs.Stats> {
		return new Promise((resolve, reject) => {
			reject('stat not supported in test');
		});
	}

	/**
	 * Returns an Observable of the array of files in a specified directory.
	 * Each file entry is the full path of that file.
	 * @param folder The folder to get the contained files for.
	 */
	filesListInFolder(folder, includeFolder: boolean = false, directoriesOnly: boolean = false): Promise<string[]> {
		return new Promise<string[]>(async (resolve, reject) => {
			console.log(`Read dir ${folder}`);
			if (this.isDirectory(folder)) {
				this.fs.readdir(folder, async (err, files) => {
					if (err) {
						console.warn(`Error ${err}`);
						reject(err);
						return;
					}
					let fullPaths: string[] = files.map((value: string) => {
						const p: string = this.path.join(folder, value);
						return p;
					});
					if (includeFolder) {
						fullPaths.push(folder);
					}
					console.log(`directoriesOnly: ${directoriesOnly}`);
					if (directoriesOnly) {
						console.log(`Filtering non directories from: ${fullPaths}`);
						fullPaths = await asyncFilter<string>(fullPaths, async p => {
							const isDir = this.isDirectory(p);
							console.log(`${p} is directory: ${isDir}`);
							return isDir;
						});
						console.log(`Got: ${fullPaths}`);
					}
					console.log(fullPaths);
					resolve(fullPaths);
				});
			} else {
				resolve([]);
			}
		});
	}

	findFilesList(pattern: string | RegExp, cwd: string): Observable<string[]> {
		return from(this.filesListInFolder(cwd)).pipe(
			map(files => {
				const reg: RegExp = new RegExp(pattern);
				files.filter(file => reg.test(file));
				return files;
			})
		);
	}

	findFiles(pattern: string, cwd?: string): Observable<string> {
		return of(undefined);
	}

	fileExists(file: string): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			this.access(file).then(err => {
				if (err == null) {
					resolve(true);
				} else {
					resolve(false);
				}
			});
		});
	}

	readFile(file: string): Promise<Buffer | string> {
		return new Promise<Buffer | string>((resolve, reject) => {
			const data: Buffer | string | undefined = this.getFile(file);
			if (data != null) {
				resolve(data);
			} else {
				reject(new Error('File does not exist'));
			}
		});
	}

	readdir(dirPath: string): Promise<string[]> {
		return new Promise<string[]>((resolve) => {
			const files: string[] = this.getFilesInDir(dirPath);
			resolve(files);
		});
	}

	writeFile(file: string, content: string | Buffer, encoding: string = 'utf8'): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			this.addFile(file, content);
			resolve(true);
		});
	}

	copyFile(src: string, dest: string, flags: number = 0): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			this.readFile(src).then(value => {
				this.addFile(dest, value);
				resolve(true);
			}).catch(reason => reject(reason));
		});
	}

	access(file: string, mode?: number): Promise<NodeJS.ErrnoException> {
		return new Promise((resolve) => {
			const data: Buffer | string | undefined = this.getFile(file);
			if (data != null) {
				resolve(undefined);
			} else {
				resolve(new Error('File does not exist'));
			}
		});
	}

	pathJoin(...paths: string[]): string {
		return paths.join('\\');
	}

	openExternal(file: string): Promise<void> {
		return Promise.reject();
	}

	encodeFileAsBase64(file: string): Promise<string> {
		return new Promise((resolve, reject) => {
			console.error(`Load file: ${file}`);
			const data = this.getFile(file);
			if (data == null) {
				reject(new Error('File does not exist'));
			} else {
				if (data instanceof Buffer) {
					resolve(data.toString('base64'));
				} else {
					return btoa(data);
				}
			}
		});
	}

	loadFileAsBlob(file: string): Promise<Blob> {
		return new Promise((resolve, reject) => {
			const data = this.getFile(file);
			if (data == null) {
				reject(new Error('File does not exist'));
			} else {
				if (data instanceof Buffer) {
					resolve(new Blob([Uint8Array.from(data)]));
				} else {
					resolve(new Blob());
				}
			}
		});
	}

	loadFileAsBuffer(file: string): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			const data = this.getFile(file);
			if (data == null) {
				reject(new Error('File does not exist'));
			} else {
				if (data instanceof Buffer) {
					resolve(data);
				} else {
					resolve(new Buffer(''));
				}
			}
		});
	}

	// Helper methods

	addFile(file: string, data: Buffer | string) {
		this.configuration.files[file] = data;
	}

	getFile(file: string): Buffer | string | undefined {
		return this.configuration.files[file];
	}

	isDirectory(dirPath: string) {
		// Does it have files?
		return this.getFilesInDir(dirPath).length > 0;
	}

	getFilesInDir(dirPath: string): string[] {
		const contents: string[] = [];
		Object.keys(this.configuration.files).forEach((filePath: string) => {
			if (filePath.startsWith(dirPath)) {
				const rem: string = filePath.substr(dirPath.length);
				const parts: string[] = rem.split('\\');
				const file: string = parts[1];
				contents.push(file);
			}
		});
		return contents.filter((value: string, index: number, self: string[]	) => {
			return self.indexOf(value) === index;
		});
	}

	getFileVersion(file: string): Promise<string> {
		return new Promise((resolve, reject) => {
			resolve(undefined);
		});
	}

	showReleaseNotes() {

	}

	getLogFilePath(): string {
		return undefined;
	}

	openLogFile() {

	}
}
