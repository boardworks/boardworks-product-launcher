import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElectronTestingService } from './electron-testing.service';

@NgModule({
	imports: [CommonModule],
	providers: [ElectronTestingService],
})
export class ElectronTestingModule {

}

export * from './electron-testing.service';
