import { async, TestBed, inject } from '@angular/core/testing';
import { ElectronTestingService, ElectronTestingConfiguration } from './electron-testing.service';

describe('Electron testing service', () => {
	let service: ElectronTestingService;
	let emptyService: ElectronTestingService;

	const programFilesEnv = 'PROGRAMFILES(X86)';
	const programFiles = `C:\\Program Files (x86)`;
	const newFilePath = 'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\NewFile.txt';
	const newFileContents = 'New File Contents';

	beforeEach(async(() => {
		const config: ElectronTestingConfiguration = {
			files: {
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\Activities.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/Activities.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\BTMapping.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/BTMapping.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\BTStandards.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/BTStandards.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\OHMapping.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/OHMapping.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\OHStandards.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/OHStandards.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\PAMapping.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/PAMapping.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\PAStandards.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/PAStandards.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\ProductProps.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/ProductProps.xml'),
				'C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\states.xml':
					require('raw-loader!../../../../testing-assets/MS English Ohio/curricula/states.xml'),
			},
			env: {},
			electron: true,
			userDataPath: '',
			applicationPath: '',
			applicationName: '',
			pathSep: '\\'
		};
		// Set environment variable
		config.env[programFiles] = programFilesEnv;
		service = new ElectronTestingService();
		service.configure(config);
		emptyService = new ElectronTestingService();
	}));

	it('should create', () => {
		expect(service).toBeTruthy();
	});

	it('should create without configuration', () => {
		expect(emptyService).toBeTruthy();
	});

	it('should be able to toggle the electron flag', () => {
		expect(service.isElectron()).toBe(true);
		service.configuration.electron = false;
		expect(service.isElectron()).toBe(false);
		service.configuration.electron = true;
	});

	it('should be able to retreive environment variables', () => {
		expect(service.environmentVariable(programFiles)).toEqual(programFilesEnv);
	});

	it('should be able to check if a file exists (when it does)', (done) => {
		service.fileExists('C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\Activities.xml').then((value: boolean) => {
			expect(value).toBeTruthy();
			done();
		});
	});

	it('should be able to check if a file exists (when it doesn\'t)', (done) => {
		service.fileExists('C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\MissingFile.xml').then((value: boolean) => {
			expect(value).toBeFalsy();
			done();
		});
	});

	it('should be able to retreive file contents (when it exists)', (done) => {
		service.readFile('C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\Activities.xml').then((value: string) => {
			expect(value.length).toBeGreaterThan(0);
			done();
		});
	});

	it('should fail to retreive file contents (when it doesn\'t exist)', (done) => {
		service.readFile('C:\\Program Files (x86)\\Boardworks\\MS English Ohio\\curricula\\MissingFile.xml').then((value: string) => {
			// It shouldn't get here
			expect(false).toBeTruthy();
			done();
		}).catch((reason: Error) => {
			expect(reason.message).toEqual('File does not exist');
			done();
		});
	});

	it('should return the contents of a directory', (done) => {
		service.readdir('C:\\Program Files (x86)\\Boardworks').then((value: string[]) => {
			value.forEach((res: string) => console.error(res));
			expect(value.length).toBe(1);
			expect(value[0]).toBe('MS English Ohio');
			done();
		});
	});

	it('should be able to add a file', (done) => {
		service.addFile(newFilePath, newFileContents);
		service.readFile(newFilePath).then(data => {
			expect(data).toEqual(newFileContents);
			done();
		});
	});
});
