import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote, shell } from 'electron';
import * as childProcess from 'child_process';
import { constants } from 'fs';
import * as fs from 'fs';
import { platform } from 'os';
import * as path from 'path';
import { Observable, Observer, Subscriber, forkJoin, merge, of, observable, from } from 'rxjs';
import { filter, map, mergeMap, concatMap } from 'rxjs/operators';
import * as semver from 'semver';
import * as Showdown from 'showdown';
import * as iconv from 'iconv-lite';
// import { Glob, IGlobStatic, IGlob, IOptions } from 'glob';

// import { reject } from 'q';


/*export function asyncFilter<T>(predicate) {
	console.log(`RUN FILTER`);
	const EMPTY = Symbol();
	return Observable.create((subscriber: Subscriber<T>) => {
		console.log(`Sub...`);
		const source: Observable<T> = this;
		source.pipe(mergeMap(async x => {
			if (await predicate(x)) {
				return x;
			}
			return EMPTY;
		}), filter(x => x !== EMPTY));
	});
}*/

// export const asyncFilter = (predicate)

export async function asyncFilter<T>(arr: Array<T>, callback) {
	const fail = Symbol();
	return (await Promise.all(arr.map(async item => (await callback(item)) ? item : fail))).filter(i => i !== fail) as Array<T>;
}

@Injectable()
export class ElectronService {

	ipcRenderer: typeof ipcRenderer;
	webFrame: typeof webFrame;
	remote: typeof remote;
	childProcess: typeof childProcess;
	fs: typeof fs;
	path: typeof path;
	shell: typeof shell;
	semver: typeof semver;
	showdown: typeof Showdown;
	iconv: typeof iconv;
	// glob: typeof Glob;

	get F_OK(): number {
		return this.fs.constants.F_OK;
	}

	get R_OK(): number {
		return this.fs.constants.R_OK;
	}

	get W_OK(): number {
		return this.fs.constants.W_OK;
	}

	get X_OK(): number {
		return this.fs.constants.X_OK;
	}

	get applicationVersion(): string {
		return this.remote.app.getVersion();
	}

	get applicationPath(): string {
		try {
			if (this.remote && this.remote.app) {
				return this.remote.app.getAppPath();
			}
			return '';
		} catch (e) {
			return '';
		}
	}

	get userDataPath(): string {
		try {
			if (this.remote && this.remote.app) {
				return this.remote.app.getPath('userData');
			}
			return '';
		} catch (e) {
			return '';
		}
	}

	get applicationName(): string {
		return this.remote.app.getName();
	}

	get pathSep(): string {
		return this.path.sep;
	}

	get downloadedContentPath(): string {
		if (this.portable) {
			return this.pathJoin(this.environmentVariable('PORTABLE_EXECUTABLE_DIR'), 'boardworks-downloaded-content');
		} else {
			return this.pathJoin(this.applicationPath, 'downloaded-content');
		}
	}

	get portable(): boolean {
		return this.environmentVariable('PORTABLE_EXECUTABLE_DIR') != null;
	}

	constructor() {
		// Conditional imports
		if (this.isElectron()) {
			this.ipcRenderer = window.require('electron').ipcRenderer;
			this.webFrame = window.require('electron').webFrame;
			this.remote = window.require('electron').remote;
			this.shell = window.require('electron').shell;

			this.childProcess = window.require('child_process');
			this.fs = window.require('fs');
			this.path = window.require('path');
			this.showdown = window.require('showdown');
			this.iconv = window.require('iconv-lite');
			// this.glob = window.require('glob');
			/*const log = window.require('electron-log');*/

			this.semver = window.require('semver');
		}
	}

	parseVersion(versionString: string): semver.SemVer {
		return new this.semver.SemVer(versionString);
	}

	version(): semver.SemVer {
		return this.parseVersion(this.applicationVersion);
	}

	isElectron = () => {
		return window && window.process && window.process.type;
	}

	environmentVariable(name: string): string {
		return window.process.env[name];
	}

	markdownToHtml(markdown: string): string {
		const converter: Showdown.Converter = new this.showdown.Converter();
		return converter.makeHtml(markdown);
	}

	filesInFolders(folders: string[], includeSelf: boolean = false): Observable<string> {
		const files: Observable<string>[] = folders.map(value => {
			return this.filesInFolder(value);
		});
		if (includeSelf) {
			folders.forEach(value => {
				files.push(of(value));
			});
		}
		merge(...files).subscribe({
			next: (f) => console.log(`Folder: ${f}`),
			error: (err) => console.error(`Error: ${err}`),
			complete: () => console.warn(`DONE`)
		});
		return of('');
	}

	fileListInFolders(folders: string[], includeSelf: boolean = false, directoriesOnly: boolean = false): Promise<string[]> {
		return new Promise<string[]>((resolve, reject) => {
			const files: Promise<string[]>[] = folders.map(value => {
				console.log(`Get Files in ${value}`);
				return this.filesListInFolder(value, includeSelf, directoriesOnly);
			});
			let allFiles: string[] = [];
			Promise.all(files).then((groups: string[][]) => {
				groups.forEach(group => {
					allFiles = allFiles.concat(group);
				});
				resolve(allFiles);
			});
		});
	}

	filesInFolder(folder): Observable<string> {
		return Observable.create(async (observer: Observer<string>) => {
			if ((await this.stat(folder)).isDirectory()) {
				this.fs.readdir(folder, (err, files) => {
					if (err) {
						console.warn(`Error ${err}`);
						observer.error(err);
						return;
					}
					files.forEach((value: string) => {
						const p: string = this.path.join(folder, value);
						observer.next(p);
					});
					observer.complete();
					/*observer.next(files.map(p => {
						return this.path.join(folder, p);
					}));*/
				});
			} else {
				observer.next(undefined);
				observer.complete();
			}
		});
	}

	stat(filePath: string): Promise<fs.Stats> {
		return new Promise((resolve, reject) => {
			this.fs.stat(filePath, (err, stats: fs.Stats) => {
				if (!err) {
					resolve(stats);
					return;
				}
				reject(err);
			});
		});
	}

	/**
	 * Returns an Observable of the array of files in a specified directory.
	 * Each file entry is the full path of that file.
	 * @param folder The folder to get the contained files for.
	 */
	filesListInFolder(folder, includeFolder: boolean = false, directoriesOnly: boolean = false): Promise<string[]> {
		return new Promise<string[]>(async (resolve, reject) => {
			console.log(`Read dir ${folder}`);
			try {
				if ((await this.stat(folder)).isDirectory()) {
					this.fs.readdir(folder, async (err, files) => {
						if (err) {
							console.warn(`Error ${err}`);
							reject(err);
							return;
						}
						let fullPaths: string[] = files.map((value: string) => {
							const p: string = this.path.join(folder, value);
							return p;
						});
						if (includeFolder) {
							fullPaths.push(folder);
						}
						console.log(`directoriesOnly: ${directoriesOnly}`);
						if (directoriesOnly) {
							console.log(`Filtering non directories from: ${fullPaths}`);
							fullPaths = await asyncFilter<string>(fullPaths, async p => {
								const isDir = (await this.stat(p)).isDirectory();
								console.log(`${p} is directory: ${isDir}`);
								return isDir;
							});
							console.log(`Got: ${fullPaths}`);
						}
						console.log(fullPaths);
						resolve(fullPaths);
					});
				} else {
					resolve([]);
				}
			} catch (err) {
				resolve([]);
			}
		});
	}

	findFiles(pattern: string | RegExp, cwd: string): Observable<string> {
		// return Observable.create((observer: Observer<string>) => {
			/*const options: IOptions = {

			};
			if (cwd != null) {
				options.cwd = cwd;
			}
			const g: IGlob = new this.glob(pattern, options, (err, files) => {
				if (err) {
					observer.error(err);
				} else {
					files.forEach(value => {
						observer.next(value);
					});
				}
			});*/
			return this.filesInFolder(cwd).pipe(
				filter(file => {
					const reg: RegExp = new RegExp(pattern);
					// console.log(`Try ${file} with pattern ${pattern}`);
					if (reg.test(file)) {
						// console.log(`${file} matches pattern: ${pattern}`);
						return true;
					}
					return false;
				})
			);
		// });
	}

	findFilesList(pattern: string | RegExp, cwd: string): Observable<string[]> {
		return from(this.filesListInFolder(cwd)).pipe(
			map(files => {
				const reg: RegExp = new RegExp(pattern);
				files.filter(file => reg.test(file));
				return files;
			})
		);
	}

	fileExists(file: string): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			/* this.fs.access(file, this.fs.constants.F_OK, (accessError) => {
				if (!accessError) {
					console.log(`${file} exists!`);
					resolve(true);
				} else {
					console.log(`${file} NOT exists!`);
					resolve(false);
				}
			}); */
			this.fs.stat(file, (err, stats) => {
				if (err && err.errno === 34) {
					resolve(false);
				} else if (err) {
					// reject(err);
					resolve(false);
				} else {
					resolve(true);
				}
			});
		});
	}

	readFile(file: string, encoding: string = 'utf8'): Promise<Buffer | string> {
		const supportedEncodings: string[] = [
			'ascii', 'base64', 'binary', 'hex', 'ucs2', 'ucs-2', 'utf16le', 'utf-16le', 'utf8', 'utf-8', 'latin1'
		];
		return new Promise<Buffer | string>((resolve, reject) => {
			if (supportedEncodings.indexOf(encoding) === -1) {
				this.fs.readFile(file, (err, data) => {
					if (err) {
						reject(err);
					} else {
						resolve(this.iconv.decode(data, encoding));
					}
				});
			} else {
				this.fs.readFile(file, encoding, (err, data) => {
					if (err) {
						reject(err);
					} else {
						resolve(data);
					}
				});
			}
		});
	}

	readdir(dirPath: string): Promise<string[]> {
		return new Promise<string[]>((resolve, reject) => {
			this.fs.readdir(dirPath, (err, files) => {
				if (err) {
					reject(err);
				} else {
					resolve(files);
				}
			});
		});
	}

	writeFile(file: string, content: string | Buffer, encoding: string = 'utf8'): Promise<boolean> {
		const supportedEncodings: string[] = [
			'ascii', 'base64', 'binary', 'hex', 'ucs2', 'ucs-2', 'utf16le', 'utf-16le', 'utf8', 'utf-8', 'latin1'
		];
		return new Promise<boolean>((resolve, reject) => {
			if (supportedEncodings.indexOf(encoding) === -1) {
				this.fs.writeFile(file, content, (err) => {
					if (err) {
						reject(err);
					} else {
						resolve(true);
					}
				});
			} else {
				this.fs.writeFile(file, content, encoding, (err) => {
					if (err) {
						reject(err);
					} else {
						resolve(true);
					}
				});
			}
		});
	}

	copyFile(src: string, dest: string, flags: number = 0): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			this.fs.copyFile(src, dest, flags, (err) => {
				if (err) {
					reject(err);
				} else {
					resolve(true);
				}
			});
		});
	}

	access(file: string, mode?: number): Promise<NodeJS.ErrnoException> {
		return new Promise((resolve) => {
			this.fs.access(file, mode, (err: NodeJS.ErrnoException) => {
				resolve(err);
			});
		});
	}

	pathJoin(...paths: string[]): string {
		return this.path.join(...paths);
	}

	openExternal(file: string): Promise<void> {
		return this.shell.openExternal(file);
	}

	encodeFileAsBase64(file: string): Promise<string> {
		return new Promise((resolve, reject) => {
			console.error(`Load file: ${file}`);
			this.fs.readFile(file, (err, data) => {
				if (err) {
					reject(err);
				} else {
					resolve(data.toString('base64'));
				}
			});
		});
	}

	loadFileAsBlob(file: string): Promise<Blob> {
		return new Promise((resolve, reject) => {
			this.fs.readFile(file, (err, data) => {
				if (err) {
					reject(err);
				} else {
					resolve(new Blob([Uint8Array.from(data)]));
				}
			});
		});
	}

	loadFileAsBuffer(file: string): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			this.fs.readFile(file, (err, data) => {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		});
	}

	getFileVersion(file: string): Promise<string> {
		const p = file.replace(/\\/g, '\\\\');
		return new Promise<string> ((resolve, reject) => {
			const command = `wmic datafile where name="${p}" get Version`;
			this.childProcess.exec(command, ((err: Error, stdout: string, stderr: string) => {
				if (stdout != null && stdout.length > 0) {
					const parts: string[] = stdout.split('\n');
					const v: string  = parts[1].replace(/\s/g, '');
					resolve(v);
				}
			}));
		});
	}

	getLogFilePath(): string {
		const platformType: string = platform();
		if (platformType === 'win32') {
			const userProfile: string = this.environmentVariable('USERPROFILE');
			const appName: string = this.remote.app.getName();
			const logPath: string = this.pathJoin(userProfile, 'AppData', 'Roaming', appName, 'log.log');
			return logPath;
		} else if (platformType === 'darwin') {
			const homeDir: string = this.environmentVariable('HOME');
			const appName: string = this.remote.app.getName();
			const logPath: string = this.pathJoin(homeDir, 'Library', 'Logs', appName, 'log.log');
			return logPath;
		} else {
			return null;
		}
	}

	openLogFile() {
		const logPath = this.getLogFilePath();
		if (logPath) {
			this.shell.openPath(logPath);
		}
	}
}
