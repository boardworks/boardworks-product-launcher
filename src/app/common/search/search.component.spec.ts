import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { ElectronService } from '../electron/electron.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductTestingService } from '../product/testing/product-testing.module';
import { ProductService } from '../product/product.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SearchComponent', () => {
	let component: SearchComponent;
	let fixture: ComponentFixture<SearchComponent>;

	let mockElectronService: ElectronTestingService;
	const mockProductService = new ProductTestingService().configure({products: []});
	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule
			],
			declarations: [ SearchComponent ],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService },
				{ provide: ProductService, useValue: mockProductService }
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SearchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
