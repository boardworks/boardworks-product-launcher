import { Standards } from '../product/standards';
import { Activity } from '../product/activity';

export enum SearchableTypes {
	Product,
	Standard,
	Activity
}

export interface SearchDocument {
	id: string;
	type: SearchableTypes;
	productId: string;
	fields: string[];
	index: lunr.Index;
}

export interface Searchable {
	buildSearchDocuments(): SearchDocument[];
}

export interface SearchResult {
	result: lunr.Index.Result;
	document: SearchDocument;
}

export interface IdDescription {
	productId: string;
	type: SearchableTypes;
	id: string;
	data?: any;
}

export interface StandardsIdDescription {
	state: string;
	standardsId: number;
}

export interface ActivityIdDescription {
	activityId: number;
}

export function	createDocumentId(productId: string, type: SearchableTypes, dataId: string) {
	return `${productId}__${type.toString(10)}__${dataId}`;
}

export function parseDocumentId(id: string): IdDescription {
	const parts = id.split('__');
	const t: SearchableTypes = SearchableTypes[parts[1]];
	switch (t) {
		case SearchableTypes.Product:
			return {
				productId: parts[0],
				type: SearchableTypes[parts[1]],
				id: parts[2]
			};
		case SearchableTypes.Standard:
			return {
				productId: parts[0],
				type: SearchableTypes[parts[1]],
				id: parts[2],
				data: parseStandardsId(parts[2])
			};
		default:
			break;
	}
}

export function createStandardsId(standards: Standards) {
	return `${standards.state.abbreviation}#${standards.ref}`;
}

export function parseStandardsId(id: string): StandardsIdDescription {
	const parts: string[] = id.split('#');
	return {
		state: parts[0],
		standardsId: parseInt(parts[1], 10)
	};
}

export function createActivityId(activity: Activity) {
	return `~${activity.ref}`;
}

export function parseActivityId(id: string): ActivityIdDescription {
	const parts: string[] = id.split('~');
	return {
		activityId: parseInt(parts[1], 10)
	};
}
