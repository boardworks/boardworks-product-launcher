import { Injectable, NgZone } from '@angular/core';
import * as lunr from 'lunr';
import { SearchDocument, SearchResult, createDocumentId } from './search';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';
import { Subscriber, BehaviorSubject } from 'rxjs';
import { ElectronService } from '../electron/electron.module';
import { Product } from '../product/product';
import { ProductService } from '../product/product.module';

@Injectable({
	providedIn: 'root'
})
export class SearchService {
	private documents: Record<string, SearchDocument> = {};
	private indices: lunr.Index[] = [];
	private _searchThread = false;
	private productService: ProductService;
	public searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>('');
	public searchResults: BehaviorSubject<SearchResult[]> = new BehaviorSubject<SearchResult[]>([]);

	public searchTerm$: Observable<string> = this.searchTerm.asObservable();
	public searchResults$: Observable<SearchResult[]> = this.searchResults.asObservable();
	public activeProduct: Product;

	constructor(private electronService: ElectronService,  private ngZone: NgZone) {
		this.searchResults = new BehaviorSubject<SearchResult[]>([]);
		const handler = (event, results_json: string) => {
			const results: SearchResult[] = JSON.parse(results_json);
			console.log(`Got ${results.length} search results`);
			this.ngZone.run(() => {
				this.searchResults.next(results);
			});
		};
		if (this.electronService && this.electronService.ipcRenderer) {
			this.electronService.ipcRenderer.on('search-results', handler);
		}
	}

	reset(productService?: ProductService) {
		if (productService != null) {
			this.productService = productService;
		}
		if (!this._searchThread) {
			// Send message to search thread (via main) to reset the search index
			if (this.electronService && this.electronService.ipcRenderer) {
				this.electronService.ipcRenderer.send('search-reset-index');
			}
		} else {
			this.indices.splice(0);
			this.indices = [];
			this.documents = {};
		}
	}

	getDocument(id: string) {
		return this.documents[id];
	}

	add(documents: SearchDocument[]) {
		if (!this._searchThread) {
			// Send message to search thread (via main) to add documents to search index
			const documents_json: string = JSON.stringify(documents);
			if (this.electronService && this.electronService.ipcRenderer) {
				this.electronService.ipcRenderer.send('search-add', documents_json);
			}
		} else {
			documents.forEach((doc: SearchDocument) => {
				const id: string = doc.id;
				const index: lunr.Index = lunr(function() {
					this.pipeline.remove(lunr.stemmer);
					this.searchPipeline.remove(lunr.stemmer);
					this.ref('id');
					doc.fields.forEach((f) => {
						this.field(f);
					});
					this.metadataWhitelist = ['position'];
					this.add(doc);
				});
				this.indices.push(index);
				doc.index = index;
				this.documents[id] = doc;
			});
		}
	}

	inputSearch(term: Observable<string>): Observable<boolean> {
		return term/*.debounceTime(400)*/.pipe(
			distinctUntilChanged(),
			switchMap(t => {
				console.log(`t: ${t}`);
				if (t != null && t.length > 0) {
					return this.search(t, '', this.activeProduct);
				} else {
					return of(false);
				}
			})
		);
	}

	search(term: string, id?: string, product?: Product): Observable<boolean> {
		if (!this._searchThread) {
			// Send message to search thread (via main) to do a search using the provided term
			if (this.electronService && this.electronService.ipcRenderer) {
				if (product) {
					return product.index(this).pipe(
						map(value => {
							console.log(`Search for ${term} in ${product.title}`);
							this.electronService.ipcRenderer.send('search-query', term, product);
							return true;
						})
					);
				} else {
					return this.productService.indexAllProducts(this).pipe(
						map(value => {
							this.electronService.ipcRenderer.send('search-query', term);
							return true;
						})
					);
				}
			}
		} else {
			this.searchTerm.next(term);
			return Observable.create((subscriber: Subscriber<SearchResult[]>) => {
				const results: lunr.Index.Result[] = [];
				if (id != null && id.length > 0) {
					Object.values(this.documents).forEach(document => {
						if (document.productId === id) {
							results.push(...document.index.search(term));
						}
					});
				} else {
					this.indices.forEach((index: lunr.Index) => {
						results.push(...index.search(term));
					});
				}
				const searchResults: SearchResult[] = results.map<SearchResult>(res => {
					console.log(`Looking for document ${res.ref} = ${this.getDocument(res.ref)}`);
					return {
						result: res,
						document: this.getDocument(res.ref)
					};
				});
				subscriber.next(searchResults);
			});
		}
	}

	setupSearchThread() {
		this._searchThread = true;
		if (this.electronService && this.electronService.ipcRenderer) {
			this.electronService.ipcRenderer.on('search-thread-add', this.thread_add.bind(this));
			this.electronService.ipcRenderer.on('search-thread-reset-index', this.thread_reset.bind(this));
			this.electronService.ipcRenderer.on('search-thread-query', this.thread_search.bind(this));
		}
	}

	thread_add(event, documents_json: string) {
		const documents: SearchDocument[] = JSON.parse(documents_json);
		this.add(documents);
	}

	thread_reset(event) {
		this.reset();
	}

	thread_search(event, term: string) {
		this.search(term).subscribe(result => {
			if (this.electronService && this.electronService.ipcRenderer) {
				const results_json: string = JSON.stringify(result);
				this.electronService.ipcRenderer.send('search-thread-results', results_json);
			}
		});
	}
}
