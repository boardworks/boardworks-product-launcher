import { TestBed, inject, async } from '@angular/core/testing';
import { ElectronTestingService, DefaultElectronTestingConfiguration} from '../electron/testing/electron-testing.module';
import { SearchService } from './search.service';
import { ElectronService } from '../electron/electron.module';

describe('SearchService', () => {
	let mockElectronService: ElectronTestingService;

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		TestBed.configureTestingModule({
			providers: [SearchService,
				{ provide: ElectronService, useValue: mockElectronService }
			]
		});
	}));

	it('should be created', inject([SearchService], (service: SearchService) => {
		expect(service).toBeTruthy();
	}));
});
