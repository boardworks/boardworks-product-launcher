import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { SearchService } from './search.service';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchResult, IdDescription, parseDocumentId, SearchableTypes, StandardsIdDescription } from './search';
import { ProductService, Product, State } from '../product/product.module';
import { Router } from '@angular/router';
import { Standards, StandardsSearchDocument } from '../product/standards';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { UpdateService, GitLabBuild } from '../update/update.service';

@Component({
	selector: 'bwfe-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit {

	results: Subject<SearchResult[]> = new Subject<SearchResult[]>();
	results$: Observable<SearchResult[]> = this.results.asObservable();
	searchTerm$: Subject<string> = new Subject<string>();

	private _visible = false;
	private _product: Product;

	@Input()
	get product(): Product {
		return this._product;
	}
	set product(value: Product) {
		this._product = value;
		this.searchService.activeProduct = this._product;
	}

	get isVisible(): boolean {
		return this._visible;
	}

	public get placeholder(): string {
		if (this.product != null && this.product.title != null) {
			return `Search ${this.product.title}...`;
		}
		return 'Search...';
	}

	constructor(private searchService: SearchService, private http: HttpClient,
		private productService: ProductService, private router: Router) {
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
		this.searchTerm$.subscribe(value => console.log(`v: ${value}`));
		this.searchService.searchResults.subscribe(value => {
			console.log(`There are ${value.length} results returned`);
			this.results.next(value);
		});
		// Connect the search service to the search term
		this.searchService.inputSearch(this.searchTerm$).subscribe(value => {
			console.log(`Input ${value}`);
			if (!value) {
				this.results.next([]);
			}
		});
	}

	show() {
		this._visible = true;
		const headers: HttpHeaders = new HttpHeaders().set('PRIVATE-TOKEN', 'ecSgBUpPHGMJUyeneQ8K');
		/*return this.http.get<GitLabBuild[]>(
			`${UpdateService.apiBaseUrl}/jobs?scope=success`, {headers} );*/
		this.http.get(
				`${UpdateService.apiBaseUrl}/jobs?scope=success`, {headers} ).subscribe(value => {
					console.log(`DATA:`);
					console.log(value);
				});
		this.http.get<GitLabBuild[]>(
					`${UpdateService.apiBaseUrl}/jobs?scope=success`, {headers} ).subscribe(value => {
						console.log(`GL DATA:`);
						console.log(value);
					});
	}

	hide() {
		this._visible = false;
	}

	getProduct$(result: SearchResult): Observable<Product> {
		try {
			return this.productService.getProduct(result.document.productId);
		} catch (err) {
			console.error(`${err}`);
		}
	}

	getStandards$(result: SearchResult): Observable<Standards> {
		return this.getProduct$(result).pipe(
			map<Product, Standards>(value => {
				if (result.document.type === SearchableTypes.Standard) {
					const standardsDoc: StandardsSearchDocument = result.document as StandardsSearchDocument;
					return value.getState(standardsDoc.state).standards.find(standardsDoc.standardsRef);
				}
			})
		);
	}

	select(result: SearchResult) {
		switch (result.document.type) {
			case SearchableTypes.Product:
				this.router.navigate(['./product', result.document.productId]);
				break;
			case SearchableTypes.Standard:
				this.router.navigate(['./state', result.document.productId, (result.document as StandardsSearchDocument).state,
					(result.document as StandardsSearchDocument).standardsRef]);
				break;
			default:
				break;
		}
	}
}
