import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

export interface LocationData {
	status: string;
	country: string;
	countryCode: string;
	query: string;
	message?: string;
}

@Injectable({
	providedIn: 'root'
})
export class LocationService {

	locationSubject: BehaviorSubject<LocationData> = new BehaviorSubject<LocationData>(
		{status: 'unknown', country: '', countryCode: '', query: ''}
	);

	constructor(private http: HttpClient) {
	}

	get location$(): Observable<LocationData> {
		if (this.locationSubject.getValue().status !== 'success') {
			this.http.get<LocationData>('http://ip-api.com/json').subscribe(value => {
				this.locationSubject.next(value);
			});
		}
		return this.locationSubject.asObservable();
	}

	get countryCode(): Observable<string> {
		return this.location$.pipe(
			map(data => {
				if (data.status === 'success') {
					return data.countryCode;
				} else {
					return undefined;
				}
			})
		);
	}

	get country(): Observable<string> {
		return this.location$.pipe(
			map(data => {
				if (data.status === 'success') {
					return data.country;
				} else {
					return undefined;
				}
			})
		);
	}
}
