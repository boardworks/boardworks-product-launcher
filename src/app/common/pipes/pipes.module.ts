import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PathTruncatePipe } from './path-truncate.pipe';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [PathTruncatePipe],
	exports: [PathTruncatePipe]
})
export class PipesModule { }
