import { PathTruncatePipe } from './path-truncate.pipe';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { TestBed, inject } from '@angular/core/testing';
import { ElectronService } from '../electron/electron.module';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

describe('PathTruncatePipe', () => {
	let pipe: PathTruncatePipe;
	const shortPath = 'C:\\short\\path';
	const longPath = 'C:\\this\\is\\a\\long\\test\\filesystem\\path';
	const expectedLongPathResult = 'C:\\this\\is\\a\\long\\test\\...\\path';
	const longPathWithSpaces = 'C:\\spaced and\\quite long\\test\\file system\\path';
	const expectedLongPathWithSpacesResult = 'C:\\spaced and\\quite long\\...\\path';
	const shortNetworkPath = '\\\\Server\\test\\network\\path';
	const longNetworkPath = '\\\\Server\\long\\test\\network\\path';
	const expectedLongNetworkPathResult = '\\\\Server\\long\\test\\net...\\path';
	const longSinglePath = 'C:\\this is a very long path and should be cut down\\path';
	const expectedLongSinglePathResult = 'C:\\this is a very long...\\path';

	const mockElectronService: ElectronTestingService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

	/**
	 * Setup the test environment
	 */
	beforeEach(() => {
		// Must reset the test environment before initializing it.
		TestBed.resetTestEnvironment();

		TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
		.configureTestingModule({
			declarations: [ ],
			schemas: [ ],
			imports: [],
			providers: [
				{ provide: ElectronService, useValue: mockElectronService},
			]
		});
	});

	/**
	 * Create the pipe
	 */
	beforeEach(inject([ElectronService], (electronService) => {
		pipe = new PathTruncatePipe(electronService);
	}));

	afterEach(() => {
		pipe = null;
	});

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});

	it('should not transform a path below the length limit', () => {
		expect(pipe.transform(shortPath, 30)).toEqual(shortPath);
	});

	it('should truncate a long path', () => {
		expect(pipe.transform(longPath, 30)).toEqual(expectedLongPathResult);
	});

	it('should not transform a short network path', () => {
		expect(pipe.transform(shortNetworkPath, 30)).toEqual(shortNetworkPath);
	});

	it('should truncate a long network path', () => {
		expect(pipe.transform(longNetworkPath, 30)).toEqual(expectedLongNetworkPathResult);
	});

	it('should handle paths with spaces in', () => {
		expect(pipe.transform(longPathWithSpaces, 30)).toEqual(expectedLongPathWithSpacesResult);
	});

	it('should handle long paths with spaces which have very long folder names', () => {
		expect(pipe.transform(longSinglePath, 30)).toEqual(expectedLongSinglePathResult);
	});
});
