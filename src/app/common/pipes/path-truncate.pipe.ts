import { Pipe, PipeTransform } from '@angular/core';
import { ElectronService } from '../electron/electron.module';
const { remote } = window.require('electron');

@Pipe({
	name: 'pathTruncate'
})
export class PathTruncatePipe implements PipeTransform {

	constructor(private electronService: ElectronService) {

	}

	transform(value: any, args?: any): any {
		console.log(`Transform: ${value}`);
		const length: number = args as number;
		const sep: string = this.electronService.pathSep;
		if (value.length > length) {
			// Split the path
			const pathParts: string[] = (value as string).split(sep);
			const elipse = `...`;
			const elipseArray = [elipse];
			const shortFolderLength = 8;

			let start: string[] = [];
			let end: string[] = [];

			// Construct the minimum path possible
			const minimum: string[] = [];
			// Always include the end part of the path
			const minimumEnd: string[] = [pathParts.pop()];
			// Add from the front so we have at least one part
			let hasFront = false;
			while (!hasFront) {
				const part: string = pathParts.shift();
				// If the part isn't empty then stop after adding it
				// UNC network paths will have empty parts as they start with multiple slashes
				if (part.length > 0) {
					hasFront = true;
				}
				minimum.push(part);
			}

			const pathTooLong = (parts: string[]): number => {
				return parts.join(sep).length - length;
			};

			const shortenFolderName = (name: string, len: number = shortFolderLength): string => {
				if (name.length > len) {
					const short: string = name.substr(0, len - elipse.length);
					return short + elipse;
				} else {
					return name;
				}
			};

			// Begin with the minimum start
			start = minimum.slice(0);
			end = minimumEnd.slice(0);
			let success = true;
			while (pathParts.length > 0 && success) {
				console.log(`Check: ${pathParts[0]}`);
				// Try adding a folder from the start of the path
				if (pathTooLong(start.concat([pathParts[0]].concat(end))) <= 0) {
					console.log(`Adding: ${pathParts[0]}`);
					// Add this as not too long
					start.push(pathParts.shift());
				} else {
					console.log(`Too long - trying ${pathParts[pathParts.length - 1]}`);
					// If this is too long, try the next end part instead.
					if (!pathTooLong(start.concat([pathParts[pathParts.length - 1]]).concat(end))) {
						// Add this to the end
						console.log(`Adding: ${pathParts[pathParts.length - 1]}`);
						end.unshift(pathParts.pop());
					} else {
						console.log(`Too Long`);
						success = false;
					}
				}
				// If we haven't been able to add a part to the start or end, try shorten the start folder name and adding that
				if (!success) {
					const tooLongBy: number = pathTooLong(start.concat([pathParts[0]].concat(end)));
					console.log(`Too long by: ${tooLongBy}`);
					const len = pathParts[0].length - tooLongBy;
					const short = shortenFolderName(pathParts[0], len);
					console.log(`Part shortened from '${pathParts[0]}' to '${short}'`);
					start.push(short);
					// We have shortened the path but truncating a folder name - finish here and return the result
				}
			}
			return start.concat(end).join(sep);
		}
		return value;
	}

}
