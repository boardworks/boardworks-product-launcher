import { ProductTemplates, ProductPatchConfiguration, IProduct } from '../product/product-templates';
import { ProductPatchConfigurationNames, Product } from '../product/product';

export interface PatchEntry {
	match: string;
	replace: string;
}

export class Patcher {
	constructor(private templates: ProductTemplates) {
	}

	private getProductConfig(product: Product): ProductPatchConfiguration {
		if (this.templates != null) {
			let title: string = product.title;
			const productTemplate: IProduct = product.findTemplate(this.templates);
			if (productTemplate != null) {
				title = productTemplate.title;
			}
			return this.templates.patch[title];
		}
		return undefined;
	}

	private applyPatch(entries: PatchEntry[], input: string): string {
		let output: string = input;
		entries.forEach((entry: PatchEntry) => {
			const r: RegExp = new RegExp(entry.match, 'g');
			output = output.replace(r, entry.replace);
		});
		return output;
	}

	patch(product: Product, configuration: ProductPatchConfigurationNames, input: string, state?: string): string {
		const config: ProductPatchConfiguration = this.getProductConfig(product);
		if (config != null && config[configuration] != null) {
			const d: PatchEntry[] | Record<string, PatchEntry[]> = config[configuration];
			let entries: PatchEntry[];
			if (Array.isArray(d)) {
				entries = d;
			} else if (state != null && d[state] != null) {
				entries = d[state];
			}
			if (entries != null) {
				return this.applyPatch(entries, input);
			}
		}
		return input;
	}
}
