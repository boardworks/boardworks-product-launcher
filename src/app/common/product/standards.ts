import { State } from './state';
import { Activity } from './activity';
import { SearchDocument, Searchable, SearchableTypes, createStandardsId, createDocumentId } from '../search/search';

export interface StandardsSearchDocument extends SearchDocument {
	standardsRef: number;
	state: string;
	label: string;
}

export class Standards implements Searchable {
	public children: Standards[];
	private _state: State;

	public get state(): State {
		if (this._state == null) {
			return this.parent.state;
		}
		return this._state;
	}

	public set state(value: State) {
		if (this.parent == null) {
			this._state = value;
		}
	}

	public get level(): number {
		let l = 0;
		let p: Standards = this.parent;
		while (p != null) {
			p = p.parent;
			l++;
		}
		return l;
	}

	public get activities(): Activity[] {
		return this.state.getStandardsActivities(this.ref);
	}

	public get sortedChildActivities(): Activity[] {
		return this.childActivities.sort((a: Activity, b: Activity) => {
			const articles: RegExp = /^(the|a|an)\s+/g;
			// Sort activities alphabetically, discount 'The' or 'the' at the start.
			const aNoThe = a.displayTitle.toLowerCase().replace(articles, '');
			const bNoThe = b.displayTitle.toLowerCase().replace(articles, '');
			if (aNoThe > bNoThe) {
				return 1;
			}
			if (aNoThe < bNoThe) {
				return -1;
			}
			return 0;
		});
	}

	public get childActivities(): Activity[] {
		const a: Activity[] = this.activities;
		this.children.forEach(standards => {
			a.push(...standards.childActivities);
		});
		return a.filter((value, index, arr) => {
			return value != null && arr.indexOf(value) === index;
		});
	}

	constructor(public ref: number, public label: string, public parent: Standards) {
		this.children = [];
	}

	static loadStandards(element: Element, parent?: Standards): Standards {
		const ref: number = parseInt(element.getAttribute('ref'), 10);
		const label: string = element.getAttribute('label');
		const standards: Standards = new Standards(ref, label, parent);
		const match: RegExpMatchArray = element.tagName.match(/h([0-9]+)/);
		if (match.length > 0) {
			const currentIndex: number = parseInt(match[1], 10);
			let childElements: HTMLCollectionOf<Element> = element.getElementsByTagName(`h${currentIndex + 1}`) as HTMLCollectionOf<Element>;
			// Workaround for erroneous standards which go from h0 > h2
			if (childElements.length === 0) {
				childElements = element.getElementsByTagName(`h${currentIndex + 2}`) as HTMLCollectionOf<Element>;
			}
			Array.from(childElements).forEach((child: Element) => {
				standards.children.push(Standards.loadStandards(child, standards));
			});
		}
		return standards;
	}

	find(ref: number): Standards {
		if (this.ref === ref) { return this; }
		let node: Standards;
		this.children.every(value => {
			if (value.ref > ref) {
				return false;
			}
			node = value;
			return true;
		});
		if (node != null) {
			return node.find(ref);
		}
		return undefined;
	}

	buildSearchDocuments(): StandardsSearchDocument[] {
		const docs: StandardsSearchDocument[] = [];
		const id: string = createDocumentId(this.state.product.id, SearchableTypes.Standard, createStandardsId(this));
		docs.push({
			id,
			type: SearchableTypes.Standard,
			productId: this.state.product.id,
			standardsRef: this.ref,
			state: this.state.abbreviation,
			label: this.label,
			fields: ['label'],
			index: undefined
		});
		this.children.forEach((child: Standards) => {
			docs.push(...child.buildSearchDocuments());
		});
		return docs;
	}
}
