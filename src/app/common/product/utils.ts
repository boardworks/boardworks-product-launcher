import { IProduct } from './product-templates';

export const applyTemplateToProduct = (product: IProduct, template: IProduct) => {
	if (template.title) {
		product.title = template.title;
	}
	product.icon = template.icon;
	product.color = template.color;
	if (template.type) {
		product.type = template.type;
	}
	if (template.standardsButtonText) {
		product.standardsButtonText = template.standardsButtonText;
	}
	if (template.contentsGuidePath) {
		product.contentsGuidePath = template.contentsGuidePath.slice(0);
	}
	if (template.userGuidePath) {
		product.userGuidePath = template.userGuidePath.slice(0);
	}
	if (template.statesPath) {
		product.statesPath = template.statesPath.slice(0);
	}
	if (template.stateMappingPaths) {
		product.stateMappingPaths = Object.assign({}, template.stateMappingPaths);
	}
	if (template.stateStandardsPaths) {
		product.stateStandardsPaths = Object.assign({}, template.stateStandardsPaths);
	}
	if (template.activitiesPath) {
		product.activitiesPath = template.activitiesPath.slice(0);
	}
	if (template.placeholderScreenshotPath) {
		product.placeholderScreenshotPath = template.placeholderScreenshotPath.slice(0);
	}
};
