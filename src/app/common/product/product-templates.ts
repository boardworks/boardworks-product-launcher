import { PatchEntry } from '../patcher/patcher';
import { State, IState } from './state';
import { Activity } from './activity';
import { satisfies } from 'semver';
import { ElectronService } from '../electron/electron.module';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SearchService } from '../search/search.service';

export type VersionedProduct = Record<string, IProduct>;

export const getVersionedProduct = (products: VersionedProduct, version: string): IProduct | null => {
	if (products == null || version == null) {
		return null;
	}
	const versionParts: string[] = version.split('.');
	while (versionParts.length > 3) {
		versionParts.pop();
	}
	const sanitisedVersion: string = versionParts.join('.');
	const ranges = Object.keys(products);
	let match: IProduct = null;
	ranges.forEach(range => {
		if (satisfies(sanitisedVersion, range)) {
			match = products[range];
		}
	});
	if (match == null && products['*'] != null) {
		return products['*'];
	}
	return match;
};

export class ProductTemplates {
	unsupported: string[];
	preload: Record<string, VersionedProduct>;
	patch: Record<string, ProductPatchConfiguration>;
	products: IProduct[];
}

export enum ProductType {
	STANDARD = 'standard',
	STANDARDLESS = 'standardless'
}

export interface IProduct {
	id: string;
	type: ProductType;
	application: string;
	version: string;
	path: string;
	path64: string;
	activities: Activity[];
	title: string;
	// aliases: string[];
	states: State[];
	icon: string;
	color: string;
	screenshotsPath: string[];
	standardsButtonText: string;
	userGuidePath: string[];
	contentsGuidePath: string[];
	productPropsPath: string[];
	statesPath: string[];
	activitiesPath: string[];
	stateMappingPaths: Record<string, string[]>;
	stateStandardsPaths: Record<string, string[]>;
	defaultStateMappingPath: string[];
	defaultStateStandardsPath: string[];
	placeholderScreenshotPath: string[];
	hasScreenshots: boolean;

	productTitle: string;

	loadStream(electronService: ElectronService, http: HttpClient, stateTemplates?: IState[],
		productTemplates?: ProductTemplates): Observable<IProduct>;

	loadProductPropertiesStream(electronService: ElectronService, templates: ProductTemplates): Observable<IProduct>;

	load(electronService: ElectronService, stateTemplates?: IState[], productTemplates?: ProductTemplates): Observable<IProduct>;
	findTemplate(templates: ProductTemplates): IProduct;

	index(searchService: SearchService): Observable<boolean>;
}

export interface ProductPatchConfiguration {
	productProps?: PatchEntry[];
	states?: PatchEntry[];
	activities?: PatchEntry[];
	mappings?: Record<string, PatchEntry[]>;
	standards?: Record<string, PatchEntry[]>;
}
