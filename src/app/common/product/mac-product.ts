import { IProduct, ProductType, ProductTemplates } from './product-templates';
import { Activity } from './activity';
import { State } from './state';
import { ElectronService } from '../electron/electron.module';
import { Observable, Observer } from 'rxjs';
import { Patcher } from '../patcher/patcher';
import { ProductPatchConfigurationNames, Product } from './product';
import { Sanitiser } from '../sanitiser/sanitiser';
const { remote } = window.require('electron');
const log = remote.require('electron-log');

export class MacProduct extends Product implements IProduct {
	public id: string;
	public type: ProductType;
	public application: string;
	public version: string;
	public path64: string;
	public activities: Activity[];
	public title: string;
	public states: State[];
	public icon: string;
	public color: string;
	public standardsButtonText: string;
	public userGuidePath: string[] = ['${productPath}', 'notes', 'User Guide.pdf'];
	public contentsGuidePath: string[] = ['${productPath}', 'notes', 'Contents Guide.pdf'];
	public productPropsPath: string[] = ['${productPath}', 'Contents', 'Resources', 'resources', 'xml', 'frontends', 'config.xml'];
	public screenshotsPath: string[] = ['${productPath}', 'screenshots'];
	public statesPath: string[] = ['${productPath}', 'curricula', 'states.xml'];
	public activitiesPath: string[] = ['${productPath}', 'curricula', 'Activities.xml'];
	public stateMappingPaths: Record<string, string[]>;
	public stateStandardsPaths: Record<string, string[]>;
	public defaultStateMappingPath: string[];
	public defaultStateStandardsPath: string[];
	public placeholderScreenshotPath: string[];
  	public hasScreenshots: boolean;

	constructor(public path: string) {
		super(path);
		this.type = ProductType.STANDARD;
  	}

	loadProductPropertiesStream(electronService: ElectronService, templates: ProductTemplates): Observable<Product> {
		return Observable.create((observer: Observer<Product>) => {
			const productPropsXML: string = this.getPath(this.productPropsPath, electronService);
			// const productPropsXML: string = electronService.pathJoin(product.path, 'curricula', 'ProductProps.xml');
			electronService.fileExists(productPropsXML).then((exists: boolean) => {
				if (exists) {
					log.info(`Loading product properties: ${productPropsXML}`);
					return electronService.readFile(productPropsXML).then((data: Buffer) => {
						// Patch the xml
						const patcher: Patcher = new Patcher(templates);
						const patchedXml: string = patcher.patch(this, ProductPatchConfigurationNames.ProductProps, data.toString());
						// Sanitise xml
						const xml: string = new Sanitiser().sanitiseXml(patchedXml);
						const parser: DOMParser = new DOMParser();
						const doc: Document = parser.parseFromString(`<data>${xml}</data>`, 'application/xml');
						const titleElements: HTMLCollectionOf<HTMLTitleElement> = doc.getElementsByTagName('name') as HTMLCollectionOf<HTMLTitleElement>;
						if (titleElements.length > 0) {
							this.title = titleElements[0].textContent;
						}
						const versionElements: HTMLCollectionOf<HTMLTitleElement> = doc.getElementsByTagName('version') as HTMLCollectionOf<HTMLTitleElement>;
						if (versionElements.length > 0) {
							this.version = versionElements[0].textContent;
						}
						log.info(`Product Properties loaded for: ${productPropsXML}`);
						log.info(`Product is: ${this.title} version: ${this.version}`);
						return this;
					});
				} else {
					log.info(`Product Properties file at ${productPropsXML} does not exist`);
					return this;
				}
			}).then((loadedProduct: Product) => {
				const t: number = Date.now();
				// Get the product application file name and version number
				return new Promise<Product>((resolve, reject) => {
					electronService.findFilesList(/\.exe$/, this.path).subscribe({
						next: (values) => {
							// Find the exe which matches a template (some products have more than one exe by mistake - find the correct one to use)
							const foundApplication: string = values.find(value => {
								const parts: string[] = value.split(electronService.pathSep);
								const exeName = parts.pop();
								console.log(`EXE NAME: ${exeName}`);
								const appName: string = exeName.replace(/\.exe$/, '');
								return this.findTemplateForApplication(templates, appName) != null;
							});
							if (foundApplication != null) {
								const parts: string[] = foundApplication.split(electronService.pathSep);
								const exeName = parts.pop();
								console.log(`FOUND EXE NAME: ${exeName}`);
								const appName: string = exeName.replace(/\.exe$/, '');
								// console.log(`Application for ${loadedProduct.title} is ${value}`);
								this.application = appName;
								electronService.getFileVersion(electronService.pathJoin(loadedProduct.path, exeName)).then((v: string) => {
									this.version = v;
									// console.log(`*** IN ${Date.now() - t}`);
									resolve(this);
								}).catch((reason) => {
									console.log(`Failed to get version: ${reason}`);
									this.version = '';
									resolve(this);
								});
							} else {
								resolve(this);
							}
						},
						error: (err) => console.error(`EXE ERROR: ${err}`),
						complete: () => {
							console.log(`EXE COMPLETE`);
							// resolve(this);
						}
					});
				});
			}).then((loadedProduct: Product) => {
				// console.log(`**Loaded product is: ${this}`);
				if (loadedProduct.application != null) {
					observer.next(loadedProduct);
					observer.complete();
				} else {
					log.warn(`No application found for product ${loadedProduct}`);
					observer.next(loadedProduct);
					observer.complete();
				}
				/* else {
					observer.next(loadedProduct);
					observer.complete();
				}*/
			}).catch(reason => {
				log.warn(`Error loading product properties: ${reason}`);
				observer.next(this);
				observer.complete();
			});
		});
	}
}
