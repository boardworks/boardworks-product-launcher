import { Product } from './product';
import { ElectronService } from '../electron/electron.module';
import { Observable, from } from 'rxjs';
import { Searchable, SearchDocument, SearchableTypes, createDocumentId, createActivityId } from '../search/search';
const { remote } = window.require('electron');
const log = remote.require('electron-log');

export class ActivityReference {
	constructor(public ref: number) {

	}
}

export interface ActivitySearchDocument extends SearchDocument {
	title: string;
	description: string;
}

export class Activity extends ActivityReference implements Searchable {

	/**
	 * The Display Title for the activity.
	 * Some product activity descriptions do not have 'displayTitle' properties,
	 * so those will use the 'title' property.
	 */
	public get displayTitle(): string {
		if (this._displayTitle != null) {
			return this._displayTitle;
		} else {
			return this.title;
		}
	}

	public set displayTitle(value: string) {
		this._displayTitle = value;
	}

	constructor(public product: Product, public ref: number,
		public title: string,
		private _displayTitle: string,
		public length: number,
		public feature: number,
		public description: string) {
			super(ref);
	}

	getScreenshot(electronService: ElectronService): string {
		if (this.product.placeholderScreenshotPath != null && this.product.placeholderScreenshotPath.length > 0) {
			return this.product.getPath(this.product.placeholderScreenshotPath, electronService);
		} else if (this.product.hasScreenshots) {
			return electronService.pathJoin(this.product.path, 'screenshots', this.title, 'Slide1.png');
		} else {
			return undefined;
		}
	}

	getPreviewScreenshots(electronService: ElectronService): string[] {
		const screenshots: string[] = [];
		for (let i = 1; i <= this.length; i++) {
			screenshots.push(electronService.pathJoin(this.product.path, 'screenshots', this.title, `Slide${i}.png`));
		}
		return screenshots;
	}

	hasWorksheet(electronService: ElectronService): Observable<boolean> {
		const worksheetPath: string = electronService.pathJoin(this.product.path, 'worksheets', `${this.title}.doc`);
		return from(electronService.fileExists(worksheetPath));
	}

	launch(electronService: ElectronService): Promise<boolean> {
		// TODO - Add check for pptx here
		const activityPath: string = electronService.pathJoin(this.product.path, 'powerpoint', `${this.title}.ppt`);
		const activityPathPptx: string = electronService.pathJoin(this.product.path, 'powerpoint', `${this.title}.pptx`);
		log.info(`Launching ${activityPath}`);
		return electronService.fileExists(activityPath).then(exists => {
			log.info(`Exists: ${exists}`);
			if (exists) {
				return electronService.openExternal(activityPath);
			} else {
				return electronService.openExternal(activityPathPptx);
			}
		}).then(v => true);
	}

	launchWorksheet(electronService: ElectronService): Promise<void> {
		const worksheetPath: string = electronService.pathJoin(this.product.path, 'worksheets', `${this.title}.doc`);
		return electronService.openExternal(worksheetPath);
	}

	buildSearchDocuments(): ActivitySearchDocument[] {
		const id: string = createDocumentId(this.product.id, SearchableTypes.Activity, createActivityId(this));
		return [{
			id,
			type: SearchableTypes.Activity,
			productId: this.product.id,
			title: this.title,
			description: this.description,
			fields: ['title', 'description'],
			index: undefined
		}];
	}
}
