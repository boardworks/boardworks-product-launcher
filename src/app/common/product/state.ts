import { Mapping } from './mapping';
import { Standards } from './standards';
import { ActivityReference, Activity } from './activity';
import { Product, ProductLocale } from './product';
import { Searchable, SearchDocument } from '../search/search';

export interface IState {
	name: string;
	abbreviation: string;
	mappable: boolean;
	locale?: ProductLocale;
}

export class State implements IState, Searchable {
	public name: string;
	public mapping: Mapping[];

	private _standards: Standards;

	public get standards(): Standards {
		return this._standards;
	}

	public set standards(value: Standards) {
		this._standards = value;
		this._standards.state = this;
	}

	constructor(public product: Product, public description: string, public abbreviation: string,
			public mappable: boolean = false, public locale?: ProductLocale ) {
		this.name = this.description;
		this.mapping = [];
	}

	public mergeTemplate(templates: IState[]): State {
		if (templates != null) {
			const template: IState = templates.find((s: IState) => s.abbreviation === this.abbreviation);
			if (template != null) {
				this.name = template.name;
				if (template.locale) {
					this.locale = template.locale;
				}
				if (template.mappable) {
					this.mappable = template.mappable;
				}
			}
		}
		return this;
	}

	public getActivities(refs: ActivityReference[]): Activity[] {
		return refs.map<Activity>((activity: ActivityReference) => {
			return this.product.getActivity(activity.ref);
		});
	}

	/**
	 * Returns the mapping for a specified standard
	 * @param ref The reference for the standard to get the mapping for.
	 */
	public getMapping(ref: number): Mapping {
		return this.mapping.find((value: Mapping) => value.ref === ref);
	}

	/**
	 * Returns the activity data for the specified standard
	 * @param ref The reference for the standard to get the activities for.
	 */
	public getStandardsActivities(ref: number): Activity[] {
		// Get the mappings for this standard
		const mapping: Mapping = this.getMapping(ref);
		if (mapping != null) {
			// Get the activities in the mapping
			return this.getActivities(mapping.activities);
		}
		return [];
	}

	public buildSearchDocuments(): SearchDocument[] {
		if (this.standards) {
			return this.standards.buildSearchDocuments();
		}
		return [];
	}
}
