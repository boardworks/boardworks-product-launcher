import { State, IState } from './state';
import { Activity, ActivityReference } from './activity';
import { ElectronService } from '../electron/electron.module';
import { Observable, Observer, Subscription, empty, of, from, forkJoin, BehaviorSubject } from 'rxjs';
import { map, merge, tap, filter, switchMap, distinctUntilChanged, take, catchError, mergeMap, mapTo } from 'rxjs/operators';
import { Mapping } from './mapping';
import { Standards } from './standards';
import { ProductTemplates, VersionedProduct, getVersionedProduct, IProduct, ProductType } from './product-templates';
import { Sanitiser } from '../sanitiser/sanitiser';
import { SearchDocument, Searchable, SearchableTypes } from '../search/search';
import { Tokenizer } from '../tokenizer/tokenizer';
import { Subscriber } from 'rxjs';
import { SearchService } from '../search/search.service';
import { PatchEntry, Patcher } from '../patcher/patcher';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { platform } from 'os';
import { MacProduct } from './mac-product';
import { mergeProductPreloadTemplate, mergeProductTemplate } from './observables';
const { remote } = window.require('electron');
const log = remote.require('electron-log');

export enum ProductLocale {
	US = 'US',
	UK = 'UK'
}

export const unknownIcon = 'assets/icons/Unknown_icon.svg';

export enum ProductPatchConfigurationNames {
	ProductProps = 'productProps',
	States = 'states',
	Activities = 'activities',
	Mappings = 'mappings',
	Standards = 'standards'
}

export interface ProductSearchDocument extends SearchDocument {
	title: string;
}

export enum TitleExpansionMethod {
	STATE = 0,
	APPLICATION = 1,
}

export class Product implements IProduct, Searchable {
	public application: string;
	public type: ProductType;
	public version: string;
	public activities: Activity[] = [];
	// public aliases: string[] = [];
	public states: State[] = [];
	public icon: string;
	public color: string;
	public standardsButtonText = 'Show Learning Standards';
	public expandTitle: boolean;
	public expandTitleMethod: TitleExpansionMethod;
	public hasSupplementaryContentSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	public hasSupplementaryContent$: Observable<boolean> = this.hasSupplementaryContentSubject.asObservable().pipe(
		distinctUntilChanged());
	public loadingSupplementaryContentSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	public loadingSupplementaryContent$: Observable<boolean> = this.loadingSupplementaryContentSubject.asObservable().pipe(
		distinctUntilChanged());
	public loadingSupplementaryContentProgressSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
	public loadingSupplementaryContentProgress$: Observable<number> = this.loadingSupplementaryContentProgressSubject.asObservable().pipe(
		distinctUntilChanged());
	public userGuidePath: string[] = ['${productPath}', 'notes', 'User Guide.pdf'];
	public contentsGuidePath: string[] = ['${productPath}', 'notes', 'Contents Guide.pdf'];
	public productPropsPath: string[] = ['${productPath}', 'curricula', 'productProps.xml'];
	public screenshotsPath: string[] = ['${productPath}', 'screenshots'];
	public statesPath: string[] = ['${productPath}', 'curricula', 'states.xml'];
	public activitiesPath: string[] = ['${productPath}', 'curricula', 'Activities.xml'];
	public stateMappingPaths: Record<string, string[]> = {};
	public stateStandardsPaths: Record<string, string[]> = {};
	public defaultScreenshotsPath: string[] = ['${productPath}', 'screenshots'];
	public defaultStateMappingPath: string[] = ['${productPath}', 'curricula', '${stateAbbreviation}Mapping.xml'];
	public defaultStateStandardsPath: string[] = ['${productPath}', 'curricula', '${stateAbbreviation}Standards.xml'];
	public defaultDownloadedContentPath: string[] = ['${downloadedContent}', '${productName}'];
	public placeholderScreenshotPath: string[] = [];
	public hasScreenshots: boolean;
	public hasContentsGuideSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(undefined);
	public hasContentsGuide$: Observable<boolean> = this.hasContentsGuideSubject.asObservable().pipe(distinctUntilChanged());
	public hasUserGuideSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(undefined);
	public hasUserGuide$: Observable<boolean> = this.hasUserGuideSubject.asObservable().pipe(distinctUntilChanged());
	private _selectedState: State;
	private _title: string;
	private _indexed: boolean;

	get id(): string {
		/*const app: string = this.application.replace(/ /g, '-').toLowerCase();
		return `${app}@${this.version}`;*/
		return this.path64;
	}

	get selectedState(): State {
		return this._selectedState;
	}

	get path64(): string {
		return btoa(this.path);
	}

	get iconUrl(): string {
		if (this.icon != null) {
			return 'assets/icons/' + this.icon;
		} else {
			return unknownIcon;
		}
	}

	get productColor(): string {
		if (this.color != null) {
			return this.color;
		} else {
			return undefined;
		}
	}

	get productTitle(): string {
		return this._title;
	}

	get title(): string {
		if (this.expandTitle) {
			return this.getExpandedTitle(this.expandTitleMethod);
		}
		return this._title;
	}

	set title(value: string) {
		this._title = value;
	}

	get tagline(): string {
		// return `for ${this.statesOnly[0].name}'s Learning Standards`;
		return this.statesOnly[0].name;
	}

	get statesOnly(): State[] {
		return this.states.filter(state => state.abbreviation !== 'BT' && state.abbreviation !== 'BS');
									// && state.abbreviation !== 'DCI' && state.abbreviation !== 'PE');
	}

	get mappableStatesOnly(): State[] {
		return this.states.filter(state => state.mappable === true);
	}

	get locale(): ProductLocale {
		const statesOnly: State[] = this.statesOnly;
		let locale: ProductLocale;
		statesOnly.every(state => {
			if (state.locale) {
				locale = state.locale;
				return false;
			}
			return true;
		});
		return locale;
	}

	get selectedStateAbbreviation(): string {
		return this.selectedState.abbreviation;
	}

	set selectedStateAbbreviation(selected: string) {
		this._selectedState = this.states.find(value => value.abbreviation === selected);
	}

	get isIndexed(): boolean {
		return this._indexed;
	}

	get supplementaryContentUrl(): string {
		if (this.application) {
			return `https://s3.amazonaws.com/boardworks-front-end/supplementary-content/${this.application.replace(/ {1}/g, '+')}.zip`;
		}
		return undefined;
	}

	constructor(public path: string, application?: string, version?: string ) {
		this.application = application;
		this.version = version;
		this.type = ProductType.STANDARD;
	}

	initialize(data: IProduct): Product {
		Object.assign(this, data);
		return this;
	}

	index(searchService: SearchService): Observable<boolean> {
		if (!this.isIndexed) {
			return Observable.create((subscriber: Subscriber<boolean>) => {
				// Add the product search documents to the search service
				console.log(`Creating search documents for ${this.title}`);
				const docs: SearchDocument[] = this.buildSearchDocuments();
				console.log(`Adding ${docs.length} search documents for: ${this.title}`);
				searchService.add(docs);
				console.log(`Added`);
				this._indexed = true;
				subscriber.next(true);
			});
		} else {
			return of(true);
		}
	}

	getExpandedTitle(method: TitleExpansionMethod): string {
		switch (method) {
			case TitleExpansionMethod.STATE:
				if (this.statesOnly.length === 1) {
					const stateName: string = this.statesOnly[0].name;
					return `${this._title} for ${stateName}`;
				} else {
					return this._title;
				}
			case TitleExpansionMethod.APPLICATION:
				return this.application.replace(/^Boardworks/, '');
		}
		/* if (this.statesOnly.length === 1) {
			const stateName: string = this.statesOnly[0].name;
			return `${this._title} for ${stateName}`;
		}
		if (this.statesOnly.length === 0) {
			return this.application.replace(/^Boardworks/, '');
		} */
	}

	getActivity(ref: number): Activity {
		return this.activities.find((value: Activity) => value.ref === ref);
	}

	getState(abbreviation: string): State | undefined {
		return this.states.find(value => value.abbreviation === abbreviation);
	}

	toString(): string {
		return this.application;
	}

	findTemplateForApplication(templates: ProductTemplates, application: string): IProduct {
		const template: IProduct = templates.products.find((p: IProduct, index: number, arr: IProduct[]) => {
			if (p.application != null && p.application === application) {
				return true;
			}
			return false;
		});
		return template;
	}

	findTemplate(templates: ProductTemplates): IProduct {
		return this.findTemplateForApplication(templates, this.application);
	}

	checkScreenshots(electronService: ElectronService): Promise<boolean> {
		const path: string = this.getPath(this.defaultScreenshotsPath, electronService);
		const downloadedPath: string = this.getPath(this.defaultDownloadedContentPath.concat('screenshots'), electronService);
		return new Promise<boolean>((resolve, reject) => {
			electronService.fileListInFolders([path]).then(entries => {
				if (entries && entries.length > 0) {
					console.log(`Product ${this.application} screenshots: ${entries}`);
					this.screenshotsPath = this.defaultScreenshotsPath.slice(0);
					resolve(true);
				} else {
					electronService.fileListInFolders([downloadedPath]).then(entriesDLP => {
						if (entriesDLP && entriesDLP.length > 0) {
							console.log(`Product ${this.application} screenshots DLP: ${entriesDLP}`);
							this.screenshotsPath = this.defaultDownloadedContentPath.concat('screenshots');
							resolve(true);
						} else {
							resolve(false);
						}
					}).catch(reasonDLP => {
						resolve(false);
					});
				}
			}).catch(reason => {
				resolve(false);
			});
		});
	}

	hasContentsGuide(electronService: ElectronService): Observable<boolean> {
		const path: string = this.getPath(this.contentsGuidePath, electronService);
		if (this.hasContentsGuideSubject.getValue() == null) {
			electronService.fileExists(path).then((value) => {
				this.hasContentsGuideSubject.next(value);
			}).catch(() => {
				this.hasContentsGuideSubject.next(false);
			});
		}
		return this.hasContentsGuide$;
	}

	hasUserGuide(electronService: ElectronService): Observable<boolean> {
		const path: string = this.getPath(this.userGuidePath, electronService);
		if (this.hasUserGuideSubject.getValue() == null) {
			electronService.fileExists(path).then((value) => {
				this.hasUserGuideSubject.next(value);
			}).catch(() => {
				this.hasUserGuideSubject.next(false);
			});
		}
		return this.hasUserGuide$;
	}

	getPath(parts: string[], electronService: ElectronService, tokens: Record<string, string> = {}): string {
		const ap: string = electronService.applicationPath;
		// console.log(`Application path is: ${ap}`);
		const t: Record<string, string> = Object.assign({
			'productPath': this.path,
			'appPath': ap,
			'assetsPath': electronService.pathJoin(ap, 'dist', 'assets'),
			'downloadedContent': electronService.downloadedContentPath,
			'productName': this.application ? this.application : '',
		}, tokens);
		// console.log(`Get path from parts: ${parts.join(';')}`);
		const tokenizer: Tokenizer = new Tokenizer(t);
		const pathParts: string[] = parts.map(value => {
			return tokenizer.expand(value);
		});
		// console.log(`Tokenized parts: ${pathParts.join(';')}`);
		return electronService.pathJoin(...pathParts);
	}

	getUserGuidePath(electronService: ElectronService): string {
		return this.getPath(this.userGuidePath, electronService);
	}

	getContentsGuidePath(electronService: ElectronService): string {
		return this.getPath(this.contentsGuidePath, electronService);
	}

	getStateMappingPath(stateAbbreviation: string, electronService: ElectronService): string {
		let p: string[] = this.defaultStateMappingPath.slice(0);
		// If we have an override for this state then use it
		if (this.stateMappingPaths != null && this.stateMappingPaths[stateAbbreviation] != null) {
			p = this.stateMappingPaths[stateAbbreviation].slice(0);
		}
		return this.getPath(p, electronService, {'stateAbbreviation': stateAbbreviation});
	}

	getStateStandardsPath(stateAbbreviation: string, electronService: ElectronService): string {
		let p: string[] = this.defaultStateStandardsPath.slice(0);
		// If we have an override for this state then use it
		if (this.stateStandardsPaths != null && this.stateStandardsPaths[stateAbbreviation] != null) {
			p = this.stateStandardsPaths[stateAbbreviation].slice(0);
		}
		return this.getPath(p, electronService, {'stateAbbreviation': stateAbbreviation});
	}

/* 	loadProperties(electronService: ElectronService, stateTemplates?: IState[], productTemplates?: ProductTemplates): Observable<Product> {
		return of(this).pipe(
			this.loadProductProperties(electronService, productTemplates)
		);
	} */

	loadStream(electronService: ElectronService, http: HttpClient, stateTemplates?: IState[],
				productTemplates?: ProductTemplates): Observable<IProduct> {
		return Observable.create((observer: Observer<IProduct>) => {
			const subscription = of(this).pipe(
				mergeProductPreloadTemplate(productTemplates),
				mergeMap(product => {
					console.log(`Type: ${product.type}`);
					const p = product as Product;
					if (product.type === ProductType.STANDARD) {
						console.log(`Load Standard product`);
						return of(p).pipe(
							p.loadProductStates(electronService, productTemplates, stateTemplates),
							p.loadProductActivities(electronService, productTemplates),
							p.loadProductMapping(electronService),
							p.loadProductStandards(electronService),
							p.checkProductScreenshots(electronService)
							// product.checkProductSupplementaryContent(electronService, http)
						);
					} else if (product.type === ProductType.STANDARDLESS) {
						return of(p).pipe(
							p.loadProductActivities(electronService, productTemplates),
							p.checkProductScreenshots(electronService)
							// product.checkProductSupplementaryContent(electronService, http)
						);
					}  else {
						return of(product);
					}
				}),
				mergeProductTemplate(productTemplates)
			).subscribe({
				next: (product: IProduct) => {
					observer.next(product);
					observer.complete();
				},
				error: (err) => {
					log.warn(`Error: ${err}`);
					observer.next(undefined);
					observer.complete();
				},
				complete: () => console.error(`LOAD STREAM COMPLETE`)
			});

			return subscription;
		});
	}

	load(electronService: ElectronService, stateTemplates?: IState[], productTemplates?: ProductTemplates): Observable<IProduct> {
		return of(this).pipe(
			mergeProductPreloadTemplate(productTemplates),
			this.loadProductStates(electronService, productTemplates, stateTemplates),
			this.loadProductActivities(electronService, productTemplates),
			this.loadProductMapping(electronService),
			this.loadProductStandards(electronService),
			mergeProductTemplate(productTemplates),
			tap(product => console.log(`Loaded Product ${product.title}`)),
		);
	}

	loadProductPropertiesStream(electronService: ElectronService, templates: ProductTemplates): Observable<IProduct> {
		return Observable.create((observer: Observer<IProduct>) => {
			const productPropsXML: string = this.getPath(this.productPropsPath, electronService);
			// const productPropsXML: string = electronService.pathJoin(product.path, 'curricula', 'ProductProps.xml');
			electronService.fileExists(productPropsXML).then((exists: boolean) => {
				if (exists) {
					log.info(`Loading product properties: ${productPropsXML}`);
					return electronService.readFile(productPropsXML).then((data: Buffer) => {
						// Patch the xml
						const patcher: Patcher = new Patcher(templates);
						const patchedXml: string = patcher.patch(this, ProductPatchConfigurationNames.ProductProps, data.toString());
						// Sanitise xml
						const xml: string = new Sanitiser().sanitiseXml(patchedXml);
						const parser: DOMParser = new DOMParser();
						const doc: Document = parser.parseFromString(`<data>${xml}</data>`, 'application/xml');
						const titleElements: HTMLCollectionOf<HTMLTitleElement> = doc.getElementsByTagName('title') as HTMLCollectionOf<HTMLTitleElement>;
						if (titleElements.length > 0) {
							this.title = titleElements[0].textContent;
						}
						log.info(`Product Properties loaded for: ${productPropsXML}`);
						return this;
					});
				} else {
					log.info(`Product Properties file at ${productPropsXML} does not exist`);
					return this;
				}
			}).then((loadedProduct: IProduct) => {
				const t: number = Date.now();
				// Get the product application file name and version number
				return new Promise<IProduct>((resolve, reject) => {
					electronService.findFilesList(/\.exe$/, this.path).subscribe({
						next: (values) => {
							// Find the exe which matches a template (some products have more than one exe by mistake - find the correct one to use)
							const foundApplication: string = values.find(value => {
								const parts: string[] = value.split(electronService.pathSep);
								const exeName = parts.pop();
								console.log(`EXE NAME: ${exeName}`);
								const appName: string = exeName.replace(/\.exe$/, '');
								return this.findTemplateForApplication(templates, appName) != null;
							});
							if (foundApplication != null) {
								const parts: string[] = foundApplication.split(electronService.pathSep);
								const exeName = parts.pop();
								console.log(`FOUND EXE NAME: ${exeName}`);
								const appName: string = exeName.replace(/\.exe$/, '');
								// console.log(`Application for ${loadedProduct.title} is ${value}`);
								this.application = appName;
								electronService.getFileVersion(electronService.pathJoin(loadedProduct.path, exeName)).then((v: string) => {
									this.version = v;
									// console.log(`*** IN ${Date.now() - t}`);
									resolve(this);
								}).catch((reason) => {
									console.log(`Failed to get version: ${reason}`);
									this.version = '';
									resolve(this);
								});
							} else {
								resolve(this);
							}
						},
						error: (err) => console.error(`EXE ERROR: ${err}`),
						complete: () => {
							console.log(`EXE COMPLETE`);
							// resolve(this);
						}
					});
				});
			}).then((loadedProduct: IProduct) => {
				// console.log(`**Loaded product is: ${this}`);
				if (loadedProduct.application != null) {
					observer.next(loadedProduct);
					observer.complete();
				} else {
					log.warn(`No application found for product ${loadedProduct}`);
					observer.next(loadedProduct);
					observer.complete();
				}
				/* else {
					observer.next(loadedProduct);
					observer.complete();
				}*/
			}).catch(reason => {
				log.warn(`Error loading product properties: ${reason}`);
				observer.next(this);
				observer.complete();
			});
		});
	}

	checkProductScreenshots = (electronService: ElectronService) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			const subscriptions: Subscription[] = [];
			subscriptions.push(source.subscribe({
				next: (product: Product) => {
					product.checkScreenshots(electronService).then(result => {
						product.hasScreenshots = result;
						observer.next(product);
					}).catch(reason => {
						product.hasScreenshots = false;
						observer.next(product);
					});
				},
				error: (err) => {
					observer.error(err);
				}
			}));
		});
	}

	checkProductSupplementaryContent = (electronService: ElectronService, http: HttpClient) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			const subscriptions: Subscription[] = [];
			subscriptions.push(source.subscribe({
				next: (product: Product) => {
					observer.next(product);
					// Does this product have supplementary content available?
					if (product.supplementaryContentUrl) {
						http.head(product.supplementaryContentUrl).pipe(
							mapTo(true),
							catchError((error) => {
								console.log(`Error getting supplementary content for ${product.application}: ${error}`);
								return of(false);
							})
						).subscribe({
							next: (result) => {
								console.log(`Supplementary content for ${product.application} is: ${result}`);
								product.hasSupplementaryContentSubject.next(result);
								observer.next(product);
							},
							complete: () => {
								observer.complete();
							}
						});
					} else {
						observer.next(product);
					}
				},
				error: (err) => {
					observer.error(err);
				}
			}));
			return () => {
				subscriptions.forEach(value => value.unsubscribe());
			};
		});
	}

	loadProductSupplementaryContent = (electronService: ElectronService, http: HttpClient) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			const subscriptions: Subscription[] = [];
			subscriptions.push(source.subscribe({
				next: async (product: Product) => {
					if (product.application === 'Boardworks Shakespeare Macbeth') {
						console.log(`THE SCOTISH PLAY`);
					}
					const hasScreenshots = await product.checkScreenshots(electronService);
					// Check if this product needs to download supplementary content
					if (hasScreenshots) {
						this.loadingSupplementaryContentSubject.next(false);
						console.log(`Product ${product.application} has screenshots!!`);
						observer.next(product);
					} else {
						console.log(`Product ${product.application} has NO screenshots!!`);
						this.loadingSupplementaryContentProgressSubject.next(0);
						this.loadingSupplementaryContentSubject.next(true);
						const screenshotsUrl = `${product.supplementaryContentUrl}/screenshots.zip`;
						const type = 'application/zip';
						/*const options = new RequestOptions({
							responseType: ResponseContentType.Blob,
							headers: new HttpHeaders({ 'Accept': type })
						});*/
						const headers: HttpHeaders = new HttpHeaders({'Accept': type});
						const request: HttpRequest<Buffer> = new HttpRequest('GET', screenshotsUrl, {
							headers,
							reportProgress: true,
							responseType: 'arraybuffer'
						});
						http.request<ArrayBuffer>(request).subscribe({
							next: event => {
								if (event.type === HttpEventType.DownloadProgress) {
									const percent: number = (Math.round(100 * event.loaded / event.total));
									product.loadingSupplementaryContentProgressSubject.next(percent);
								}
								if (event.type === HttpEventType.Response) {
									// Save the new version in the temp folder
									const path = electronService.pathJoin(electronService.remote.app.getPath('temp'), 'screenshots.zip');
									const buffer: Buffer = Buffer.from(event.body);
									electronService.writeFile(path, buffer).then((done) => {
										if (done) {
											product.loadingSupplementaryContentProgressSubject.next(100);
											product.loadingSupplementaryContentSubject.next(false);
										}
									});
								}
							},
							error: (err) => {
								console.log(`HTTP ERROR: ${err}`);
								observer.error(err);
							}
						});
						observer.next(product);
					}
				},
				error: (err) => {
					console.error(`Loading Supplementary Content Error: ${err}`);
					observer.next(this);
				},
				complete: () => {
					console.log(`SUPPLEMENTS COMPLETE`);
					observer.complete();
				}
			}));
			return () => {
				subscriptions.forEach(value => value.unsubscribe());
			};
		});
	}

	loadProductStates = (electronService: ElectronService,
		templates: ProductTemplates, stateTemplates?: IState[]) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			const subscriptions: Subscription[] = [];
			subscriptions.push(source.subscribe({
				next: (product: Product) => {
					const productStatesXML: string = product.getPath(product.statesPath, electronService);
					return new Promise<Product>((resolve, reject) => {
					/*	electronService.fileExists(productStatesXML).then((exists: boolean) => {
							if (exists) {*/
								electronService.readFile(productStatesXML).then((data: Buffer) => {
									// Patch the xml
									const patcher: Patcher = new Patcher(templates);
									const patchedXml: string = patcher.patch(this, ProductPatchConfigurationNames.States, data.toString());
									const xml: string = new Sanitiser().sanitiseXml(patchedXml);
									const parser: DOMParser = new DOMParser();
									const doc: Document = parser.parseFromString(`<data>${xml}</data>`, 'application/xml');
									const stateElements: HTMLCollectionOf<Element> = doc.getElementsByTagName('state') as HTMLCollectionOf<Element>;
									const stateElementsArray: Element[] = Array.from(stateElements);
									subscriptions.push(from(stateElementsArray).pipe(
										map<Element, State>(element => {
											const stateDescription: string = element.getAttribute('name');
											const stateAbbreviation: string = element.getAttribute('short');
											return new State(this, stateDescription, stateAbbreviation).mergeTemplate(stateTemplates);
										})
									).subscribe({
										next: state => {
											this.states.push(state);
										},
										error: (err) => log.error(err),
										complete: () => {
											resolve(product);
										}
									}));
								}).catch(reason => {
									reject(`Product States file at ${productStatesXML} does not exist: ${reason}`);
								});
							/*} else {
							}
						});*/
					}).then((loadedProduct: Product) => {
						observer.next(loadedProduct);
					}).catch(reason => {
						log.warn(`Error loading states: ${reason}`);
						observer.error(reason);
						observer.complete();
					});
				}
			}));
			return () => {
				subscriptions.forEach(value => value.unsubscribe());
			};
		});
	}

	loadProductActivities = (electronService: ElectronService,
		templates: ProductTemplates) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			return source.subscribe({
				next: (product: Product) => {
					return new Promise<Product>((resolve, reject) => {
						const productActivitiesXML: string = product.getPath(product.activitiesPath, electronService);
							// electronService.pathJoin(product.path, 'curricula', 'Activities.xml');
						electronService.fileExists(productActivitiesXML).then((exists: boolean) => {
							if (exists) {
								electronService.readFile(productActivitiesXML, 'windows-1252').then((data: Buffer) => {
									// Patch the xml
									const patcher: Patcher = new Patcher(templates);
									const patchedXml: string = patcher.patch(this, ProductPatchConfigurationNames.Activities, data.toString());
									const xml: string = new Sanitiser().sanitiseXml(patchedXml);
									const parser: DOMParser = new DOMParser();
									const doc: Document = parser.parseFromString(`<data>${xml}<\data>`, 'application/xml');
									const activityElements: HTMLCollectionOf<Element> = doc.getElementsByTagName('activity') as HTMLCollectionOf<Element>;
									Array.from(activityElements).forEach(element => {
										const activityRef: number = parseInt(element.getAttribute('ref'), 10);
										const activityTitle: string = element.getAttribute('title');
										const activityDisplayTitle: string = element.getAttribute('displayTitle');
										const activityLength: number = parseInt(element.getAttribute('length'), 10);
										const activityFeature: number = parseInt(element.getAttribute('feature'), 10);
										const activityDescription: string = element.getAttribute('description');
										product.activities.push(new Activity(this, activityRef, activityTitle,
																		activityDisplayTitle, activityLength,
																		activityFeature, activityDescription));
									});
									resolve(product);
								});
							} else {
								log.warn(`Product activities do not exist at ${productActivitiesXML}`);
								resolve(product);
							}
						});
					}).then((loadedProduct: Product) => {
						observer.next(loadedProduct);
					});
				},
				error: (err) => {
					observer.error(err);
				}
			});
		});
	}

	loadProductMapping = (electronService: ElectronService) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			return source.subscribe({
				next: (product: Product) => {
					return new Promise<Product>((resolve, reject) => {
						product.states.forEach((state: State) => {
							const stateMappingXML: string = product.getStateMappingPath(state.abbreviation, electronService);
							// electronService.pathJoin(product.path, 'curricula', `${state.abbreviation}Mapping.xml`);
							electronService.fileExists(stateMappingXML).then((exists: boolean) => {
								if (exists) {
									electronService.readFile(stateMappingXML, 'windows-1252').then((data: Buffer) => {
										const xml: string = new Sanitiser().sanitiseXml(data.toString());
										const parser: DOMParser = new DOMParser();
										const doc: Document = parser.parseFromString(`<data>${xml}</data>`, 'application/xml');
										const standardElements: HTMLCollectionOf<Element> = doc.getElementsByTagName('standard') as HTMLCollectionOf<Element>;
										Array.from(standardElements).forEach(element => {
											const ref: number = parseInt(element.getAttribute('ref'), 10);
											const mapping: Mapping = new Mapping(ref);
											const activityRefs: HTMLCollectionOf<Element> = element.getElementsByTagName('activity') as HTMLCollectionOf<Element>;
											Array.from(activityRefs).forEach(activityEl => {
												const aRef: number = parseInt(activityEl.getAttribute('ref'), 10);
												const activityRef: ActivityReference = new ActivityReference(aRef);
												mapping.activities.push(activityRef);
											});
											state.mapping.push(mapping);
										});
										resolve(product);
									});
								} else {
									log.warn(`Product mapping do not exist at ${stateMappingXML}`);
									resolve(product);
								}
							});
						});
					}).then((loadedProduct: Product) => {
						observer.next(loadedProduct);
					});
				},
				error: (err) => {
					observer.error(err);
				}
			});
		});
	}

	loadProductStandards = (electronService: ElectronService) => (source: Observable<Product>) => {
		return new Observable<Product>((observer: Observer<Product>) => {
			return source.subscribe({
				next: (product: Product) => {
					return new Promise<Product>((resolve, reject) => {
						product.states.forEach((state: State) => {
							const stateMappingXML: string = product.getStateStandardsPath(state.abbreviation, electronService);
							// electronService.pathJoin(product.path, 'curricula', `${state.abbreviation}Standards.xml`);
							electronService.fileExists(stateMappingXML).then((exists: boolean) => {
								if (exists) {
									electronService.readFile(stateMappingXML, 'windows-1252').then((data: Buffer) => {
										if (stateMappingXML === 'C:\\Program Files (x86)\\Boardworks\\Middle School Math Oklahoma\\curricula\\OKStandards.xml') {
											console.log(`Loading: C:\\Program Files (x86)\\Boardworks\\Middle School Math Oklahoma\\curricula\\OKStandards.xml`);
										}
										const xml: string = new Sanitiser().sanitiseXml(data.toString());
										const parser: DOMParser = new DOMParser();
										const doc: Document = parser.parseFromString(xml, 'application/xml');
										const childElements: HTMLCollectionOf<Element> = doc.getElementsByTagName(`h0`) as HTMLCollectionOf<Element>;
										if (childElements.length === 1) {
											state.standards = Standards.loadStandards(childElements[0]);
										} else if (childElements.length > 1) {
											state.standards = new Standards(-1, product.productTitle, undefined);
											Array.from(childElements).forEach((child: Element) => {
												state.standards.children.push(Standards.loadStandards(child, state.standards));
											});
										} else {
											console.error(`Unable to parse ${stateMappingXML}`);
										}
										resolve(product);
									});
								} else {
									log.warn(`Product standards do not exist at ${stateMappingXML}`);
									resolve(product);
								}
							});
						});
					}).then((loadedProduct: Product) => {
						observer.next(loadedProduct);
					});
				},
				error: (err) => {
					observer.error(err);
				}
			});
		});
	}

	buildSearchDocuments(): SearchDocument[] {
		const docs: SearchDocument[] = [];
		docs.push({
			id: this.id,
			type: SearchableTypes.Product,
			productId: this.id,
			title: this.title,
			fields: ['title']
		} as ProductSearchDocument);
		this.states.forEach((state: State) => {
			docs.push(...state.buildSearchDocuments());
		});
		this.activities.forEach((activity: Activity) => {
			docs.push(...activity.buildSearchDocuments());
		});
		return docs;
	}
}
