import { ProductService } from './product.service';
import { ElectronService } from '../electron/electron.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { ElectronTestingService, DefaultElectronTestingConfiguration,
		programFilesPath, programFilesEnv } from '../electron/testing/electron-testing.service';
import { RouterTestingModule } from '@angular/router/testing';
import { IState } from './state';
import { Product } from './product';
import { ProductTemplates } from './product-templates';

describe('ProductService', () => {
	let service: ProductService;
	let httpMock: HttpTestingController;
	let mockElectronService: ElectronTestingService;

	const stateTemplates: any = require('../../../assets/states.json');
	const productTemplates: any = require('../../../assets/products.json');

	// let httpMock: HttpTestingController;
	beforeEach(async(() => {

		const mockNgZone = jasmine.createSpyObj('mockNgZone', ['run', 'runOutsideAngular']);
		mockNgZone.run.and.callFake(fn => fn());

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);
		const bed = TestBed.configureTestingModule({
			declarations: [ ],
			schemas: [ ],
			imports: [
				HttpClientTestingModule,
				RouterTestingModule
			],
			providers: [
				// { provide: NgZone, useValue: mockNgZone },
				{ provide: ElectronService, useValue: mockElectronService},
				ProductService
			]
		});
		httpMock = bed.get(HttpTestingController);
		service = bed.get(ProductService);
	}));

	afterEach(() => {
		httpMock.verify();
	});

	it('should create', () => {
		expect(service).toBeTruthy();
	});

	/*it('should have a configured Program Files location', () => {
		const env: string = service.electronService.environmentVariable(programFilesEnv);
		console.log(`Env: ${env}`);
		console.log(`Should equal: ${programFilesPath}`);
		expect(env).toEqual(programFilesPath);
	});*/

	it('should have an activity xml file', (done) => {
		service.electronService.readFile(`${programFilesPath}\\Boardworks\\MS English Ohio\\curricula\\Activities.xml`).then(data => {
			const activity: string = data.toString();
			expect(activity.length).toBeGreaterThan(0);
			done();
		});
	});

	it('should load the state templates', (done) => {
		service.loadStateTemplates().subscribe({
			next: (value: IState[]) => {
				expect(value.length).toBeGreaterThan(0);
				done();
			},
			error: (err) => console.warn(err),
		});

		const req = httpMock.expectOne('assets/states.json');
		expect(req.request.method).toBe('GET');
		req.flush(stateTemplates);
	});

	it('should load the product templates', (done) => {
		service.loadProductTemplates().subscribe({
			next: (value: ProductTemplates) => {
				expect(value.products.length).toBeGreaterThan(0);
				done();
			},
			error: (err) => console.warn(err),
		});
		const req = httpMock.expectOne('assets/products.json');
		expect(req.request.method).toBe('GET');
		req.flush(productTemplates);
	});

	it('should find no products when not running in Electron', (done) => {
		mockElectronService.configuration.electron = false;
		let products: Product[] = [];
		service.getProducts().subscribe({
			next: (p: Product[]) => {
				products = p;
			},
			complete: () => {
				expect(products.length).toBe(0);
				done();
			}
		});
		const stateReq = httpMock.expectOne('assets/states.json');
		expect(stateReq.request.method).toBe('GET');
		stateReq.flush(stateTemplates);
		const prodReq = httpMock.expectOne('assets/products.json');
		expect(prodReq.request.method).toBe('GET');
		prodReq.flush(productTemplates);
	});
/*
	it('should load the products', (done) => {
		service.getProducts().subscribe({
			next: (products: Product[]) => {
				expect(products.length).toBe(1);
				done();
			},
			error: (err) => console.error(err),
			complete: () => console.log(`Completed!`)
		});
		const stateReq = httpMock.expectOne('assets/states.json');
		expect(stateReq.request.method).toBe('GET');
		stateReq.flush(stateTemplates);
		const prodReq = httpMock.expectOne('assets/products.json');
		expect(prodReq.request.method).toBe('GET');
		prodReq.flush(productTemplates);
	});*/
});
