import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from '../electron/electron.module';
import { Observer, Observable, empty, from, zip, of, Subscription, throwError, merge, EMPTY } from 'rxjs';
// tslint:disable-next-line:max-line-length
import { map, mergeMap, concatMap, switchMap, flatMap, share, filter, distinctUntilChanged, scan, tap, catchError, toArray, take, reduce, groupBy, exhaustMap, expand } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';
import { Product, ProductLocale, TitleExpansionMethod } from './product';
import { HttpClient } from '@angular/common/http';
import { IState } from './state';
import { SearchService } from '../search/search.service';
import { SettingsService } from '../settings/settings.service';
import { Router } from '@angular/router';
import { SemVer, satisfies } from 'semver';
import { ProductTemplates, ProductType, IProduct } from './product-templates';
import { loadProductsProperties, loadProducts } from './observables';
import { platform } from 'os';
import { MacProduct } from './mac-product';
const { remote } = window.require('electron');
const log = remote.require('electron-log');
// import AWS = require ('aws-sdk');

interface ProductFolderProperties {
	path: string;
	application: string;
	version: string;
}

const compareProductTitle = (a: Product, b: Product) => {
	if (a.title < b.title) {
		return -1;
	} else if (a.title > b.title) {
		return 1;
	}
	return 0;
};



@Injectable()
export class ProductService {
	stateTemplates: IState[];
	templates: ProductTemplates;

	productSubject: BehaviorSubject<Product[]> = new BehaviorSubject([]);
	product$: Observable<Product[]> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	productsLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
	productsLoading$: Observable<boolean> = this.productsLoadingSubject.asObservable().pipe(distinctUntilChanged());

	productSubscription: Subscription;

	isInitialised: boolean;

	/// productCache: Record<string, BehaviorSubject<Product>> = {};
	// private subscriptions: Subscription[];

	constructor(private router: Router, private ngZone: NgZone, private http: HttpClient, private settingsService: SettingsService,
		public electronService: ElectronService, public searchService: SearchService) {
			// this.subscriptions = [];
	}

	cleanUp() {
		if (this.productSubscription != null && !this.productSubscription.closed) {
			this.productSubscription.unsubscribe();
		}
	}

	init() {
		this.isInitialised = true;
		this.cleanUp();
		this.productSubscription = this.settingsService.productLocations$.pipe(
			exhaustMap(locations => {
				console.error(`DO RELOAD`);
				this.productsLoadingSubject.next(true);
				this.productSubject.next([]);
				return this.getProducts();
			})
		).subscribe({
			next: (value) => {
				console.log(`Items: ${value.length}`);
				this.ngZone.run(() => {
					this.productsLoadingSubject.next(false);
				});
			},
			error: (err) => console.error(`ERROR: ${err}`),
			complete: () => {
				console.warn(`COMPLETED`);
				this.ngZone.run(() => {
					this.productsLoadingSubject.next(false);
				});
			}
		});
	}

	/*clearCache() {
		this.productCache = {};
	}*/

	isSupported(product: IProduct): boolean {
		console.log(`Is Supported: ${product.path} = ${product.productTitle} - ${product.application}`);
		if (this.templates != null && this.templates.unsupported != null) {
			if (this.templates.unsupported.includes(product.application)) {
				log.warn(`Product ${product.application} at ${product.path} is not supported.`);
				return false;
			}
		}
		return true;
	}

	getProduct(id: string, loadIfMissing: boolean = true): Observable<Product> {
		/*if (this.productCache[id] != null) {
			return this.productCache[id].asObservable();
		} else {
			if (this.productCache[id] == null) {
				console.warn(`Reloading products`);
				this.productCache[id] = new BehaviorSubject(undefined);
				this.getProducts().subscribe();
			}
			return this.productCache[id].asObservable();
		}*/
		const productList: Product[] = this.productSubject.getValue();
		let selected: Product;
		productList.every(product => {
			if (product.id === id) {
				selected = product;
				return false;
			}
			return true;
		});
		if (selected == null && loadIfMissing) {
			const ret: Observable<Product> = this.product$.pipe(
				map(products => {
					const matching: Product[] = products.filter(product => {
						return product.id === id;
					});
					if (matching.length === 1) {
						return matching[0];
					} else {
						return undefined;
					}
				})
			);
			if (!this.isInitialised) {
				this.init();
			}
			return ret;
		} else {
			return of(selected);
		}
	}

	getBestTitleExpansionMethod(products: Array<Product>): TitleExpansionMethod {
		for (const param in TitleExpansionMethod) {
			if (TitleExpansionMethod.hasOwnProperty(param)) {
				let unique = true;
				const names: string[] = [];
				for (let i = 0; i < products.length; i++) {
					// Try this method
					const type = Number(param);
					const name: string = products[i].getExpandedTitle(type);
					if (names.indexOf(name) === -1) {
						names.push(name);
					} else {
						// Names match - try next method
						unique = false;
						break;
					}
				}
				if (unique) {
					const type = Number(param);
					return type;
				}
			}
		}
	}

	/**
	 * Gets a stream of an array of the products on the local system.
	 * @returns Observable<Product[]> The products on the system
	 */
	getProducts(): Observable<Product[]> {
		this.searchService.reset(this);
		return this.loadStateTemplates().pipe(
			switchMap(() => {
				return this.loadProductTemplates().pipe(
					flatMap((value) => {
						return this.products();
					}),
					map((products: Product[]) => {
						const matchingProducts: Record<string, Array<Product>> = {};
						// Find any products with duplicate titles and expand them
						products.forEach((product, index, array) => {
							const name: string = product.productTitle;
							let count = 0;
							array.forEach(p => {
								if (p.productTitle === name) {
									count++;
								}
							});
							if (count > 1) {
								if (matchingProducts[name] == null) {
									matchingProducts[name] = new Array<Product>();
								}
								matchingProducts[name].push(product);
							}
						});

						// For each set of matching products determine the best title expansion method
						for (const param in matchingProducts) {
							if (matchingProducts.hasOwnProperty(param)) {
								const method: TitleExpansionMethod = this.getBestTitleExpansionMethod(matchingProducts[param]);
								matchingProducts[param].forEach((expandProduct) => {
									expandProduct.expandTitle = true;
									expandProduct.expandTitleMethod = method;
								});
							}
						}

						// Sort the products alphabetically by title
						return products.sort(compareProductTitle);
					}),
					tap(x => {
						// Update the product subject value within the Angular scope (so that updates propogate)
						this.ngZone.run(() => {
							log.info(`Found: ${x.length} products`);
							this.productSubject.next(x);
						});
						// this.loadIcons();
					})
				);
			})
		);
				/*	}
					),
					switchMap(value => {
						return this.product$;
					})
				)
			)
		);*/
		// return Observable.of([new Product('')]);
	}

	productsLoaded(): boolean {
		return this.productSubject.getValue().length > 0;
	}

	reloadProducts() {
		/*console.error(`RELOAD`);
		if (this.productSubscription != null && !this.productSubscription.closed) {
			this.productSubscription.unsubscribe();
		}
		this.productSubject.next([]);
		this.productSubscription = this.getProducts().subscribe();*/
	}

	/**
	 * Returns (an Observable of) an array of potential product folders
	 */
	products(): Observable<IProduct[]> {
		if (this.electronService.isElectron()) {
			const locations: string[] = this.settingsService.getProductLocations();
			const platformType: string = platform();
			let products$;
			if (platformType === 'win32') {
				products$ = from(this.electronService.fileListInFolders(locations, true, true)).pipe(
					loadProductsProperties(this.electronService, Product, this.templates, this.stateTemplates)
				);
			}
			else if (platformType === 'darwin') {
				products$ = from(this.electronService.fileListInFolders(locations, true, true)).pipe(
					loadProductsProperties(this.electronService, MacProduct, this.templates, this.stateTemplates)
				);
			} else {
				throw new Error(`Unsupported platform: ${platformType}`);
			}

			return products$.pipe(
				map((products: IProduct[]) => {
					return products.filter(product => this.isSupported(product));
				}),
				loadProducts(this.electronService, this.http, this.templates, this.stateTemplates),
				map((products: IProduct[]) => {
					return products.filter(p => p != null);
				})
			);
		} else {
			console.log(`Is not electron`);
			return EMPTY;
		}
	}

	loadStateTemplates(): Observable<IState[]> {
		return this.http.get('assets/states.json').pipe(
			map<Object, IState[]>(data => {
				this.stateTemplates = Array.from(data as Array<IState>);
				return this.stateTemplates;
			}),
			catchError((err) => {
				console.error(`Error Loading State Templates: ${err}`);
				return throwError(err);
			})
		);
	}

	loadProductTemplates(): Observable<ProductTemplates> {
		return this.http.get('assets/products.json').pipe(
			map((templates: ProductTemplates) => {
				this.templates = templates;
				return templates;
			}),
			catchError((err, caught: Observable<ProductTemplates>) => {
				console.log(`Error loading templates`);
				return caught;
			})
		);
	}

	indexAllProducts(searchService: SearchService): Observable<boolean> {
		const indexed: Observable<boolean>[] = this.productSubject.getValue().map<Observable<boolean>>((value: Product) => {
			return value.index(searchService);
		});
		return zip(...indexed).pipe(
			map(value => {
				return true;
			})
		);
	}

	navigateToProduct(id: string, stateAbbreviation?: string) {
		this.getProduct(id).subscribe(product => {
			console.log(`Selected product ${product.path} = ${product.application}`);
			if (product.type === ProductType.STANDARD) {
				if (product.locale === ProductLocale.US && product.mappableStatesOnly.length > 1) {
					if (stateAbbreviation) {
						this.router.navigate(['./map', id, stateAbbreviation]);
					} else {
						this.router.navigate(['./map', id]);
					}
				} else {
					this.router.navigate(['./product', id]);
				}
			} else if (product.type === ProductType.STANDARDLESS) {
				this.router.navigate(['./stateless-product', id]);
			}
		});
	}
}
