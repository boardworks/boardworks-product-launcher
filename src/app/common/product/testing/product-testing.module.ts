import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductTestingService } from './product-testing.service';

@NgModule({
	imports: [
		CommonModule,
	],
	providers: [
		ProductTestingService
	]
})
export class ProductTestingModule {

}

export * from './product-testing.service';
