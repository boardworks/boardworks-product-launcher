import { Injectable, NgZone } from '@angular/core';
import { ElectronService } from '../../electron/electron.module';
import { Observable, Observer, of, BehaviorSubject } from 'rxjs';
import { map, flatMap, distinctUntilChanged, tap } from 'rxjs/operators';
import { Product } from '../product';
import { IState } from '../state';
import { IProduct } from '../product-templates';

function asyncIf<T> (conditionalSource: Observable<boolean>, thenSource: Observable<T>, elseSource: Observable<T>) {
	return conditionalSource.pipe(
		flatMap(conditional =>
			Observable.if(
				() => conditional,
				thenSource,
				elseSource
			)
		)
	);
}

function getProductTitleFromIcon(icon: string): string {
	let title: string = icon;
	title = title.replace(/^Lit_/, '');
	title = title.replace(/_icon\.svg$/, '');
	title = title.replace(/^ES/, 'ElementarySchool');
	title = title.replace(/^HSS?/, 'HighSchool');
	title = title.replace(/^MS/, 'MiddleSchool');
	title = title.replace(/ICCSS/g, 'I');
	title = title.replace(/([A-Z])/g, ' $1');
	title = title.replace(/^\s/, '');
	title = title.replace(/C C S S/, 'CCSS');
	title = title.replace(/I I/, 'II');
	return title;
}

const isFrontEndFolder = (electronService: ElectronService) => (source: Observable<string>) => {
	return new Observable<string>((observer: Observer<string>) => {
		console.log(`isFrontEndFolder`);
		return source.subscribe({
			next: (value: string) => {
				console.log(`Checking ${value}`);
				const productPropsXml = electronService.pathJoin(value, 'curricula', 'ProductProps.xml');
				electronService.access(productPropsXml, electronService.R_OK).then(accessError => {
					if (!accessError) {
						// Folder is a front-end folder. Pass it on.
						console.log(`IsProduct ${productPropsXml}`);
						observer.next(value);
					}
				});
			},
			error(err) { observer.error(err); },
			complete() {console.log('isFE complete'); observer.complete(); }
		});
	});
};

const compareProductTitle = (a: Product, b: Product) => {
	if (a.title < b.title) {
		return -1;
	} else if (a.title > b.title) {
		return 1;
	}
	return 0;
};
/*
export interface TestingProduct extends IProduct {

}*/

export interface ProductTestingConfiguration {
	products: IProduct[];
}

@Injectable()
export class ProductTestingService {
	stateTemplates: IState[];
	templates: IProduct[];

	private configuration: ProductTestingConfiguration;

	productSubject: BehaviorSubject<Product[]> = new BehaviorSubject([]);
	product$: Observable<Product[]> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	constructor() {
	}

	configure(configuration: ProductTestingConfiguration): ProductTestingService {
		this.configuration = configuration;
		return this;
	}

	getProduct(id: string): Observable<Product> {
		const p: Product = this.configuration.products.find(value => value.id === id) as Product;
		return of(p);
	}

	/**
	 * Gets a stream of an array of the products on the local system.
	 * @returns Observable<Product[]> The products on the system
	 */
	getProducts(): Observable<Product[]> {
		console.error(`GET PRODUCTS`);
		return of(this.configuration.products).pipe(
			map((value: Product[]) => {
				// Sort the products alphabetically by title
				return value.sort(compareProductTitle);
			}),
			tap(x => {
				this.productSubject.next(x);
			})
		);
	}
}
