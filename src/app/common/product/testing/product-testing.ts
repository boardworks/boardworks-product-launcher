import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../../electron/testing/electron-testing.module';
import { Product } from '../product';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IState } from '../state';
import { ProductTemplates, IProduct } from '../product-templates';
import { loadProduct } from '../observables';

const stateTemplatesData: string = require('raw-loader!../../../../testing-assets/states.json');
const productsTemplatesData: string = require('raw-loader!../../../../testing-assets/products.json');

const mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

export class TestProduct {

	static templates: ProductTemplates;
	static stateTemplates: IState[];

	static loadStateTemplates(): Observable<IState[]> {
		const data: any = JSON.parse(stateTemplatesData);
		return of(data).pipe(
			map(arr => {
				TestProduct.stateTemplates = Array.from(arr as Array<IState>);
				return this.stateTemplates;
			}),
			catchError((err) => {
				return throwError(err);
			})
		);
	}

	static loadProductTemplates(): Observable<ProductTemplates> {
		const data: any = JSON.parse(productsTemplatesData);
		return of(data).pipe(
			map((templates: ProductTemplates) => {
				this.templates = templates;
				return templates;
			})
		);
	}

	static getProduct(folder: string): Observable<IProduct> {
		return of(new Product(folder)).pipe(
			loadProduct(mockElectronService)
		);
	}
}
