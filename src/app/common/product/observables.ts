import { ElectronService } from '../electron/electron.module';
import { ProductTemplates, IProduct, VersionedProduct, getVersionedProduct } from './product-templates';
import { IState } from './state';
import { Observable, Observer, Subscription, forkJoin } from 'rxjs';
import { platform } from 'os';
/* import { Product } from './product';
import { MacProduct } from './macProduct'; */
import { HttpClient } from '@angular/common/http';
import { applyTemplateToProduct } from './utils';
import { SearchService } from '../search/search.service';
const { remote } = window.require('electron');
const log = remote.require('electron-log');

export const loadProducts = (electronService: ElectronService, http: HttpClient, templates?: ProductTemplates,
	stateTemplates?: IState[]) => (source: Observable<IProduct[]>) => {
	return new Observable<IProduct[]>((observer: Observer<IProduct[]>) => {
		let innerSubscription: Subscription;
		const subscription: Subscription =  source.subscribe({
			next: (products: IProduct[]) => {
				console.log(`There are ${products.length} products to load`);
				const streams = products.map(product => {
					// This will emit the product with loaded properties and then complete
					return product.loadStream(electronService, http, stateTemplates, templates);
				});
				console.log(`There are ${streams.length} product streams to complete`);
				innerSubscription = forkJoin(streams).subscribe({
					next: (values) => {
						observer.next(values);
						console.log(`COMPLETE Load FORK`);
						observer.complete();
					},
					error: (err) => console.error(`er: ${err}`)
				});
			},
			error(err) { observer.error(err); },
			complete() { observer.complete(); }
		});
		return () => {
			if (innerSubscription) {
				innerSubscription.unsubscribe();
			}
			subscription.unsubscribe();
		};
	});
};

export const loadProduct = (electronService: ElectronService, templates?: ProductTemplates,
	stateTemplates?: IState[]) => (source: Observable<IProduct>) => {
	return new Observable<IProduct>((observer: Observer<IProduct>) => {
		let innerSubscription: Subscription;
		const subscription: Subscription =  source.subscribe({
			next: (product: IProduct) => {
				if (product != null) {
					innerSubscription = product.load(electronService, stateTemplates, templates).subscribe({
						next: (loadedProduct: IProduct) => {
							observer.next(loadedProduct);
							// observer.complete();
						}
					});
				}
			},
			error(err) { observer.error(err); },
			complete() {
				console.log(`COMPLETING Product LOAD`);
			}
		});
		return () => {
			if (innerSubscription) {
				innerSubscription.unsubscribe();
			}
			subscription.unsubscribe();
		};
	});
};

export const loadProductsProperties = (electronService: ElectronService, productConstructor: new (folder: string) => IProduct,
	 templates?: ProductTemplates, stateTemplates?: IState[]) => (source: Observable<string[]>) => {
	return new Observable<IProduct[]>((observer: Observer<IProduct[]>) => {
		let innerSubscription: Subscription;
		const subscription: Subscription =  source.subscribe({
			next: (folders: string[]) => {
				log.info(`Looking for products in: ${folders}`);
				const streams = folders.map(folder => {
					const platformType = platform();
					// tslint:disable-next-line: no-use-before-declare
					const product = new productConstructor(folder);
					// This will emit the product with loaded properties and then complete
					return product.loadProductPropertiesStream(electronService, templates);
				});
				console.log(`Join: ${streams.length} streams`);
				if (streams.length > 0) {
					innerSubscription = forkJoin(streams).subscribe({
						next: (values) => {
							observer.next(values);
							console.log(`COMPLETE FORK`);
							// observer.complete();
						},
						error: (err) => log.warn(`eror: ${err}`)
					});
				} else {
					log.info(`No products found`);
					observer.complete();
				}
			},
			error(err) {
				log.warn(`ERROR: ${err}`);
				observer.error(err);
			},
			complete() {
				console.log(`COMPLETING PROPERTY LOAD`);
			}
		});
		return () => {
			if (innerSubscription) {
				innerSubscription.unsubscribe();
			}
			subscription.unsubscribe();
		};
	});
};

export const mergeProductPreloadTemplate = (templates: ProductTemplates) => (source: Observable<IProduct>): Observable<IProduct> => {
	return new Observable<IProduct>((observer: Observer<IProduct>) => {
		return source.subscribe({
			next: (product: IProduct) => {
				if (templates != null) {
					const template: IProduct = product.findTemplate(templates);
					if (template != null) {
						// Get the preload template for this
						const preloads: VersionedProduct = templates.preload[template.application];
						const preload: IProduct | null = getVersionedProduct(preloads, product.version);
						// const preload: IProduct = templates.preload[template.application];
						if (preload != null) {
							// Apply these settings
							applyTemplateToProduct(product, preload);
						}
					}
				}
				observer.next(product);
			}
		});
	});
};

export const mergeProductTemplate = (templates: ProductTemplates) => (source: Observable<IProduct>): Observable<IProduct> => {
	return new Observable<IProduct>((observer: Observer<IProduct>) => {
		return source.subscribe({
			next: (product: IProduct) => {
				if (templates != null) {
					const template: IProduct = templates.products.find((p: IProduct, index: number, arr: IProduct[]) => {
						if (p.application != null && p.application === product.application) {
							return true;
						}/* else {
							if (p.aliases.indexOf(product.title) !== -1) {
								return true;
							}
						}*/
						return false;
					});
					if (template != null) {
						applyTemplateToProduct(product, template);
					}
				}
				observer.next(product);
			},
			error: (err) => observer.error(err)
		});
	});
};

export const indexProduct = (searchService: SearchService) => (source: Observable<IProduct>): Observable<IProduct> => {
	return new Observable<IProduct>((observer: Observer<IProduct>) => {
		return source.subscribe({
			next: (product: IProduct) => {
				product.index(searchService).subscribe(value => {
					observer.next(product);
				});
			}
		});
	});
};
