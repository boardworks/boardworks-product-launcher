import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductService } from './product.service';
import { ElectronModule } from '../electron/electron.module';

@NgModule({
	imports: [CommonModule, ElectronModule],
	providers: [ProductService],
})
export class ProductModule {

}

export * from './product.service';
export * from './activity';
export * from './mapping';
export * from './state';
export * from './product';
