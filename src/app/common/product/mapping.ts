import { ActivityReference } from './activity';

export class Mapping {
	activities: ActivityReference[];

	constructor(public ref: number) {
		this.activities = [];
	}
}
