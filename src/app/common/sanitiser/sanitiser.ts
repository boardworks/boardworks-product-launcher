export class Sanitiser {
	sanitiseXml(xml: string): string {
		// Replace single & with correct html code
		// Ignore character codes or entity codes (&...;)
		const clean: string = xml.replace(/(?!&[#0-9a-z]+;)&/g, '&amp;').replace('ï»¿', '');
		// Re-format attributes so that there are spaces between them
		const attributePattern = /\s*([a-zA-Z0-9\-_]+)\s*=\s*"/g;
		const reformatted = clean.replace(attributePattern, ' $1="');
		return reformatted;
	}
}
