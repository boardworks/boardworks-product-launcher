import { TestBed, inject } from '@angular/core/testing';

import { Sanitiser } from './sanitiser';

const sanitiser: Sanitiser = new Sanitiser();

const invalidAmpXml =
	// tslint:disable-next-line:max-line-length
	`<?xml version='1.0' encoding='UTF-8'?><root><h0 ref ="0"label="Alabama Course of Study &amp; - Social Studies grades 10 & 11"></h0></root>`;
const validAmpXml =
	// tslint:disable-next-line:max-line-length
	`<?xml version='1.0' encoding='UTF-8'?><root><h0 ref="0" label="Alabama Course of Study &amp; - Social Studies grades 10 &amp; 11"></h0></root>`;

describe('SanitiserService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
		});
	});

	it('should be created', () => {
		expect(sanitiser).toBeTruthy();
	});

	it('should sanitise malformed xml', () => {
		expect(sanitiser.sanitiseXml(invalidAmpXml)).toEqual(validAmpXml);
	});
});
