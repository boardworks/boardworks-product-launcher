export class Tokenizer {
	constructor(public tokens: Record<string, string>) {

	}

	expand(input: string): string {
		let text: string = input;
		Object.keys(this.tokens).forEach(pattern => {
			const reg: RegExp = new RegExp('\\${' + pattern + '}', 'g');
			/*
			In the token replace each dollar ($) with two dollars (using four in the replacement because each $$ means use $)
			so that if the input is $$ the token will be $$$$ and so will come out with $$ again after the replacement.
			This is an issue with usernames as by convension admin names on windows often have two dollar signs at the start,
			these would get replaced by one in the string replace as $$ means insert $.
			For details see:
			https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace#Specifying_a_string_as_a_parameter
			*/
			const token = this.tokens[pattern].replace(/\$/g, '$$$$');
			text = text.replace(reg, token);
		});
		return text;
	}
}
