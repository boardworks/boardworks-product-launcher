import { async} from '@angular/core/testing';

// tslint:disable-next-line:max-line-length
import { ElectronTestingService, DefaultElectronTestingConfiguration, ElectronTestingConfiguration } from '../electron/testing/electron-testing.module';
import { Tokenizer } from './tokenizer';

describe('ProductLocationComponent', () => {
	let mockElectronService: ElectronTestingService;
	let tokens: Record<string, string> = {};
	const productPath = 'C:\\Program Files (x86)\\Boardworks\\HS US History';
	const applicationPath = 'C:\\Users\\$$constance.young\\AppData\\Local\\Temp\\application';
	const fullPath = `${applicationPath}\\dist\\assets\\replacement-curricula\\ap\\states.xml`;

	beforeEach(async(() => {
		const config: ElectronTestingConfiguration = DefaultElectronTestingConfiguration;
		config.applicationPath = applicationPath;
		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		const ap: string = mockElectronService.applicationPath;
		console.log(`Application path is: ${ap}`);
		tokens = Object.assign({
			'productPath': productPath,
			'appPath': ap,
			'assetsPath': mockElectronService.pathJoin(ap, 'dist', 'assets')
		}, {});

	}));

	beforeEach(() => {

	});

	it('should create the tokens', () => {
		expect(tokens).toBeTruthy();
	});

	it('should resolve the tokenized path', () => {
		const parts: string[] = ['${assetsPath}', 'replacement-curricula', 'ap', 'states.xml'];
		console.log(`Get path from parts: ${parts.join(';')}`);
		console.log(`Asset path: ${tokens['assetsPath']}`);
		const tokenizer: Tokenizer = new Tokenizer(tokens);
		const pathParts: string[] = parts.map(value => {
			return tokenizer.expand(value);
		});
		console.log(`Tokenized parts: ${pathParts.join(';')}`);
		const result =  mockElectronService.pathJoin(...pathParts);
		expect(result).toEqual(fullPath);
	});
});
