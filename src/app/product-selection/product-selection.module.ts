import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductTileComponent } from './product-tile/product-tile.component';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ProductSelectionComponent } from './product-selection.component';
import { FooterModule } from '../common/footer/footer.module';
import { ToolbarModule } from '../common/toolbar/toolbar.module';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';

@NgModule({
	imports: [CommonModule,
		// FlexLayoutModule,
		PerfectScrollbarModule,
		FooterModule,
		ToolbarModule,
		ContactDetailsModule
	],
	exports: [ProductTileComponent, ProductSelectionComponent],
	declarations: [ProductTileComponent, ProductSelectionComponent],
})
export class ProductSelectionModule {

}

export * from './product-selection.component';
