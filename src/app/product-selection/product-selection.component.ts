import { Component, OnInit, ChangeDetectorRef, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
// import { ObservableMedia  } from '@angular/flex-layout';
import { ElectronService } from '../common/electron/electron.service';
import { ProductService } from '../common/product/product.service';
import { Observable, BehaviorSubject, Subscription, fromEvent } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { Product } from '../common/product/product.module';

// Note importing from lettable/pipeable operators - 'operators' plural
/*import { tap } from 'rxjs/operators';

import {MatButton} from '@angular/material/button';
import { switchMap } from 'rxjs-compat/operator/switchMap';*/
import { Router, ActivatedRoute } from '@angular/router';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { LocationService } from '../common/location/location.service';
import { SettingsService } from '../common/settings/settings.service';
import { ProductLocationSettingsComponent } from '../common/settings/product-location-settings/product-location-settings.component';
import { ProductLocale } from '../common/product/product';

@Component({
	selector: 'bwfe-product-selection',
	templateUrl: './product-selection.component.html',
	styleUrls: ['./product-selection.component.scss']
})
export class ProductSelectionComponent implements OnInit, AfterViewInit, OnDestroy {
	// public products: string[] = ['hi'];

	public product$: Observable<Product[]>;
	public productsLoading$: Observable<boolean>;

	// Access the button through the template variable, typed to MatButton
	@ViewChild('btnTemplateName') myBtn: HTMLButtonElement;
	@ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;

	myBtnClicks$: Observable<any>;

	/**
	 * The number of colums in the mat-grid-list directive.
	 */
	public cols: Observable<number>;

	public selectedProductIdSubject: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
	public selectedProductId$: Observable<string> = this.selectedProductIdSubject.asObservable().pipe(distinctUntilChanged());

	private subscriptions: Subscription[];

	constructor(private router: Router, private route: ActivatedRoute, public electronService: ElectronService,
		public productService: ProductService, private settingsService: SettingsService,
		private cdr: ChangeDetectorRef, private location: LocationService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.product$ = this.productService.product$;
		this.productsLoading$ = this.productService.productsLoading$;
		this.subscriptions.push(this.route.params.subscribe(params => {
			this.selectedProductIdSubject.next(params['previous']);
		}));
		if (!this.productService.isInitialised) {
			this.productService.init();
		}
		this.location.countryCode.subscribe(code => {
			console.log(`Country code: ${code}`);
		});
		// this.productService.productFolders().subscribe(value => console.error('Folder: ' + value));
/* 		const programFiles: string = this.electronService.environmentVariable('PROGRAMFILES(X86)');
		console.log(`pg: ${programFiles}`);
		const bwFolder = this.electronService.pathJoin(programFiles, 'Boardworks'); */
		// this.electronService.filesInFolder(bwFolder).subscribe(value => console.error(value));
	}

	ngAfterViewInit() {
		/* if (this.selectedProductIdSubject.getValue() != null) {
			this.subscriptions.push(this.productService.productsLoading$.subscribe({
				next: () => {
					console.log(`Loaded....`);
					const el = document.querySelector('[id="product_' + this.selectedProductIdSubject.getValue() + ']');
					console.error(`ELEment: ${el}`);
				}
			}));
		} */
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	updateProducts() {
		return this.productService.getProducts();
	}

	selectProduct(product: Product) {
		console.log(`Select product: ${product.application}`);
		this.selectedProductIdSubject.next(product.id);
	}

	deselectProduct() {
		console.log(`DESELECT`);
		this.selectedProductIdSubject.next(undefined);
	}

	isSelected(product: Product): boolean {
		return product && product.id === this.selectedProductIdSubject.getValue();
	}

	openProduct() {
		/* this.subscriptions.push(this.productService.getProduct(this.selectedProductId).subscribe(product => {
			if (product.type === ProductType.STANDARD) {
				if (product.locale === ProductLocale.US && product.statesOnly.length > 1) {
					this.router.navigate(['./map', this.selectedProductId]);
				} else {
					this.router.navigate(['./product', this.selectedProductId]);
				}
			} else if (product.type === ProductType.STANDARDLESS) {
				this.router.navigate(['./stateless-product', this.selectedProductId]);
			}
		})); */
		this.productService.navigateToProduct(this.selectedProductIdSubject.getValue());
	}

	showProductLocationSettings() {
		this.settingsService.showSettings();
		this.settingsService.showSettingsMenuEntry(ProductLocationSettingsComponent.entryName);
	}
}
