import { Component, OnInit, Input, Output, ViewChild } from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';
import { Product, ProductService } from '../../common/product/product.module';
import { AfterViewInit } from '@angular/core';
import { ElementRef } from '@angular/core';

@Component({
	selector: 'bwfe-product-tile',
	templateUrl: './product-tile.component.html',
	styleUrls: ['./product-tile.component.scss'],
	/*animations: [
		trigger('loadedState', [
			state('void', style({
				'opacity': 0
			})),
			state('loaded', style({
				'opacity': 1
			})),
			transition('* => *', animate(500))
		])
	]*/
})
export class ProductTileComponent implements OnInit, AfterViewInit {

	@Input() product: Product;
	@Input() selected: boolean;

	@Output() loadedState: string;

	@ViewChild('card') card: ElementRef;

	get imageUrl(): string {
		if (this.product && this.product.iconUrl != null) {
			return this.product.iconUrl;
		} else {
			return '';
		}
	}

	constructor(private productService: ProductService, private elementRef: ElementRef) {

	}

	ngOnInit() {
		this.loadedState = 'loaded';
	}

	ngAfterViewInit() {
		if (this.selected) {
			const timeout = 10;
			setTimeout(() => {
				console.error(`Scroll into view: ${this.card.nativeElement.offsetTop}`);
				// this.card.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
				this.card.nativeElement.ownerDocument.querySelector('.ps').scrollTop = this.card.nativeElement.offsetTop;
			}, timeout);
		}
	}

}
