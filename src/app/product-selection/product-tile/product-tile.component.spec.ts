import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NgZone } from '@angular/core';
import { ProductTileComponent } from './product-tile.component';
/*import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';*/
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Product, ProductService } from '../../common/product/product.module';
import { ElectronService } from '../../common/electron/electron.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ProductTestingService } from '../../common/product/testing/product-testing.service';

describe('ProductTileComponent', () => {
	let component: ProductTileComponent;
	let fixture: ComponentFixture<ProductTileComponent>;
	let httpMock: HttpTestingController;
	beforeEach(async(() => {

		const mockNgZone = jasmine.createSpyObj('mockNgZone', ['run', 'runOutsideAngular']);
		mockNgZone.run.and.callFake(fn => fn());

		const mockProductService = new ProductTestingService().configure({products: []});

		const bed = TestBed.configureTestingModule({
			declarations: [ ProductTileComponent ],
			schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
			imports: [/*BrowserAnimationsModule, MatButtonModule, MatCheckboxModule, MatCardModule,
				MatChipsModule,	MatGridListModule, MatIconModule, MatToolbarModule, MatDividerModule,*/
				HttpClientTestingModule
			],
			providers: [
				/*{ provide: NgZone, useValue: mockNgZone },*/
				{ provide: ProductService, useValue: mockProductService }
			]
		});
		httpMock = bed.get(HttpTestingController);
		bed.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductTileComponent);
		component = fixture.componentInstance;
		component.product = new Product('');
		fixture.detectChanges();
	});

	it('should create component', () => {
		expect(component).toBeTruthy();
	});
});
