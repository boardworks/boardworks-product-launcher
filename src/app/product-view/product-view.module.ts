import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductViewComponent } from './product-view.component';
import { FooterModule } from '../common/footer/footer.module';
import { ToolbarModule } from '../common/toolbar/toolbar.module';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
	imports: [CommonModule,
		FooterModule,
		ToolbarModule,
		ContactDetailsModule,
		PerfectScrollbarModule,
	],
	exports: [ProductViewComponent],
	declarations: [ProductViewComponent],
})
export class ProductViewModule {

}

export * from './product-view.component';
