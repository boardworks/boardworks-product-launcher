import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService, Product, State, ProductLocale } from '../common/product/product.module';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
// import { MatRadioChange } from '@angular/material';

@Component({
	selector: 'bwfe-product-view',
	templateUrl: './product-view.component.html',
	styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit, OnDestroy {
	productSubject: BehaviorSubject<Product> = new BehaviorSubject<Product>(undefined);
	product$: Observable<Product> = this.productSubject.asObservable().pipe(distinctUntilChanged());

	productIconSubject: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
	productIcon$: Observable<string> = this.productIconSubject.asObservable().pipe(distinctUntilChanged());

	productId: string;
	productSubscription: Subscription;

	usLocale: ProductLocale = ProductLocale.US;
	ukLocale: ProductLocale = ProductLocale.UK;

	private subscriptions: Subscription[];
/*
	get product(): Product {
		return this.productService.selectedProduct;
	}

	get selectedState(): State {
		return this.product$.getValue().selectedState;
		// return undefined;
	}*/

	constructor(private router: Router, private route: ActivatedRoute, public productService: ProductService) {
		this.subscriptions = [];
	}

	ngOnInit() {
		this.subscriptions.push(this.productSubscription = this.route.params.subscribe(params => {
			this.productId = params['id'];
			console.log(`Product View is: ${this.productId}`);
			this.productService.getProduct(this.productId).subscribe(value => {
				if (value != null) {
					this.productSubject.next(value);
					this.productIconSubject.next(value.iconUrl);
				}
			});
		}));
	}

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			subscription.unsubscribe();
		});
		this.subscriptions = [];
	}

	isDoubleHeightButton(state: State) {
		return state.name.length > 20;
	}

	goBack() {
		this.router.navigate(['./']);
	}

	stateChange(event/* : MatRadioChange */) {
		/*console.log(`Selected: ${event.value}`);
		this.product.selectedStateAbbreviation = event.value;*/
	}
/*
	isSelected(state: State): boolean {
		if (this.product != null && state != null) {
			return this.product.selectedStateAbbreviation === state.abbreviation;
		}
		return false;
	}
*/

	viewState(state?: string) {
		if (state == null) {
			state = this.productSubject.getValue().statesOnly[0].abbreviation;
		}
		this.router.navigate(['./state', this.productId, state]);
	}
}
