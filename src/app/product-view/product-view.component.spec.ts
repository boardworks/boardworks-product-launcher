import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductViewComponent } from './product-view.component';
import { ProductService } from '../common/product/product.service';
import { ProductTestingService, ProductTestingConfiguration } from '../common/product//testing/product-testing.service';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FooterModule } from '../common/footer/footer.module';
import { ElectronService } from '../common/electron/electron.module';
import { ElectronTestingService, DefaultElectronTestingConfiguration } from '../common/electron/testing/electron-testing.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToolbarTestingModule } from '../common/toolbar/testing/toolbar-testing.module';
import { ContactDetailsModule } from '../common/contact-details/contact-details.module';

describe('ProductViewComponent', () => {
	let component: ProductViewComponent;
	let fixture: ComponentFixture<ProductViewComponent>;

	let mockElectronService: ElectronTestingService;

	const configuration: ProductTestingConfiguration = {
		products: [],
	};

	beforeEach(async(() => {

		mockElectronService = new ElectronTestingService().configure(DefaultElectronTestingConfiguration);

		TestBed.configureTestingModule({
			declarations: [ ProductViewComponent ],
			imports: [
				RouterTestingModule,
				FooterModule,
				ToolbarTestingModule,
				HttpClientTestingModule,
				ContactDetailsModule,
				PerfectScrollbarModule
			],
			providers: [
				{ provide: ProductService, useValue: new ProductTestingService().configure(configuration) },
				{ provide: ElectronService, useValue: mockElectronService },
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductViewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
