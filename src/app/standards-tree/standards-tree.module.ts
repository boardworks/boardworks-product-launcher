import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StandardsTreeComponent } from './standards-tree.component';
import { StandardsTreeNodeComponent } from './standards-tree-node/standards-tree-node.component';
import { FooterModule } from '../common/footer/footer.module';
import { ToolbarModule } from '../common/toolbar/toolbar.module';
import { StandardsTreeService } from './standards-tree.service';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
	imports: [CommonModule,
		PerfectScrollbarModule,
		FooterModule,
		ToolbarModule,
	],
	exports: [StandardsTreeComponent, StandardsTreeNodeComponent],
	declarations: [StandardsTreeComponent, StandardsTreeNodeComponent],
	providers: [StandardsTreeService],
})
export class StandardsTreeModule {

}

export * from './standards-tree.component';
