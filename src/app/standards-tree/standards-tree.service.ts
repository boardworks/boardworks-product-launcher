import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class StandardsTreeService {
	selection: BehaviorSubject<number> = new BehaviorSubject<number>(-1);

	setSelection(id: number) {
		this.selection.next(id);
	}

	getSelection(): Observable<number> {
		return this.selection.asObservable();
	}

	selectedRef(): number {
		return this.selection.getValue();
	}
}
