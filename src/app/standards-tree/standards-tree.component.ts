import { Component, OnInit, Input, AfterViewInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { Standards } from '../common/product/standards';
import { StandardsTreeService } from './standards-tree.service';
import { Subscription } from 'rxjs';
import { StandardsTreeNodeComponent } from './standards-tree-node/standards-tree-node.component';

@Component({
	selector: 'bwfe-standards-tree',
	templateUrl: './standards-tree.component.html',
	styleUrls: ['./standards-tree.component.scss']
})
export class StandardsTreeComponent implements OnInit, AfterViewInit, OnDestroy {

	@Input() standards: Standards;
	@ViewChildren(StandardsTreeNodeComponent) nodes: QueryList<StandardsTreeNodeComponent>;

	private selectionSubscription: Subscription;

	constructor(private treeService: StandardsTreeService) { }

	ngOnInit() {

	}

	ngAfterViewInit() {
		console.log(`Standards: ${this.standards}`);
		this.treeService.getSelection().subscribe(value => console.log(`Selected: ${value}`));
	}

	ngOnDestroy() {
		if (this.selectionSubscription != null) {
			this.selectionSubscription.unsubscribe();
			this.selectionSubscription = null;
		}
	}

}
