import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardsTreeComponent } from './standards-tree.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FooterModule } from '../common/footer/footer.module';
import { StandardsTreeNodeComponent } from './standards-tree-node/standards-tree-node.component';
import { StandardsTreeService } from './standards-tree.service';
import { ToolbarTestingModule } from '../common/toolbar/testing/toolbar-testing.module';

describe('StandardsTreeComponent', () => {
	let component: StandardsTreeComponent;
	let fixture: ComponentFixture<StandardsTreeComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				PerfectScrollbarModule,
				FooterModule,
				ToolbarTestingModule,
			],
			declarations: [ StandardsTreeComponent, StandardsTreeNodeComponent ],
			providers: [StandardsTreeService],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StandardsTreeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
