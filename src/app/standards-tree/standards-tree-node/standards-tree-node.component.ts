import { Component, OnInit, Input } from '@angular/core';
import { Standards } from '../../common/product/standards';
import { StandardsTreeService } from '../standards-tree.service';

@Component({
	selector: 'bwfe-standards-tree-node',
	templateUrl: './standards-tree-node.component.html',
	styleUrls: ['./standards-tree-node.component.scss']
})
export class StandardsTreeNodeComponent implements OnInit {

	@Input() node: Standards;
	@Input() open ? = false;
	@Input() selected ? = false;

	constructor(private treeService: StandardsTreeService) { }

	ngOnInit() {
	}

	toggleOpen() {
		this.open = !this.open;
	}

	select() {
		if (this.node.children.length > 0) {
			this.toggleOpen();
		}
		this.treeService.setSelection(this.node.ref);
	}

	nodeDetailsClass(): string[] {
		const cls: string[] = [];
		if (this.node != null && this.node.children.length > 0) {
			cls.push('parent-node');
		}
		return cls;
	}

	nodeClass(): string[] {
		const cls: string[] = [];
		if (this.node) {
			const level: number = this.node.level;
			cls.push(`node-level-${level}`);
			if (this.open) {
				cls.push('node-open');
			}
			if (this.treeService.selectedRef() === this.node.ref) {
				cls.push('node-selected');
			}
			if (this.node.sortedChildActivities.length === 0) {
				cls.push('node-activities-empty');
			}
		}
		return cls;
	}
}
