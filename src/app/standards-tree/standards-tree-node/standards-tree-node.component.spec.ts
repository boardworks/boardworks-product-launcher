import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardsTreeNodeComponent } from './standards-tree-node.component';
import { StandardsTreeService } from '../standards-tree.service';
import { Standards } from '../../common/product/standards';
import { State } from '../../common/product/state';
import { Product } from '../../common/product/product';

describe('StandardsTreeNodeComponent', () => {
	let component: StandardsTreeNodeComponent;
	let fixture: ComponentFixture<StandardsTreeNodeComponent>;
	const product: Product = new Product('');
	const state: State = new State(product, 'test state', 'TS');
	const standards: Standards = new Standards(0, 'test', null);
	standards.state = state;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ StandardsTreeNodeComponent ],
			providers: [StandardsTreeService],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StandardsTreeNodeComponent);
		component = fixture.componentInstance;
		component.node = standards;
		component.open = false;
		component.selected = false;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
