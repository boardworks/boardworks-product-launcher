// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '/',
    files: [
      { pattern: './src/assets/**/*.*', watched: false, included: false, nocache: false, served: true },
      // { pattern: './src/testing-assets/**/*.*', watched: false },
    ],
    exclude: [
      "node_modules"
    ],
    proxies: {
      'assets/': '/base/src/assets/',
    },
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
      require('karma-electron'),
      require('karma-scss-preprocessor')
    ],
    client:{
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
      useIframe: false,
      jasmine: {
        random: false
      }
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage'),
      reports: [ 'html', 'lcov' ],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml', 'coverage-istanbul'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['CustomElectron'],
    singleRun: true,
    preprocessors: {
      // './src/test.ts': ['@angular/cli'],
      // '**/*.js': ['electron']
    },
    customLaunchers: {
      CustomElectron: {
        base: 'Electron',
        flags: ['--show'],
        browserWindowOptions: {
          webPreferences: {
            preload: __dirname + '/preload.js'
          }
        }
      }
    }
  });
};
