// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import 'zone.js/dist/zone-testing';
import { getTestBed } from '@angular/core/testing';

import {
	BrowserDynamicTestingModule,
	platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing';
import { NgZone, NO_ERRORS_SCHEMA,  } from '@angular/core';

declare const require: any;

const TestBed = getTestBed();

// First, initialize the Angular testing environment.
TestBed.initTestEnvironment(
	BrowserDynamicTestingModule,
	platformBrowserDynamicTesting()
);

console.log(`CONFIGURE TESTBED!!!`);

TestBed.configureTestingModule({
	// this line
	providers: [
	],
});

// Then we find all the tests.
const context = require.context('./', true, /\.spec\.ts$/);
// And load the modules.
context.keys().map(context);
