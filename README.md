
# Boardworks Product Launcher

Launch and navigate all the installed Boardworks PowerPoint product content and browse curricula mapping from within this single cross-platform application.

The application will discover the Boardworks products installed on the local system, or in user specified network locations, and will parse the product curricula and mapping to replicate the functionality of the original product 'front-end' applications which shipped with the disc products. The user can navigate the curricula mapping to launch presentations and other supporting assets from the application.

## Development

### Getting Started

Clone this repository locally :

``` bash
git clone https://gitlab.com/boardworks/boardworks-product-launcher.git
```

Install dependencies with npm :

``` bash
npm install
```

Development build

```bash
npm start
```

Tests can be run with

```bash
npm run test
```

Locally building the application can be done with:

On Windows:
```bash
npm run electron:windows
```

On macOS:
```bash
npm run electron:mac
```

When running the application (even a release build), if the `--debug` command line option is provided, a Chrome debug window will open allongside the application for debugging purposes.

### Releasing
The respository on GitLab has associated [build pipelines](https://gitlab.com/boardworks/boardworks-product-launcher/-/pipelines) which execute when files are committed to the `master` branch. The pipeline is configured by the `.gitlab-ci.yml` file at the root of the project.

The build pipeline requires a GitLab build Runner to be assigned ([in settings](https://gitlab.com/boardworks/boardworks-product-launcher/-/settings/ci_cd)). The runner should be a Windows machine to build the Windows version of the application, and a macOS machine to build the macOS.

Currently the build and application release is geared towards Windows. It should be possible to add stages, filtered by tags, to target macOS within the same pipeline.

The build pipeline works in multiple stages:
1. First the tests are run to ensure there have been no regressions in functionality.
2. Next the build stage runs which will build a Windows release of the application. This build isn't used, it just confirms that the application *will* build successfully.
3. The third job in the pipeline is deployment, which is manually triggered. This job will tag the repository with the incremented version number. The version number bump is determined by analysis of the commit logs using 'conventional commit' convensions.
4. Tagging the repository with the release version will trigger the `tags` only job `build_release` which will do a fresh build (without tests) and realse the build artifact. *It is possible to trigger this stage directly by manually tagging the repository with the desired release version number.*

### Application automatic updates
The application uses the `electron-updater` package (as part of [`electron-builder`](https://www.electron.build)) to manage automatic updates. Releases are retreived from GitLab as build artifacts using the GitLab API (and `Boardworks Update User` project member `BoardworksFrontEnd` Private Token with read access to the API). See [`UpdateService`](./src/app/common/update/update.service.ts) class for details.

### Product Structure
To be loaded by the application, a product must contain specific files in known locations. The requirements are set out below:

#### Product Properties
The `curricula/ProductProps.xml` file describes the product. It contains an XML fragment (with no root element), with a `title` element which specifies the product name.

#### Activities
The presentations in a product are itemised in an `curricula/Activities.xml` file found in the `curricula` folder of the product. This file contains a list of `activity` elements as an XML fragment (with no root element). Each entry includes the presentation filename (`title` attribute), the title to use for the presentation within the application (`displayTitle`), the number of slides in the presentation (`length`), a description (`description`) and an id (`ref`) which is used to reference the activity in the curricula mapping.

#### States
The states that the product covers are listed in the `curricula/states.xml

### Supported Products
The products supported by this application are determined by the entries in the [`products.json`](./src/assets/products.json) configuration file. Each product is represented as a settings 'template' which is populated from this configuration object. The entries in the `products` property are for supported applications. The correct entry is determined by the `application` property which will match the product `front-end` application name. A product entry sets the title to use in the application for that product, the icon and color to use within the application for highlights. Explicitly unsupported products (by product 'front-end' .exe name) in the `unsupported` property, these products will be excluded from the application. 

Several of the product settings in the 'template' are determined by examining the structure of the installed product on disc, such as whether to display the product mapping as a geographic map of the US, or a state list.

The `preload` property is used to pre-configure the product 'template' to populate settings which can't be inferred from the installed product either because a particular release version has errors, or because of the bespoke nature of a particular poduct. The product entries in this property are 'versioned' using SemVer convensions. Data which can be set here includes paths to product settings files, to either remap incorrectly named files, or to override a settings file which shipped with a product with a version included in this application. For example the entry in `preload` below:

```json
"Boardworks MS History": {
	"<1.2": {
		"statesPath": ["${assetsPath}", "replacement-curricula", "ms-history", "1.0.0-1.1.0", "states.xml"]
	}
}
```

is used to override the states.xml path for the versions below 1.2 of *Boardworks MS History* product so that the `states.xml` file is loaded from one bundled with this application `assets/replacement-curricula/ms-history/1.0.0-1.1.0/state.xml`.

The entry for `Boardworks Lord of the Flies:

```json
"Boardworks Lord of the Flies": {
	"*": {
		"type": "standardless",
		"activitiesPath": ["${assetsPath}", "replacement-curricula", "lord-of-the-flies", "activities.xml"]
	}
}
```

sets that product to show as `standardless` (no state standards - so without a geographic map or state list, but directly viewing the list of presentations), and the path to the `activities.xml` (the file which lists the presentations for the curricula mapping) is overridden to one bundled in the application. This product doesn't have the expected `curricula` folder with `Activities.xml`, `states.xml` etc, so the Product Launcher application contains the necessary files to allow this product to be supported (the `${assetsPath}` entry is a token which represents the Product Launcher assets folder. The entries in this list are joined together to form a path).

By using this `preload` property errors can be fixed in shipped products, or legacy products can be supported by including all necessary data.

The `patch` property lists Regular Expression text replacements for products and files. These can be used to resolve typos in settings files without bundling entire file replacements. See [Patching](#patching-settings-files)

### Patching Settings Files
By using the `patch` entry in the `products.json` it is possible to execute Regular Expression search/replace queries against the raw text of the settings files before they are processed as XML. The `patch` entries match the name of the product 'front-end' application. An example `patch` entry is given below:

```json
"patch": {
    "High School US History": {
        "activities": [{
            "match": "\"displayTitle=",
            "replace": "\" displayTitle="
        }]
    }
}
```

This entry will patch the `Activity.xml` file of the *HS US History* product and fix the missing space before the `displayTitle` attribute in the XML  which prevents it from being processed correctly. The values in the `ProductPatchConfigurationNames` Enum determine the file being patched.

### Adobe AIR applications (OSX)
In addition to supporting Windows based installed products, cross-platform Adobe AIR base product installs are supported. These are often installed on OSX systems. Although the structure of these products do not match the Windows versions, the `MacProduct` specialisation of the `Product` class provides support for these products by mapping them to a standard product format.

## Customer support

### Issue Tracking and Contact Details
The *Product Launcher* application has the features to allow users to submit bug reports. This is done by providing a *Report a Bug* button which takes the user directly to the issue tracking board in project repository. This button is on the *Contact Details* dialog reached by clicking on the *Boardworks* logo. The button and the contact details for the US and UK offices are in the template for the `ContactDetailsComponent` component.

### Automatic Updated
Whenever the application is launched a check is made to GitLab to see if there is a more recent release than the version currently running. If there is, the user is given the option to download and install that version. 
If the application was originally installed (on Windows) to the Program Files directory, then Administrator access is required to install an update.