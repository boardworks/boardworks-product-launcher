import { app, BrowserWindow, screen, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';
import { autoUpdater, UpdateCheckResult } from 'electron-updater-bin';
import { Observable, of } from 'rxjs';
import log from 'electron-log';
import { ENOENT } from 'constants';
import { tmpdir } from 'os';

/*const fetch = require('node-fetch');
const Headers = fetch.Headers;*/

let win, serve, debug;
let assetWindow: BrowserWindow;
let releaseWindow: BrowserWindow;
let searchWindow: BrowserWindow;
const mainDirectory: string = __dirname;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');
debug = args.some(val => val === '--debug');
log.catchErrors({showDialog: false});
log.transports.file.level = 'info';

try {
	log.info('Starting application');
} catch (e) {
	if (e.code === ENOENT) {
		const tempDir: string = tmpdir();
		const p = path.join(tempDir, 'boardworks-product-launcher.log');
		log.transports.file.file = p;
		log.info('Starting application');
		log.info(`Using temp path: ${p}`);
	} else {
		// Ignore

	}
}

function createWindow() {
	const electronScreen = screen;
	const size = electronScreen.getPrimaryDisplay().workAreaSize;

	// Create the browser window.
	const settings: any = {
		x: 0,
		y: 0,
		width: size.width,
		height: size.height,
		title: 'Boardworks',
		webPreferences: {
			nodeIntegration: true
		}
	};
	if (serve) {
		settings.webPreferences = {
			webSecurity: false,
			nodeIntegration: true
		};
	}
	win = new BrowserWindow(settings);

	if (serve) {
		require('electron-reload')(__dirname, {
		electron: require(`${__dirname}/node_modules/electron`)});
		win.loadURL('http://localhost:4200');
	} else {
		log.warn(`Opening main window as: ${path.join(__dirname, 'dist/index.html')}`);
		win.loadURL(url.format({
			pathname: path.join(__dirname, 'dist/index.html'),
			protocol: 'file:',
			slashes: true
		}));
	}

	if (debug) {
		win.webContents.openDevTools();
	}

	// Emitted when the window is closed.
	win.on('closed', () => {
		// Dereference the window object, usually you would store window
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		win = null;
	});
}

function createAssetWindow() {
	if (assetWindow == null || assetWindow.isDestroyed()) {
		assetWindow = new BrowserWindow({parent: win, title: 'Asset Window', show: false, webPreferences: {
			nodeIntegration: true
		}});
	}
	if (serve) {
		require('electron-reload')(__dirname, {
		electron: require(`${__dirname}/node_modules/electron`)});
		assetWindow.loadURL(`http://localhost:4200/#/asset-loader`);
	} else {
		const assetUrl = path.join(mainDirectory, `/../dist/index.html#/asset-loader`);
		log.warn(`Opening asset window as: ${assetUrl}`);
		assetWindow.loadURL(url.format({
			pathname: path.join(__dirname, 'dist/index.html'),
			protocol: 'file:',
			slashes: true,
			hash: 'asset-loader'
		}));
	}
	// assetWindow.webContents.openDevTools();
}

function createSearchWindow() {
	if (searchWindow == null || searchWindow.isDestroyed()) {
		searchWindow = new BrowserWindow({parent: win, title: 'Search Window', show: false, webPreferences: {
			// webSecurity: false,
			nodeIntegration: true
		}});
	}
	const searchUrl = path.join(__dirname, `dist/index.html?goto=search`);
	log.warn(`Opening search window as: ${searchUrl}`);
	if (serve) {
		require('electron-reload')(__dirname, {
		electron: require(`${__dirname}/node_modules/electron`)});
		searchWindow.loadURL(`http://localhost:4200/#/search`);
	} else {
		searchWindow.loadURL(url.format({
			pathname: `${__dirname}/dist/index.html`,
			protocol: 'file:',
			slashes: true,
			hash: 'search'
		}));
	}
	// searchWindow.webContents.openDevTools();
}

function showReleaseNotes(version, notesMd) {
	if (releaseWindow == null || releaseWindow.isDestroyed()) {
		releaseWindow = new BrowserWindow({parent: win, title: 'Release Notes', webPreferences: {nodeIntegration: true}});
	}
	const notes: string = encodeURIComponent(Buffer.from(notesMd).toString('base64'));
	if (serve) {
		require('electron-reload')(__dirname, {
		electron: require(`${__dirname}/node_modules/electron`)});
		releaseWindow.loadURL(`http://localhost:4200/#/release-notes/${version}/${notes}`);
	} else {
		releaseWindow.loadURL(url.format({
			pathname: `${__dirname}/dist/index.html`,
			// pathname: path.join(__dirname, `dist/index.html/#/release-notes/${notes}`),
			protocol: 'file:',
			slashes: true,
			hash: `release-notes/${version}/${notes}`
		}));
	}

	const handleRedirect = (e, link: string) => {
		if (link !== releaseWindow.webContents.getURL()) {
			e.preventDefault();
			require('electron').shell.openExternal(link);
		}
	};
	releaseWindow.webContents.on('will-navigate', handleRedirect);
	releaseWindow.webContents.on('new-window', handleRedirect);
}

function loadAsset(assetPath: string): Observable<any> {
	return of(`hello: ${assetPath}`);
}

try {
	app.setAppUserModelId('uk.co.boardworks.product-launcher');

	autoUpdater.autoDownload = false;
	autoUpdater.autoInstallOnAppQuit = true;
	autoUpdater.requestHeaders = { 'PRIVATE-TOKEN': 'ecSgBUpPHGMJUyeneQ8K'};

	/**
	 * Asset loading ipc
	 */
	ipcMain.on('load-asset', (event, assetUrl: string) => {
		log.warn(`Loading ${assetUrl}`);
		// event.sender.send('load-asset-response', assetUrl, 'abcdef');
		assetWindow.webContents.send('load-asset-buffer', assetUrl);
	});

	ipcMain.on('load-asset-complete', (event, assetUrl: string, buffer: any) => {
		log.warn(`Asset Loaded: ${assetUrl}`);
		win.webContents.send('load-asset-complete', assetUrl, buffer);
	});

	ipcMain.on('load-asset-failed', (event, assetUrl: string, reason: string) => {
		log.warn(`Failed to load asset ${assetUrl}: ${reason}`);
		win.webContents.send('load-asset-complete', assetUrl, undefined, reason);
	});

	/**
	 * Search ipc
	 */

	ipcMain.on('search-reset', (event, documents_json) => {
		searchWindow.webContents.send('search-thread-reset');
	});

	ipcMain.on('search-add', (event, documents_json) => {
		searchWindow.webContents.send('search-thread-add', documents_json);
	});

	ipcMain.on('search-query', (event, term) => {
		searchWindow.webContents.send('search-thread-query', term);
	});

	ipcMain.on('search-thread-results', (event, results_json) => {
		win.webContents.send('search-results', results_json);
	});

	/**
	 * Updater ipc
	 */

	ipcMain.once('updater-ready', (event) => {
		win.webContents.send('start-update-check');
	});

	autoUpdater.on('error', (error) => {
		log.error(`ERROR Updating: ${error}`);
		win.webContents.send('update-error', error);
	});

	autoUpdater.on('checking-for-update', function (event) {
		log.info('Checking for update...');
	});

	autoUpdater.on('update-available', function (info) {
		log.info('Update available.');
		log.debug('Nofifying renderer');
		win.webContents.send('update-available', info);
	});

	autoUpdater.on('update-not-available', function (info) {
		log.info('Update not available.');
		win.webContents.send('update-not-available', info);
	});

	autoUpdater.on('download-progress', function (info) {
		log.info(`Downloading Update: ${info.percent}`);
		win.webContents.send('download-progress', info.percent);
	});

	autoUpdater.on('update-downloaded', function (info) {
		log.info(`Update downloaded`);
		log.debug(`Telling renderer`);
		win.webContents.send('update-downloaded', true);
	});

	ipcMain.on('check-for-update', (event, updateUrl: string, channel?: string) => {
		log.info(`Check for update on channel ${channel}`);
		autoUpdater.setFeedURL({
			provider: 'generic',
			url: updateUrl,
			channel,
		});
		autoUpdater.checkForUpdates();
	});

	ipcMain.on('download-update', () => {
		autoUpdater.downloadUpdate();
	});

	ipcMain.on('install-update', () => {
		autoUpdater.quitAndInstall(false);
	});

	ipcMain.on('show-release-notes', (event, version, html) => {
		showReleaseNotes(version, html);
	});

	// This method will be called when Electron has finished
	// initialization and is ready to create browser windows.
	// Some APIs can only be used after this event occurs.
	app.on('ready', () => {
		createWindow();
		createAssetWindow();
		createSearchWindow();
	});

	app.on('before-quit', (event) => {
		if (serve) {
			event.preventDefault();
		}
	});

	app.on('will-quit', (event) => {
		if (serve) {
			event.preventDefault();
		}
	});

	// Quit when all windows are closed.
	app.on('window-all-closed', () => {
		// On OS X it is common for applications and their menu bar
		// to stay active until the user quits explicitly with Cmd + Q
		if (process.platform !== 'darwin') {
			app.quit();
		}
	});

	app.on('activate', () => {
		// On OS X it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (win === null) {
			createWindow();
		}
	});

} catch (e) {
	// Catch Error
	// throw e;
	log.warn(e);
}
